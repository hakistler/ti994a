package com.dreamcodex.ti;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.net.URL;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.*;

import com.dreamcodex.ti.component.BitmapCanvas;
import com.dreamcodex.ti.component.BitmapEditor;
import com.dreamcodex.ti.component.ImagePreview;
import com.dreamcodex.ti.component.TadatakaExportDialog;
import com.dreamcodex.ti.util.Globals;
import com.dreamcodex.ti.util.TIGlobals;
import com.dreamcodex.ti.util.MutableFilter;

/** Tadataka
  * TI-99/4A bitmap mode map editor
  *
  * @author Howard Kistler
  */

public class Tadataka extends JFrame implements WindowListener, ActionListener, MouseListener, MouseMotionListener
{
// Constants -------------------------------------------------------------------------------/

	public static final String CMD_MPTIARAW = "importtiartistraw";
	public static final String CMD_MPTIAHDR = "importtiartistheader";
	public static final String CMD_XPTIARAW = "exporttiartistraw";
	public static final String CMD_XPTIAFIL = "exporttiartistheadertifiles";
	public static final String CMD_XPTIAV9  = "exporttiartistheaderv9t9";
	public static final String CMD_SAVESEL  = "saveselection";
	public static final String CMD_XPASMSEL = "exportassemblyselection";
	public static final String CMD_XPTIASEL = "exporttiartistselection";

	public final int HEADER_SIZE = 128;
	public final int HEADER_TYPE_NONE = 0;
	public final int HEADER_TYPE_TIFILES = 1;
	public final int HEADER_TYPE_V9T9 = 2;
	public static final String[] HEADER_TIFILES_HEX = { "07", "54", "49", "46", "49", "4C", "45", "53", "00", "18", "01", "00", "00", "01", "18", "00" };
	public static int[] HEADER_TIFILES = new int[HEADER_TIFILES_HEX.length];
	static
	{
		for(int hdr = 0; hdr < HEADER_TIFILES_HEX.length; hdr++)
		{
			HEADER_TIFILES[hdr] = Integer.parseInt(HEADER_TIFILES_HEX[hdr], 16);
		}
	}
	public static final String[] HEADER_V9T9_P_HEX = { "69", "6D", "61", "67", "65", "5F", "5F", "50", "20", "20", "00", "00", "01", "00", "00", "18" };
	public static final String[] HEADER_V9T9_C_HEX = { "69", "6D", "61", "67", "65", "5F", "5F", "43", "20", "20", "00", "00", "01", "00", "00", "18" };
	public static int[] HEADER_V9T9_P = new int[HEADER_V9T9_P_HEX.length];
	public static int[] HEADER_V9T9_C = new int[HEADER_V9T9_C_HEX.length];
	static
	{
		for(int hdr = 0; hdr < HEADER_V9T9_P_HEX.length; hdr++)
		{
			HEADER_V9T9_P[hdr] = Integer.parseInt(HEADER_V9T9_P_HEX[hdr], 16);
		}
		for(int hdr = 0; hdr < HEADER_V9T9_C_HEX.length; hdr++)
		{
			HEADER_V9T9_C[hdr] = Integer.parseInt(HEADER_V9T9_C_HEX[hdr], 16);
		}
	}

// Local Constants -------------------------------------------------------------------------/

	private final String APPTITLE = "Tadataka : TI-99/4A Bitmap Editor";
	private final String FILEEXT = "tad";
	private final String[] FILEEXTS = { FILEEXT };
	private final String XBEXT = "xb";
	private final String[] XBEXTS = { XBEXT, "bas", "txt" };
	private final String ASMEXT = "asm";
	private final String[] ASMEXTS = { ASMEXT };
	private final String IMGEXT = "png";
	private final String[] IMGEXTS = { IMGEXT };
	private final String TIARTEXT = "tia";
	private final String TIARTPIXEXT = "tiap";
	private final String TIARTCLREXT = "tiac";
	private final String[] TIARTEXTS = { TIARTEXT, TIARTPIXEXT, TIARTCLREXT };
	private final String[] TIARTPIXEXTS = { TIARTPIXEXT };
	private final String[] TIARTCLREXTS = { TIARTCLREXT };

	private final int BITMAP_COLS = 32;
	private final int BITMAP_ROWS = 192;
	private final int BITMAP_CELL = 8;

	private final int ASM_LINELEN = 40;

// Variables -------------------------------------------------------------------------------/

	protected File mapDataFile;

	protected boolean bColorForeground = true;
	protected boolean bColorBackground = true;
	protected boolean bExportComments  = true;

// Components ------------------------------------------------------------------------------/

	private BitmapEditor bitmMain;

	private JMenu jmenSelection;

	private Properties appProperties = new Properties();

// Constructors ----------------------------------------------------------------------------/

	public Tadataka()
	{
		super();
		this.setTitle(APPTITLE);

		// obtain application properties (if exist)
		try
		{
			FileInputStream fis = new FileInputStream(new File("Tadataka.prefs"));
			appProperties.load(fis);
			fis.close();
		}
		catch(Exception e)
		{
		}

		if(appProperties.getProperty("exportComments") != null) { bExportComments = appProperties.getProperty("exportComments").toLowerCase().equals("true"); }

		// Create bitmap editor panel (needs to initialise early for the listeners)
		bitmMain = new BitmapEditor(BITMAP_COLS, BITMAP_ROWS, BITMAP_CELL, (MouseListener)this, (MouseMotionListener)this);
		bitmMain.clearGrid();
		bitmMain.setBkgrndColor(Globals.CLR_COMPONENTBACK);
		if(appProperties.getProperty("magnif") != null) { bitmMain.setViewScale(Integer.parseInt(appProperties.getProperty("magnif"))); }
		if(appProperties.getProperty("showGrid") != null) { bitmMain.setShowGrid(appProperties.getProperty("showGrid").toLowerCase().equals("true")); }
		if(appProperties.getProperty("showPosition") != null) { bitmMain.setShowPosIndic(appProperties.getProperty("showPosition").toLowerCase().equals("true")); }

		// Create the menus
		JMenuBar jMenuBar = new JMenuBar();
		JMenu jmenFile = new JMenu("File");
		JMenuItem jmitNew = new JMenuItem("New"); jmitNew.setActionCommand(Globals.CMD_NEW); jmitNew.addActionListener(this); jmenFile.add(jmitNew);
		JMenuItem jmitOpen = new JMenuItem("Open..."); jmitOpen.setActionCommand(Globals.CMD_OPEN); jmitOpen.addActionListener(this); jmenFile.add(jmitOpen);
		JMenuItem jmitSave = new JMenuItem("Save"); jmitSave.setActionCommand(Globals.CMD_SAVE); jmitSave.addActionListener(this); jmenFile.add(jmitSave);
		JMenuItem jmitSaveAs = new JMenuItem("Save As..."); jmitSaveAs.setActionCommand(Globals.CMD_SAVEAS); jmitSaveAs.addActionListener(this); jmenFile.add(jmitSaveAs);
//		jmenFile.addSeparator();
//		JMenuItem jmitAppend = new JMenuItem("Append Bitmaps"); jmitAppend.setActionCommand(Globals.CMD_APPEND); jmitAppend.addActionListener(this); jmenFile.add(jmitAppend);
		jmenFile.addSeparator();
		JMenuItem jmitExit = new JMenuItem("Exit"); jmitExit.setActionCommand(Globals.CMD_EXIT); jmitExit.addActionListener(this); jmenFile.add(jmitExit);
		jMenuBar.add(jmenFile);

		JMenu jmenImport = new JMenu("Import");
		JMenuItem jmitImportTIArtistRAW = new JMenuItem("TI Artist (RAW)..."); jmitImportTIArtistRAW.setActionCommand(CMD_MPTIARAW); jmitImportTIArtistRAW.addActionListener(this); jmenImport.add(jmitImportTIArtistRAW);
		JMenuItem jmitImportTIArtistHeader = new JMenuItem("TI Artist (HEADER)..."); jmitImportTIArtistHeader.setActionCommand(CMD_MPTIAHDR); jmitImportTIArtistHeader.addActionListener(this); jmenImport.add(jmitImportTIArtistHeader);
		jmenImport.addSeparator();
		JMenuItem jmitImportImgColor = new JMenuItem("Color Image..."); jmitImportImgColor.setActionCommand(Globals.CMD_MPIMGCL); jmitImportImgColor.addActionListener(this); jmenImport.add(jmitImportImgColor);
		jMenuBar.add(jmenImport);

		JMenu jmenExport = new JMenu("Export");
		JMenuItem jmitExportAsm = new JMenuItem("Assembler..."); jmitExportAsm.setActionCommand(Globals.CMD_XPASM); jmitExportAsm.addActionListener(this); jmenExport.add(jmitExportAsm);
		JMenuItem jmitExportTIArtistRAW = new JMenuItem("TI Artist (RAW)..."); jmitExportTIArtistRAW.setActionCommand(CMD_XPTIARAW); jmitExportTIArtistRAW.addActionListener(this); jmenExport.add(jmitExportTIArtistRAW);
		JMenuItem jmitExportTIArtistTifiles = new JMenuItem("TI Artist (TIFILES)..."); jmitExportTIArtistTifiles.setActionCommand(CMD_XPTIAFIL); jmitExportTIArtistTifiles.addActionListener(this); jmenExport.add(jmitExportTIArtistTifiles);
		JMenuItem jmitExportTIArtistV9T9 = new JMenuItem("TI Artist (V9T9)..."); jmitExportTIArtistV9T9.setActionCommand(CMD_XPTIAV9); jmitExportTIArtistV9T9.addActionListener(this); jmenExport.add(jmitExportTIArtistV9T9);
		jmenExport.addSeparator();
		JMenuItem jmitExportBitmapImg = new JMenuItem("Color Image..."); jmitExportBitmapImg.setActionCommand(Globals.CMD_XPMAPIMG); jmitExportBitmapImg.addActionListener(this); jmenExport.add(jmitExportBitmapImg);
		jMenuBar.add(jmenExport);

		jmenSelection = new JMenu("Selection"); jmenSelection.setEnabled(false); 
		JMenuItem jmitSaveSel = new JMenuItem("Save..."); jmitSaveSel.setActionCommand(CMD_SAVESEL); jmitSaveSel.addActionListener(this); jmenSelection.add(jmitSaveSel);
		JMenuItem jmitSaveSelASM = new JMenuItem("Assembler..."); jmitSaveSelASM.setActionCommand(CMD_XPASMSEL); jmitSaveSelASM.addActionListener(this); jmenSelection.add(jmitSaveSelASM);
//		JMenuItem jmitSaveSelTIArtistRAW = new JMenuItem("TI Artist (RAW)..."); jmitSaveSelTIArtistRAW.setActionCommand(CMD_XPTIASEL); jmitSaveSelTIArtistRAW.addActionListener(this); jmenSelection.add(jmitSaveSelTIArtistRAW);
		jMenuBar.add(jmenSelection);

		JMenu jmenOptions = new JMenu("Options");
		JMenuItem jmitShowPos = new JCheckBoxMenuItem("Show Position", bitmMain.showPosIndic()); jmitShowPos.setActionCommand(Globals.CMD_SHOWPOS); jmitShowPos.addActionListener(this); jmenOptions.add(jmitShowPos);

		jMenuBar.add(jmenOptions);

		// Assemble the application
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(bitmMain, BorderLayout.CENTER);
		this.getContentPane().add(jMenuBar, BorderLayout.NORTH);
		this.addWindowListener(this);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.pack();
		this.setVisible(true);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);

		updateComponents(true);
	}

// Listeners -------------------------------------------------------------------------------/

	/* ActionListener methods */
	public void actionPerformed(ActionEvent ae)
	{
		try
		{
			String command = ae.getActionCommand();
			if(command.equals(Globals.CMD_EXIT))
			{
				exitApp(0);
			}
			else if(command.equals(Globals.CMD_NEW))
			{
				int userResponse = confirmationAction(this, "Confirm New Project", "This will delete all current data.\n\rAre you sure?");
				if(userResponse == JOptionPane.YES_OPTION)
				{
					newProject();
				}
			}
			else if(command.equals(Globals.CMD_OPEN))
			{
				openDataFile();
			}
			else if(command.equals(Globals.CMD_SAVE))
			{
				saveDataFile(false);
			}
			else if(command.equals(Globals.CMD_SAVEAS))
			{
				saveDataFile(true);
			}
			else if(command.equals(CMD_SAVESEL))
			{
				saveDataFileSelection();
			}
			else if(command.equals(Globals.CMD_APPEND))
			{
				appendDataFile();
			}
			else if(command.equals(Globals.CMD_XPASM))
			{
				exportAssemblerFile(false);
			}
			else if(command.equals(CMD_XPASMSEL))
			{
				exportAssemblerFile(true);
			}
			else if(command.equals(Globals.CMD_XPMAPIMG))
			{
				exportBitmapImage();
			}
			else if(command.equals(CMD_XPTIARAW))
			{
				exportTIArtist(HEADER_TYPE_NONE, false);
			}
			else if(command.equals(CMD_XPTIASEL))
			{
				exportTIArtist(HEADER_TYPE_NONE, true);
			}
			else if(command.equals(CMD_XPTIAFIL))
			{
				exportTIArtist(HEADER_TYPE_TIFILES, false);
			}
			else if(command.equals(CMD_XPTIAV9))
			{
				exportTIArtist(HEADER_TYPE_V9T9, false);
			}
			else if(command.equals(Globals.CMD_MPIMGCL))
			{
				importImage();
			}
			else if(command.equals(CMD_MPTIARAW))
			{
				importTIArtist(false);
			}
			else if(command.equals(CMD_MPTIAHDR))
			{
				importTIArtist(true);
			}
			else if(command.equals(Globals.CMD_SHOWPOS))
			{
				bitmMain.toggleShowPosIndic();
			}
			bitmMain.redrawCanvas();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	/* WindowListener methods */
	public void windowClosing(WindowEvent we)
	{
		exitApp(0);
	}
	public void windowOpened(WindowEvent we)      { ; }
	public void windowClosed(WindowEvent we)      { ; }
	public void windowActivated(WindowEvent we)   { ; }
	public void windowDeactivated(WindowEvent we) { ; }
	public void windowIconified(WindowEvent we)   { ; }
	public void windowDeiconified(WindowEvent we) { ; }

	/* MouseListener methods */
	public void mousePressed(MouseEvent me)
	{
		if(!bitmMain.getHotPoint().equals(BitmapCanvas.PT_OFFGRID))
		{
			bitmMain.requestFocus();
		}
		updateComponents();
	}
	public void mouseReleased(MouseEvent me) {}
	public void mouseClicked(MouseEvent me) {}
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}

	/* MouseMotionListener methods */
	public void mouseMoved(MouseEvent me)
	{
		updateComponents();
	}
	public void mouseDragged(MouseEvent me)
	{
		if(!bitmMain.getHotPoint().equals(BitmapCanvas.PT_OFFGRID))
		{
			bitmMain.requestFocus();
		}
		updateComponents();
	}

// Component Builder Methods ---------------------------------------------------------------/

	protected JPanel getPanel(LayoutManager layout)
	{
		JPanel jpnlRtn = new JPanel(layout);
		jpnlRtn.setOpaque(true);
		jpnlRtn.setBackground(Globals.CLR_COMPONENTBACK);
		return jpnlRtn;
	}

	protected Image getImage(String name)
	{
		return Toolkit.getDefaultToolkit().getImage(name);
	}

	protected ImageIcon getIcon(String name)
	{
		URL imageURL = getClass().getResource("images/icon_" + name + "_mono.png");
		return new ImageIcon(Toolkit.getDefaultToolkit().getImage(imageURL));
	}

	protected JButton getToolButton(String buttonkey, String tooltip, Color bgcolor)
	{
		JButton jbtnTool = new JButton(getIcon(buttonkey));
		jbtnTool.setActionCommand(buttonkey);
		jbtnTool.addActionListener(this);
		jbtnTool.setToolTipText(tooltip);
		jbtnTool.setMargin(new Insets(0, 0, 0, 0));
		jbtnTool.setBackground(bgcolor);
		jbtnTool.setPreferredSize(Globals.DM_TOOL);
		return jbtnTool;
	}

	protected JButton getToolButton(String buttonkey, String tooltip)
	{
		return getToolButton(buttonkey, tooltip, Globals.CLR_BUTTON_NORMAL);
	}

	protected JLabel getLabel(String text, int align, Color clrback)
	{
		JLabel jlblRtn = new JLabel(text, align);
		jlblRtn.setOpaque(true);
		jlblRtn.setBackground(clrback);
		return jlblRtn;
	}

	protected JLabel getLabel(String text, int align)
	{
		return getLabel(text, align, Globals.CLR_COMPONENTBACK);
	}

// Data Transformation Methods -------------------------------------------------------------/

	protected int[][] shiftGrid(int[][] grid, int xshift, int yshift)
	{
		int y = (yshift < 0 ? grid.length - 1 : 0);
		while(y >= 0 && y <= (grid.length - 1))
		{
			int getY = y + yshift;
			int x = (xshift < 0 ? grid[y].length - 1 : 0);
			while(x >= 0 && x <= (grid[y].length - 1))
			{
				int getX = x + xshift;
				if(getY >= 0 && getY < grid.length && getX >= 0 && getX < grid[y].length)
				{
					grid[y][x] = grid[getY][getX];
				}
				else
				{
					grid[y][x] = 0;
				}
				x = x + (xshift < 0 ? -1 : 1);
			}
			y = y + (yshift < 0 ? -1 : 1);
		}
		return grid;
	}

// File Handling Methods -------------------------------------------------------------------/

	protected void openDataFile()
	{
		int charNum = TIGlobals.MINCHAR;
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, FILEEXTS, "Bitmap Data Files");
		if(whatFile != null)
		{
			newProject();
			mapDataFile = whatFile;
			this.setTitle(APPTITLE + " : " + mapDataFile.getName());
			readDataFile(mapDataFile);
		}
		bitmMain.goToMap(0);
		bitmMain.clearCloneBuffer();
		updateComponents(true);
	}

	protected void saveDataFile(boolean askForFilename)
	{
		if(askForFilename || mapDataFile == null)
		{
			File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, FILEEXTS, "Bitmap Data Files");
			if(whatFile != null)
			{
				if(!whatFile.getAbsolutePath().toLowerCase().endsWith("." + FILEEXT))
				{
					whatFile = new File(whatFile.getAbsolutePath() + "." + FILEEXT);
				}
				mapDataFile = whatFile;
			}
		}
		if(mapDataFile != null)
		{
			this.setTitle(APPTITLE + " : " + mapDataFile.getName());
			writeDataFile(mapDataFile);
		}
	}

	protected void saveDataFileSelection()
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, FILEEXTS, "Bitmap Data Files");
		if(whatFile != null)
		{
			if(!whatFile.getAbsolutePath().toLowerCase().endsWith("." + FILEEXT))
			{
				whatFile = new File(whatFile.getAbsolutePath() + "." + FILEEXT);
			}
		}
		if(whatFile != null)
		{
			writeDataFileSelection(whatFile);
		}
	}

	protected void appendDataFile()
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, FILEEXTS, "Bitmap Data Files");
		if(whatFile != null)
		{
			readAppendDataFile(whatFile);
		}
		updateComponents();
	}

	protected void importImage()
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, IMGEXTS, "Image Files", true);
		if(whatFile != null)
		{
			Image loadImg = getImage(whatFile.getAbsolutePath());
			int imgW = loadImg.getWidth(this);
			int imgH = loadImg.getHeight(this);
			BufferedImage buffImg = new BufferedImage(imgW, imgH, BufferedImage.TYPE_3BYTE_BGR);
			while(buffImg == null) { try { Thread.sleep(50); } catch(InterruptedException ie) {} }
			Graphics2D g2d = ((Graphics2D)(buffImg.getGraphics()));
			g2d.setColor(TIGlobals.TI_COLOR_TRANSOPAQUE);
			g2d.fillRect(0, 0, imgW, imgH);
			g2d.setComposite(AlphaComposite.SrcOver);
			g2d.drawImage(loadImg, 0, 0, this);
			g2d.setComposite(AlphaComposite.Src);
			readImage(buffImg);
		}
		updateComponents();
	}

	protected void importTIArtist(boolean hasHeader)
	{
		File bitdataFile = (File)null;
		File colordataFile = (File)null;
		File firstFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, TIARTEXTS, "TI Artist Files", true);
		if(firstFile != null)
		{
			if(firstFile.getName().toLowerCase().endsWith(TIARTPIXEXT))
			{
				String baseFilename = firstFile.getName().substring(0, firstFile.getName().toLowerCase().indexOf(TIARTPIXEXT));
				bitdataFile = firstFile;
				colordataFile = new File(firstFile.getParent() + File.separator + baseFilename + TIARTCLREXT);
				if(!colordataFile.exists())
				{
					colordataFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, TIARTCLREXTS, "TI Artist Color Data Files", true);
				}
			}
			else if(firstFile.getName().toLowerCase().endsWith(TIARTCLREXT))
			{
				String baseFilename = firstFile.getName().substring(0, firstFile.getName().toLowerCase().indexOf(TIARTCLREXT));
				colordataFile = firstFile;
				bitdataFile = new File(firstFile.getParent() + File.separator + baseFilename + TIARTPIXEXT);
				if(!bitdataFile.exists())
				{
					bitdataFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, TIARTPIXEXTS, "TI Artist Pixel Data Files", true);
				}
			}
		}
		if(bitdataFile != null && colordataFile != null)
		{
			bitmMain.setGridCols(BITMAP_COLS);
			bitmMain.setGridRows(BITMAP_ROWS);
			try
			{
				FileInputStream fib = new FileInputStream(bitdataFile);
				int imgW = 256;
				int imgH = 192;
				int c = 0;
				int r = 0;
				int b = 0;
				int readInt;
				if(hasHeader)
				{
					for(int i = 0; i < HEADER_SIZE; i++)
					{
						fib.read();
					}
				}
				while((readInt = fib.read()) != -1)
				{
					bitmMain.setGridByte(c, r + b, getReversedByte(readInt));
					b++;
					if(b > 7)
					{
						b = 0;
						c++;
						if(c >= (imgW / 8))
						{
							c = 0;
							r += 8;
						}
					}
				}
				fib.close();
				FileInputStream fic = new FileInputStream(colordataFile);
				c = 0;
				r = 0;
				b = 0;
				if(hasHeader)
				{
					for(int i = 0; i < HEADER_SIZE; i++)
					{
						fic.read();
					}
				}
				while((readInt = fic.read()) != -1)
				{
					bitmMain.setColorByte(c, r + b, readInt);
					b++;
					if(b > 7)
					{
						b = 0;
						c++;
						if(c >= (imgW / 8))
						{
							c = 0;
							r += 8;
						}
					}
				}
				fic.close();
			}
			catch(Exception e) {}
		}
		updateComponents(true);
	}

	protected void readImage(BufferedImage buffImg)
	{
		int imgWidth = buffImg.getWidth();
		int imgHeight = buffImg.getHeight();
		int imgCols = (int)(Math.ceil(imgWidth / 8));
		int imgRows = imgHeight;
		bitmMain.setGridCols(imgCols);
		bitmMain.setGridRows(imgRows);
		bitmMain.clearGrid();
		for(int r = 0; r < imgRows; r++)
		{
			for(int c = 0; c < imgCols; c++)
			{
				// get color byte
				int[] counta = new int[17];
				for(int b = 0; b < 8; b++)
				{
					int pixelColor = buffImg.getRGB((c * 8) + b, r);
					int tiColor = getTIColorForPixel(pixelColor);
					if(tiColor > 0) { counta[tiColor]++; }
				}
				int priFreq = -1;
				int priNdex = -1;
				int secFreq = -1;
				int secNdex = -1;
				for(int i = 0; i < counta.length; i++)
				{
					if(counta[i] > priFreq)
					{
						secFreq = priFreq;
						secNdex = priNdex;
						priFreq = counta[i];
						priNdex = i;
					}
					else if(counta[i] > secFreq)
					{
						secFreq = counta[i];
						secNdex = i;
					}
				}
				int colorFore = Math.max(priNdex, secNdex);
				int colorBack = Math.min(priNdex, secNdex);
				if(colorFore == -1) { colorFore = 0; }
				if(colorBack == -1) { colorBack = 0; }
				int colorByte = (colorFore<<4 | colorBack);
				bitmMain.setColorByte(c, r, colorByte);
				// get data byte
				int dataByte = 0;
				for(int b = 0; b < 8; b++)
				{
					int pixelColor = buffImg.getRGB((c * 8) + b, r);
					int tiColor = getTIColorForPixel(pixelColor);
					if(tiColor == colorFore) { dataByte = (dataByte | 1<<b); }
				}
				bitmMain.setGridByte(c, r, dataByte);
			}
		}
		updateComponents(true);
	}

	protected void exportAssemblerFile(boolean isSelection)
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, ASMEXTS, "Assembler Source Files");
		if(whatFile != null)
		{
			boolean isExtensionAdded = false;
			for(int ex = 0; ex < ASMEXTS.length; ex++)
			{
				if(whatFile.getAbsolutePath().toLowerCase().endsWith("." + ASMEXTS[ex]))
				{
					isExtensionAdded = true;
				}
			}
			if(!isExtensionAdded)
			{
				whatFile = new File(whatFile.getAbsolutePath() + "." + ASMEXT);
			}
//			TadatakaExportDialog exporter = new TadatakaExportDialog(TadatakaExportDialog.TYPE_ASM, this, bExportComments);
//			if(exporter.isOkay())
//			{
//				bExportComments = exporter.includeComments();
				bExportComments = false;
				if(isSelection)
				{
					writeASMDataFile(whatFile, bitmMain.getCloneBitmapArray(), bitmMain.getCloneColorArray(), bExportComments);
				}
				else
				{
					writeASMDataFile(whatFile, bitmMain.getBitmapData(), bitmMain.getColorData(), bExportComments);
				}
//			}
//			exporter.dispose();
		}
	}

	protected void exportBitmapImage()
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, IMGEXTS, "Image Files");
		if(whatFile != null)
		{
			boolean isExtensionAdded = false;
			for(int ex = 0; ex < IMGEXTS.length; ex++)
			{
				if(whatFile.getAbsolutePath().toLowerCase().endsWith("." + IMGEXTS[ex]))
				{
					isExtensionAdded = true;
				}
			}
			if(!isExtensionAdded)
			{
				whatFile = new File(whatFile.getAbsolutePath() + "." + IMGEXT);
			}
			writeMapImage(whatFile);
		}
	}

	protected void exportTIArtist(int headerType, boolean isSelection)
	{
		File saveFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, TIARTEXTS, "TI Artist Files", true);
		if(saveFile != null)
		{
			String baseFilename = new String(saveFile.getName());
			if(baseFilename.toLowerCase().indexOf("." + TIARTPIXEXT) > -1 )
			{
				baseFilename = baseFilename.substring(0, baseFilename.toLowerCase().indexOf("." + TIARTPIXEXT));
			}
			if(baseFilename.toLowerCase().indexOf("." + TIARTCLREXT) > -1 )
			{
				baseFilename = baseFilename.substring(0, baseFilename.toLowerCase().indexOf("." + TIARTCLREXT));
			}
			File bitdataFile = new File(saveFile.getParent() + File.separator + baseFilename + "." + TIARTPIXEXT);
			File colordataFile = new File(saveFile.getParent() + File.separator + baseFilename + "." + TIARTCLREXT);
			if(bitdataFile != null && colordataFile != null)
			{
				if(isSelection)
				{
					writeTIArtistFiles(bitdataFile, bitmMain.getCloneBitmapArray(), colordataFile, bitmMain.getCloneColorArray(), headerType);
				}
				else
				{
					writeTIArtistFiles(bitdataFile, bitmMain.getBitmapData(), colordataFile, bitmMain.getColorData(), headerType);
				}
			}
		}
	}

	private File getFileFromChooser(String startDir, int dialogType, String[] exts, String desc, boolean hasImagePreview)
	{
		JFileChooser jfileDialog = new JFileChooser(startDir);
		jfileDialog.setDialogType(dialogType);
		jfileDialog.setFileFilter(new MutableFilter(exts, desc));
		if(hasImagePreview)
		{
			jfileDialog.setAccessory(new ImagePreview(jfileDialog));
		}
		int optionSelected = JFileChooser.CANCEL_OPTION;
		if(dialogType == JFileChooser.OPEN_DIALOG)
		{
			optionSelected = jfileDialog.showOpenDialog(this);
		}
		else if(dialogType == JFileChooser.SAVE_DIALOG)
		{
			optionSelected = jfileDialog.showSaveDialog(this);
		}
		else // default to an OPEN_DIALOG
		{
			optionSelected = jfileDialog.showOpenDialog(this);
		}
		if(optionSelected == JFileChooser.APPROVE_OPTION)
		{
			return jfileDialog.getSelectedFile();
		}
		return (File)null;
	}

	private File getFileFromChooser(String startDir, int dialogType, String[] exts, String desc)
	{
		return getFileFromChooser(startDir, dialogType, exts, desc, false);
	}

	protected void readDataFile(File mapDataFile)
	{
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(mapDataFile));
			String lineIn = "";
			int mapY = 0;
			int mapX = 0;
			int mapCount = 1;
			int mapColor = 15;
			int mapCols = 32;
			int mapRows = 192;
			int currMap = 0;
			do
			{
				lineIn = br.readLine();
				if(lineIn == null)
				{
					break;
				}
				else
				{
					if(lineIn.startsWith(Globals.KEY_MAPCOUNT))
					{
						lineIn = lineIn.substring(Globals.KEY_MAPCOUNT.length());
						mapCount = Integer.parseInt(lineIn);
					}
					else if(lineIn.startsWith(Globals.KEY_MAPSIZE))
					{
						lineIn = lineIn.substring(Globals.KEY_MAPSIZE.length());
						mapCols = Integer.parseInt(lineIn.substring(0, lineIn.indexOf("|")));
						mapRows = Integer.parseInt(lineIn.substring(lineIn.indexOf("|") + 1));
						bitmMain.setGridCols(mapCols);
						bitmMain.setGridRows(mapRows);
					}
					else if(lineIn.equals(Globals.KEY_MAPSTART))
					{
						if(mapY > 0)
						{
							bitmMain.addBlankMap(mapCols, mapRows);
							currMap++;
							bitmMain.setCurrentMapId(currMap);
							mapY = 0;
						}
					}
					else if(lineIn.equals(Globals.KEY_MAPEND))
					{
						bitmMain.storeCurrentMap();
					}
					else if(lineIn.startsWith(Globals.KEY_MAPDATA))
					{
						if(mapY >= mapRows)
						{
							bitmMain.storeCurrentMap();
							bitmMain.addBlankMap(mapCols, mapRows);
							currMap++;
							bitmMain.setCurrentMapId(currMap);
							mapY = 0;
						}
						lineIn = lineIn.substring(Globals.KEY_MAPDATA.length());
						StringTokenizer stParse = new StringTokenizer(lineIn, "|", false);
						while(stParse.hasMoreTokens())
						{
							String sVal = stParse.nextToken();
							int mByte = Integer.parseInt(sVal.substring(0, sVal.indexOf(":")));
							int cByte = Integer.parseInt(sVal.substring(sVal.indexOf(":") + 1));
							bitmMain.setGridByte(mapX, mapY, mByte);
							bitmMain.setColorByte(mapX, mapY, cByte);
							mapX++;
						}
						mapX = 0;
						mapY++;
					}
				}
			} while (lineIn != null);
			br.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	protected void readAppendDataFile(File mapDataFile)
	{
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(mapDataFile));
			String lineIn = "";
			int mapY = 0;
			int mapX = 0;
			int mapCount = 1;
			int mapColor = 15;
			int mapCols = 32;
			int mapRows = 192;
			int currMap = bitmMain.getMapCount();
			do
			{
				lineIn = br.readLine();
				if(lineIn == null)
				{
					break;
				}
				else
				{
					if(lineIn.startsWith(Globals.KEY_COLORSET))
					{
					}
					else if(lineIn.startsWith(Globals.KEY_CHARDATA))
					{
					}
					else if(lineIn.startsWith(Globals.KEY_MAPCOUNT))
					{
						lineIn = lineIn.substring(Globals.KEY_MAPCOUNT.length());
						mapCount = Integer.parseInt(lineIn);
					}
					else if(lineIn.startsWith(Globals.KEY_MAPSIZE))
					{
						lineIn = lineIn.substring(Globals.KEY_MAPSIZE.length());
						mapCols = Integer.parseInt(lineIn.substring(0, lineIn.indexOf("|")));
						mapRows = Integer.parseInt(lineIn.substring(lineIn.indexOf("|") + 1));
					}
					else if(lineIn.equals(Globals.KEY_MAPSTART))
					{
						if(mapY > 0)
						{
							bitmMain.addBlankMap(mapCols, mapRows);
							currMap++;
							bitmMain.setCurrentMapId(currMap);
							mapY = 0;
						}
					}
					else if(lineIn.equals(Globals.KEY_MAPEND))
					{
						bitmMain.storeCurrentMap();
					}
					else if(lineIn.startsWith(Globals.KEY_MAPDATA))
					{
						if(currMap == bitmMain.getMapCount())
						{
							bitmMain.storeCurrentMap();
							bitmMain.addBlankMap(mapCols, mapRows);
							bitmMain.setCurrentMapId(currMap);
							mapY = 0;
						}
						else if(mapY >= mapRows)
						{
							bitmMain.storeCurrentMap();
							bitmMain.addBlankMap();
							currMap++;
							bitmMain.setCurrentMapId(currMap);
							mapY = 0;
						}
						lineIn = lineIn.substring(Globals.KEY_MAPDATA.length());
						StringTokenizer stParse = new StringTokenizer(lineIn, "|", false);
						while(stParse.hasMoreTokens())
						{
							String sVal = stParse.nextToken();
							bitmMain.setGridByte(mapX, mapY, Integer.parseInt(sVal));
							mapX++;
						}
						mapX = 0;
						mapY++;
					}
				}
			} while (lineIn != null);
			br.close();
			updateComponents(true);
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	protected void writeDataFile(File mapDataFile)
	{
		try
		{
			// store working bitmap first
			bitmMain.storeCurrentMap();
			// get file output buffer
			BufferedWriter bw = new BufferedWriter(new FileWriter(mapDataFile));
			// save map parameters
			bw.write("* BITMAPS"); bw.newLine();
			bw.write(Globals.KEY_MAPCOUNT + bitmMain.getMapCount());
			bw.newLine();
			// save map(s)
			for(int m = 0; m < bitmMain.getMapCount(); m++)
			{
				bw.write("* BITMAP #" + (m + 1)); bw.newLine();
				bw.write(Globals.KEY_MAPSTART);
				bw.newLine();
				int[][] pixelsToSave = bitmMain.getMapData(m);
				int[][] colorsToSave = bitmMain.getColorData(m);
				bw.write(Globals.KEY_MAPSIZE + pixelsToSave[0].length + "|" + pixelsToSave.length);
				bw.newLine();
				for(int y = 0; y < pixelsToSave.length; y++)
				{
					bw.write(Globals.KEY_MAPDATA);
					for(int x = 0; x < pixelsToSave[y].length; x++)
					{
						bw.write((x > 0 ? "|" : "") + pixelsToSave[y][x] + ":" + colorsToSave[y][x]);
					}
					bw.newLine();
				}
				bw.write(Globals.KEY_MAPEND);
				bw.newLine();
			}
			bw.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
		updateComponents(true);
	}

	protected void writeDataFileSelection(File mapDataFile)
	{
		try
		{
			// store working bitmap first
			bitmMain.storeCurrentMap();
			// get file output buffer
			BufferedWriter bw = new BufferedWriter(new FileWriter(mapDataFile));
			// save selection
			bw.write("* BITMAPS"); bw.newLine();
			bw.write(Globals.KEY_MAPCOUNT + "1"); bw.newLine();
			bw.write("* BITMAP #1"); bw.newLine();
			bw.write(Globals.KEY_MAPSTART);
			bw.newLine();
			int[][] pixelsToSave = bitmMain.getCloneBitmapArray();
			int[][] colorsToSave = bitmMain.getCloneColorArray();
			if(pixelsToSave != null && colorsToSave != null)
			{
				bw.write(Globals.KEY_MAPSIZE + pixelsToSave[0].length + "|" + pixelsToSave.length);
				bw.newLine();
				for(int y = 0; y < pixelsToSave.length; y++)
				{
					bw.write(Globals.KEY_MAPDATA);
					for(int x = 0; x < pixelsToSave[y].length; x++)
					{
						bw.write((x > 0 ? "|" : "") + pixelsToSave[y][x] + ":" + colorsToSave[y][x]);
					}
					bw.newLine();
				}
				bw.write(Globals.KEY_MAPEND);
				bw.newLine();
			}
			bw.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
		updateComponents(true);
	}

	protected void writeASMDataFile(File mapDataFile, int[][] pixelsToSave, int[][] colorsToSave, boolean includeComments)
	{
		int itemCount = 0;
		StringBuffer sbLine = new StringBuffer();
		int charDataStart = 0;
		int charCount = 0;
		int mapDataStart = 0;
		int mapCols = 0;
		int mapRows = 0;
		StringBuffer sbOutLine = new StringBuffer();
		try
		{
			bitmMain.storeCurrentMap();
			BufferedWriter bw = new BufferedWriter(new FileWriter(mapDataFile));
			if(includeComments)
			{
				printPaddedLine(bw, "****************************************", false);
				printPaddedLine(bw, "* Bitmap Data", false);
				printPaddedLine(bw, "****************************************", false);
			}
			sbLine.delete(0, sbLine.length());
			boolean isFirstByte = true;
			String hexChunk = new String();
			sbLine.append("BSIZE  DATA >");
			hexChunk = Integer.toHexString(pixelsToSave[0].length).toUpperCase();
			sbLine.append((hexChunk.length() < 1 ? "00" : (hexChunk.length() < 2 ? "0" : "")) + hexChunk);
			hexChunk = Integer.toHexString(pixelsToSave.length).toUpperCase();
			sbLine.append((hexChunk.length() < 1 ? "00" : (hexChunk.length() < 2 ? "0" : "")) + hexChunk);
			printPaddedLine(bw, sbLine.toString(), includeComments);
			sbLine.delete(0, sbLine.length());
			isFirstByte = true;
			for(int y = 0; y < pixelsToSave.length; y++)
			{
				if(includeComments)
				{
					printPaddedLine(bw, "* -- Pixel Row " + y + " -- ", false);
				}
				for(int cl = 0; cl < Math.ceil(pixelsToSave[y].length / 8); cl++)
				{
					if(y == 0 && cl == 0)
					{
						sbLine.append("BPIXEL DATA ");
					}
					else
					{
						sbLine.append("       DATA ");
					}
					for(int colpos = (cl * 8); colpos < Math.min((cl + 1) * 8, pixelsToSave[y].length); colpos++)
					{
						if(isFirstByte)
						{
							if(colpos > (cl * 8))
							{
								sbLine.append(",");
							}
							sbLine.append(">");
						}
						hexChunk = Integer.toHexString(pixelsToSave[y][colpos]).toUpperCase();
						sbLine.append((hexChunk.length() < 1 ? "00" : (hexChunk.length() < 1 ? "0" : "")) + hexChunk);
						isFirstByte = !isFirstByte;
					}
					printPaddedLine(bw, sbLine.toString(), includeComments);
					sbLine.delete(0, sbLine.length());
				}
			}
			for(int y = 0; y < colorsToSave.length; y++)
			{
				if(includeComments)
				{
					printPaddedLine(bw, "* -- Color Row " + y + " -- ", false);
				}
				for(int cl = 0; cl < Math.ceil(colorsToSave[y].length / 8); cl++)
				{
					if(y == 0 && cl == 0)
					{
						sbLine.append("BCOLOR DATA ");
					}
					else
					{
						sbLine.append("       DATA ");
					}
					for(int colpos = (cl * 8); colpos < Math.min((cl + 1) * 8, colorsToSave[y].length); colpos++)
					{
						if(isFirstByte)
						{
							if(colpos > (cl * 8))
							{
								sbLine.append(",");
							}
							sbLine.append(">");
						}
						hexChunk = Integer.toHexString(colorsToSave[y][colpos]).toUpperCase();
						sbLine.append((hexChunk.length() < 1 ? "00" : (hexChunk.length() < 1 ? "0" : "")) + hexChunk);
						isFirstByte = !isFirstByte;
					}
					printPaddedLine(bw, sbLine.toString(), includeComments);
					sbLine.delete(0, sbLine.length());
				}
			}
			bw.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	protected void writeTIArtistFiles(File bitdataFile, int[][] pixelsToSave, File colordataFile, int[][] colorsToSave, int headerType)
	{
		try
		{
			FileOutputStream fob = new FileOutputStream(bitdataFile);
			if(headerType != HEADER_TYPE_NONE)
			{
				for(int i = 0; i < HEADER_SIZE; i++)
				{
					if(headerType == HEADER_TYPE_TIFILES && i < HEADER_TIFILES.length)
					{
						fob.write(HEADER_TIFILES[i]);
					}
					else if(headerType == HEADER_TYPE_V9T9 && i < HEADER_V9T9_P.length)
					{
						fob.write(HEADER_V9T9_P[i]);
					}
					else
					{
						fob.write(0);
					}
				}
			}
			for(int y = 0; y < Math.ceil(pixelsToSave.length / 8); y++)
			{
				for(int x = 0; x < pixelsToSave[y].length; x++)
				{
					for(int b = 0; b < 8; b++)
					{
						fob.write(getReversedByte(pixelsToSave[(y * 8) + b][x]));
					}
				}
				fob.flush();
			}
			fob.flush();
			fob.close();
			FileOutputStream foc = new FileOutputStream(colordataFile);
			if(headerType != HEADER_TYPE_NONE)
			{
				for(int i = 0; i < HEADER_SIZE; i++)
				{
					if(headerType == HEADER_TYPE_TIFILES && i < HEADER_TIFILES.length)
					{
						foc.write(HEADER_TIFILES[i]);
					}
					else if(headerType == HEADER_TYPE_V9T9 && i < HEADER_V9T9_C.length)
					{
						foc.write(HEADER_V9T9_C[i]);
					}
					else
					{
						foc.write(0);
					}
				}
			}
			for(int y = 0; y < Math.ceil(colorsToSave.length / 8); y++)
			{
				for(int x = 0; x < colorsToSave[y].length; x++)
				{
					for(int b = 0; b < 8; b++)
					{
						foc.write(colorsToSave[(y * 8) + b][x]);
					}
				}
				foc.flush();
			}
			foc.flush();
			foc.close();
		}
		catch(Exception e) {}
	}

	protected void printPaddedLine(BufferedWriter bw, String str, boolean isCommand, int padlen)
	throws IOException
	{
		if(str.length() < padlen)
		{
			StringBuffer sbOut = new StringBuffer();
			sbOut.append(str);
			for(int i = str.length(); i < padlen; i++)
			{
				if((i == (padlen - 1)) && isCommand)
				{
					sbOut.append(";");
				}
				else
				{
					sbOut.append(" ");
				}
			}
			bw.write(sbOut.toString());
		}
		else
		{
			bw.write(str);
		}
		bw.newLine();
	}

	protected void printPaddedLine(BufferedWriter bw, String str, boolean isCommand)
	throws IOException
	{
		printPaddedLine(bw, str, isCommand, ASM_LINELEN);
	}

	protected void printPaddedLine(BufferedWriter bw, String str)
	throws IOException
	{
		printPaddedLine(bw, str, true);
	}

	protected void writeMapImage(File imageOut)
	{
		try
		{
			updateComponents();
			ImageIO.write(bitmMain.getBuffer(), "png", imageOut);
		}
		catch(Exception e)
		{
			e.printStackTrace(System.out);
		}
	}

	private void savePreferences()
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(new File("Tadataka.prefs"));
			appProperties.setProperty("magnif", "" + bitmMain.getViewScale());
			appProperties.setProperty("exportComments", (bExportComments ? "true" : "false"));
			appProperties.setProperty("showGrid", (bitmMain.isShowGrid() ? "true" : "false"));
			appProperties.setProperty("showPosition", (bitmMain.showPosIndic() ? "true" : "false"));
			appProperties.store(fos, (String)null);
			fos.close();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace(System.out);
		}
	}

// Class Utility Methods -------------------------------------------------------------------/

	protected void newProject()
	{
		int cellNum = 0;
		bitmMain.delAllMaps();
		bitmMain.clearCloneBuffer();
		mapDataFile = (File)null;
		updateComponents(true);
	}

	protected int confirmationAction(Frame parent, String title, String message) 
	{
		return JOptionPane.showConfirmDialog(parent, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
	}

// Convenience Methods ---------------------------------------------------------------------/

	protected String getCharKey(int charnum)
	{
		return ("ch" + charnum);
	}

	protected void updateComponents(boolean forceRedraw)
	{
		bitmMain.updateComponents(forceRedraw);
		jmenSelection.setEnabled(bitmMain.getCloneBitmapArray() != null);
	}

	protected void updateComponents() { updateComponents(false); }

	protected int getTIColorForPixel(int pixelRBG)
	{
		for(int c = 0; c < TIGlobals.TI_PALETTE_OPAQUE.length; c++)
		{
			if(pixelRBG == TIGlobals.TI_PALETTE_OPAQUE[c].getRGB())
			{
				return c;
			}
		}
		return 0;
	}

	protected int getReversedByte(int byt)
	{
		int newByt = 0;
		for(int i = 0; i < 8; i++)
		{
			if((byt & 1<<i) == 1<<i)
			{
				newByt = (newByt | (1<<(7-i)));
			}
		}
		return (newByt & 0xFF);
	}

	public void exitApp(int status)
	{
		if(status == 0) { savePreferences(); }
		this.dispose();
		System.exit(status);
	}

// Main Method -----------------------------------------------------------------------------/

	public static void main(String[] args)
	{
		Tadataka tadataka = new Tadataka();
	}

}
