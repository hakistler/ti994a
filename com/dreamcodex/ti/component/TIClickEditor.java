package com.dreamcodex.ti.component;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.dreamcodex.ti.component.ClickEditor;
import com.dreamcodex.ti.util.TIGlobals;

public class TIClickEditor extends JPanel implements ItemListener, MouseListener, MouseMotionListener
{
// Listeners -------------------------------------------------------------------------------/

	protected MouseListener parentMouseListener;
	protected MouseMotionListener parentMouseMotionListener;

// Components ------------------------------------------------------------------------------/

	protected ClickEditor cEditor;
	protected JComboBox jcmbForeground;
	protected JComboBox jcmbBackground;

// Constructors ----------------------------------------------------------------------------/

	public TIClickEditor(int gridWidth, int gridHeight, int clrFore, int clrBack, MouseListener mlParent, MouseMotionListener mmlParent)
	{
		super(new BorderLayout(), true);
		this.setOpaque(true);

		cEditor = new ClickEditor(gridWidth, gridHeight, mlParent, mmlParent);
		jcmbForeground = new JComboBox(TIGlobals.TI_PALETTE_NAMES);
		jcmbForeground.addItemListener(this);
		jcmbForeground.setSelectedIndex(clrFore);
		cEditor.setColorDraw(TIGlobals.TI_PALETTE[clrFore]);
		jcmbBackground = new JComboBox(TIGlobals.TI_PALETTE_NAMES);
		jcmbBackground.addItemListener(this);
		jcmbBackground.setSelectedIndex(clrBack);
		cEditor.setColorBack(TIGlobals.TI_PALETTE[clrBack]);

		JPanel jpnlColors = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jpnlColors.setOpaque(false);
		jpnlColors.add(new JLabel("Foreground"));
		jpnlColors.add(jcmbForeground);
		jpnlColors.add(new JLabel("Background"));
		jpnlColors.add(jcmbBackground);

		this.add(cEditor, BorderLayout.CENTER);
		this.add(jpnlColors, BorderLayout.NORTH);

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		parentMouseListener = mlParent;
		parentMouseMotionListener = mmlParent;
	}

// Accessors -------------------------------------------------------------------------------/

	public ClickEditor getEditor()      { return cEditor; }
	public int         getForeNumber()  { return jcmbForeground.getSelectedIndex(); }
	public int         getBackNumber()  { return jcmbBackground.getSelectedIndex(); }
	public int         getTIColorFore() { return jcmbForeground.getSelectedIndex() + 1; }
	public int         getTIColorBack() { return jcmbBackground.getSelectedIndex() + 1; }

	public void clearGrid() { cEditor.clearGrid(); }
	public void fillGrid()  { cEditor.fillGrid(); }

// Pass-through Accessors For ClickEditor --------------------------------------------------/

	public int[][] getGridData()  { return cEditor.getGridData(); }
	public Color   getColorBack() { return cEditor.getColorBack(); }
	public Color   getColorDraw() { return cEditor.getColorDraw(); }
	public Color   getColorGrid() { return cEditor.getColorGrid(); }
	public Color   getColorGrup() { return cEditor.getColorGrup(); }
	public Color   getColorHigh() { return cEditor.getColorHigh(); }
	public int     getGridGroup() { return cEditor.getGridGroup(); }
	public boolean isPaintOn()    { return cEditor.isPaintOn(); }

	public void setGridData(int[][] gd) { cEditor.setGridData(gd); }
	public void setColorBack(Color clr) { cEditor.setColorBack(clr); }
	public void setColorDraw(Color clr) { cEditor.setColorDraw(clr); }
	public void setColorGrid(Color clr) { cEditor.setColorGrid(clr); }
	public void setColorGrup(Color clr) { cEditor.setColorGrup(clr); }
	public void setColorHigh(Color clr) { cEditor.setColorHigh(clr); }
	public void setGridGroup(int i)     { cEditor.setGridGroup(i); }
	public void setPaintOn(boolean b)   { cEditor.setPaintOn(b); }

	public void redrawCanvas() { cEditor.repaint(); }

// Listener Methods ------------------------------------------------------------------------/

	/* ItemListener methods */
	public void itemStateChanged(ItemEvent ie)
	{
		if(ie.getSource().equals(jcmbForeground))
		{
			cEditor.setColorDraw(TIGlobals.TI_PALETTE[jcmbForeground.getSelectedIndex()]);
			cEditor.repaint();
		}
		else if(ie.getSource().equals(jcmbBackground))
		{
			cEditor.setColorBack(TIGlobals.TI_PALETTE[jcmbBackground.getSelectedIndex()]);
			cEditor.repaint();
		}
	}

	/* MouseListener methods */
	public void mousePressed(MouseEvent me) { parentMouseListener.mousePressed(me); }
	public void mouseReleased(MouseEvent me) {}
	public void mouseClicked(MouseEvent me) {}
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}

	/* MouseMotionListener methods */
	public void mouseMoved(MouseEvent me) {}
	public void mouseDragged(MouseEvent me) { parentMouseMotionListener.mouseDragged(me); }

}

