package com.dreamcodex.ti.component;

import java.awt.Component;
import javax.swing.*;

import com.dreamcodex.ti.iface.IconProvider;

public class IconComboRenderer extends JLabel implements ListCellRenderer
{
	private IconProvider iconProv;

	public IconComboRenderer(IconProvider ipParent)
	{
		iconProv = ipParent;
		setOpaque(true);
		setHorizontalAlignment(LEFT);
		setVerticalAlignment(CENTER);
	}

	public Component getListCellRendererComponent(JList jlist, Object value, int index, boolean isSelected, boolean cellHasFocus)
	{
		this.setText(value.toString());
		this.setIcon(iconProv.getIconForObject(new Integer(index)));
		return this;
	}
}
