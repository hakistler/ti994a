package com.dreamcodex.ti.component;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import javax.swing.*;

import com.dreamcodex.ti.iface.IconProvider;
import com.dreamcodex.ti.util.TIGlobals;

public class CharacterSwapDialog extends JDialog implements ItemListener, PropertyChangeListener
{
	public static final String CHAR_NOTHING = "chrnone";
	public static final String CHAR_REPLACE = "chrrepl";
	public static final String CHAR_SWAP    = "chrswap";

	public static final String IMAGE_NOTHING = "imgnone";
	public static final String IMAGE_REPLACE = "imgrepl";
	public static final String IMAGE_SWAP    = "imgswap";

	private String OK_TEXT = "OK";
	private String CANCEL_TEXT = "Cancel";

	private JComboBox jcmbBaseChar;
	private JComboBox jcmbSwapChar;
	private JLabel jlblBaseChar;
	private JLabel jlblSwapChar;
	private ButtonGroup bgrpChars;
	private JRadioButton jradCharReplace;
	private JRadioButton jradCharSwap;
	private JRadioButton jradCharNothing;
	private ButtonGroup bgrpImage;
	private JRadioButton jradImageReplace;
	private JRadioButton jradImageSwap;
	private JRadioButton jradImageNothing;
	private JCheckBox jchkAllMaps;
	private IconProvider iconProv;
	private boolean clickedOkay = false;;
	private int minChar = 0;
	private int maxChar = 0;

	public CharacterSwapDialog(JFrame parent, IconProvider ip, int minc, int maxc)
	{
		super(parent, "Swap/Replace Characters", true);
		minChar = minc;
		maxChar = maxc;
		iconProv = ip;
		jcmbBaseChar = new JComboBox(); jcmbBaseChar.addItemListener(this);
		jcmbSwapChar = new JComboBox(); jcmbSwapChar.addItemListener(this);
		jlblBaseChar = new JLabel(); jlblBaseChar.setHorizontalAlignment(JLabel.CENTER);
		jlblSwapChar = new JLabel(); jlblSwapChar.setHorizontalAlignment(JLabel.CENTER);
		bgrpChars = new ButtonGroup();
		jradCharReplace = new JRadioButton("Replace", true); jradCharReplace.setActionCommand(CHAR_REPLACE); bgrpChars.add(jradCharReplace);
		jradCharSwap    = new JRadioButton("Swap");          jradCharSwap.setActionCommand(CHAR_SWAP);       bgrpChars.add(jradCharSwap);
		jradCharNothing = new JRadioButton("Do Nothing");    jradCharNothing.setActionCommand(CHAR_NOTHING); bgrpChars.add(jradCharNothing);
		bgrpImage = new ButtonGroup();
		jradImageReplace = new JRadioButton("Replace");          jradImageReplace.setActionCommand(IMAGE_REPLACE); bgrpImage.add(jradImageReplace);
		jradImageSwap    = new JRadioButton("Swap");             jradImageSwap.setActionCommand(IMAGE_SWAP);       bgrpImage.add(jradImageSwap);
		jradImageNothing = new JRadioButton("Do Nothing", true); jradImageNothing.setActionCommand(IMAGE_NOTHING); bgrpImage.add(jradImageNothing);
		jchkAllMaps = new JCheckBox("On All Maps", false);
		int chardex = 0;
		for(int i = minChar; i <= maxChar; i++)
		{
			chardex = i - TIGlobals.CHARMAPSTART;
			if(chardex >= 0 && chardex < TIGlobals.CHARMAP.length)
			{
				jcmbBaseChar.addItem(new String("" + TIGlobals.CHARMAP[chardex]));
				jcmbSwapChar.addItem(new String("" + TIGlobals.CHARMAP[chardex]));
			}
			else
			{
				jcmbBaseChar.addItem(new String("x" + i));
				jcmbSwapChar.addItem(new String("x" + i));
			}
		}
		jcmbBaseChar.setSelectedIndex(0);
		jcmbSwapChar.setSelectedIndex(0);

		JPanel jpnlForm = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.fill = GridBagConstraints.NONE;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.insets = new Insets(1, 1, 1, 1);
		gbc.ipadx = 2;
		gbc.ipady = 2;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.gridx = 1; gbc.gridy = 1; jpnlForm.add(new JLabel("Replace This"), gbc);
		gbc.gridx = 2; gbc.gridy = 1; jpnlForm.add(jcmbBaseChar, gbc);
		gbc.gridx = 3; gbc.gridy = 1; jpnlForm.add(jlblBaseChar, gbc);
		gbc.gridx = 1; gbc.gridy = 2; jpnlForm.add(new JLabel("With This"), gbc);
		gbc.gridx = 2; gbc.gridy = 2; jpnlForm.add(jcmbSwapChar, gbc);
		gbc.gridx = 3; gbc.gridy = 2; jpnlForm.add(jlblSwapChar, gbc);
		gbc.gridx = 1; gbc.gridy = 3; jpnlForm.add(new JLabel("On Map:"), gbc);
		gbc.gridx = 1; gbc.gridy = 4; gbc.gridwidth = 3; jpnlForm.add(jradCharReplace, gbc);
		gbc.gridx = 1; gbc.gridy = 5; gbc.gridwidth = 3; jpnlForm.add(jradCharSwap, gbc);
		gbc.gridx = 1; gbc.gridy = 6; gbc.gridwidth = 3; jpnlForm.add(jradCharNothing, gbc);
		gbc.gridx = 1; gbc.gridy = 7; jpnlForm.add(new JLabel("In Dock:"), gbc);
		gbc.gridx = 1; gbc.gridy = 8; gbc.gridwidth = 3; jpnlForm.add(jradImageReplace, gbc);
		gbc.gridx = 1; gbc.gridy = 9; gbc.gridwidth = 3; jpnlForm.add(jradImageSwap, gbc);
		gbc.gridx = 1; gbc.gridy = 10; gbc.gridwidth = 3; jpnlForm.add(jradImageNothing, gbc);
		gbc.gridx = 1; gbc.gridy = 11; gbc.gridwidth = 3; jpnlForm.add(jchkAllMaps, gbc);

		Object[] objForm = new Object[1];
		objForm[0] = jpnlForm;
		Object[] objButtons = { OK_TEXT, CANCEL_TEXT };

		JOptionPane joptMain = new JOptionPane(objForm, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null, objButtons, objButtons[0]);
		joptMain.addPropertyChangeListener(this);
		this.setContentPane(joptMain);

		this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(this);
		this.setVisible(true);
	}

	public CharacterSwapDialog(JFrame parent, IconProvider ip)
	{
		this(parent, ip, TIGlobals.MINCHAR, TIGlobals.MAXCHAR);
	}

	public int getBaseChar() { return jcmbBaseChar.getSelectedIndex() + minChar; }
	public int getSwapChar() { return jcmbSwapChar.getSelectedIndex() + minChar; }
	public String getCharAction() { return bgrpChars.getSelection().getActionCommand(); }
	public String getImageAction() { return bgrpImage.getSelection().getActionCommand(); }
	public boolean doAllMaps() { return jchkAllMaps.isSelected(); }
	public boolean isOkay() { return clickedOkay; }

	/* ItemListener method */
	public void itemStateChanged(ItemEvent ie)
	{
		if(ie.getSource().equals(jcmbBaseChar))
		{
			jlblBaseChar.setIcon(iconProv.getIconForObject(new Integer(getBaseChar())));
		}
		else if(ie.getSource().equals(jcmbSwapChar))
		{
			jlblSwapChar.setIcon(iconProv.getIconForObject(new Integer(getSwapChar())));
		}
	}

	/* PropertyChangeListener method */
	public void propertyChange(PropertyChangeEvent pce)
	{
		if(pce != null && pce.getNewValue() != null)
		{
			if(pce.getNewValue().equals(OK_TEXT))
			{
				clickedOkay = true;
				this.setVisible(false);
			}
			else if(pce.getNewValue().equals(CANCEL_TEXT))
			{
				clickedOkay = false;
				this.setVisible(false);
			}
		}
	}
}

