package com.dreamcodex.ti.component;

import java.beans.*;
import javax.swing.*;

import com.dreamcodex.ti.util.TIGlobals;

public class MagellanExportDialog extends JDialog implements PropertyChangeListener
{
	public static int TYPE_BASIC  = 0;
	public static int TYPE_ASM    = 1;
	public static int TYPE_BINARY = 2;

	private String OK_TEXT = "Export";
	private String CANCEL_TEXT = "Cancel";

	private JComboBox jcmbStartChar;
	private JComboBox jcmbEndChar;
	private JTextField jtxtCodeLineStart;
	private JTextField jtxtCharLineStart;
	private JTextField jtxtMapLineStart;
	private JTextField jtxtLineInterval;
	private JCheckBox jchkIncludeColorsets;
	private JCheckBox jchkIncludeChardata;
	private JCheckBox jchkIncludeSpritedata;
	private JCheckBox jchkIncludeComments;
	private JCheckBox jchkCurrentMapOnly;
	private boolean clickedOkay = false;
	private int minChar = 0;
	private int maxChar = 0;

	public MagellanExportDialog(int type, JFrame parent, boolean setCommentsOn, int startChar, int endChar, int minc, int maxc)
	{
		super(parent, "Export Settings", true);
		minChar = minc;
		maxChar = maxc;
		jcmbStartChar = new JComboBox();
		jcmbEndChar = new JComboBox();
		jtxtCodeLineStart = new JTextField("100");
		jtxtCharLineStart = new JTextField("500");
		jtxtMapLineStart = new JTextField("900");
		jtxtLineInterval = new JTextField("10");
		jchkIncludeColorsets = new JCheckBox("Include Colorsets", true);
		jchkIncludeChardata = new JCheckBox("Include Character Data", true);
		jchkIncludeSpritedata = new JCheckBox("Include Sprite Data", false); jchkIncludeSpritedata.setEnabled(false);
		jchkIncludeComments = new JCheckBox("Include Comments", setCommentsOn);
		jchkCurrentMapOnly = new JCheckBox("Current Map Only", false);
		int chardex = 0;
		for(int i = minChar; i <= maxChar; i++)
		{
			chardex = i - TIGlobals.CHARMAPSTART;
			if(chardex >= 0 && chardex < TIGlobals.CHARMAP.length)
			{
				jcmbStartChar.addItem(new String("" + TIGlobals.CHARMAP[chardex]));
				jcmbEndChar.addItem(new String("" + TIGlobals.CHARMAP[chardex]));
			}
			else
			{
				jcmbStartChar.addItem(new String("x" + i));
				jcmbEndChar.addItem(new String("x" + i));
			}
		}
		jcmbStartChar.setSelectedIndex(0);
		jcmbEndChar.setSelectedIndex(jcmbEndChar.getItemCount() - 1);

		Object[] objForm = new Object[15];
		int objCount = 0;
		objForm[objCount] = new JLabel("Character Range"); objCount++;
		objForm[objCount] = new JLabel("From Char #"); objCount++;
		objForm[objCount] = jcmbStartChar; objCount++;
		objForm[objCount] = new JLabel("To Char #"); objCount++;
		objForm[objCount] = jcmbEndChar; objCount++;
		if(type == TYPE_BASIC)
		{
			objForm[objCount] = new JLabel("Code Line Number Start"); objCount++;
			objForm[objCount] = jtxtCodeLineStart; objCount++;
			objForm[objCount] = new JLabel("Character Data Line Number Start"); objCount++;
			objForm[objCount] = jtxtCharLineStart; objCount++;
			objForm[objCount] = new JLabel("Map Data Line Number Start"); objCount++;
			objForm[objCount] = jtxtMapLineStart; objCount++;
			objForm[objCount] = new JLabel("Line Number Interval"); objCount++;
			objForm[objCount] = jtxtLineInterval; objCount++;
		}
		if(type == TYPE_BINARY)
		{
			objForm[objCount] = jchkIncludeColorsets; objCount++;
			objForm[objCount] = jchkIncludeChardata; objCount++;
			objForm[objCount] = jchkIncludeSpritedata; objCount++;
		}
		objForm[objCount] = jchkCurrentMapOnly; objCount++;
		if(type == TYPE_BASIC || type == TYPE_ASM)
		{
			objForm[objCount] = jchkIncludeComments; objCount++;
		}
		Object[] objButtons = { OK_TEXT, CANCEL_TEXT };

		JOptionPane joptMain = new JOptionPane(objForm, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null, objButtons, objButtons[0]);
		joptMain.addPropertyChangeListener(this);
		this.setContentPane(joptMain);

		this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(this);
		this.setVisible(true);
	}

	public MagellanExportDialog(int type, JFrame parent, boolean setCommentsOn)
	{
		this(type, parent, setCommentsOn, TIGlobals.FIRSTCHAR, TIGlobals.LASTCHAR, TIGlobals.MINCHAR, TIGlobals.MAXCHAR);
	}

	public int getStartChar() { return jcmbStartChar.getSelectedIndex() + minChar; }
	public int getEndChar()   { return jcmbEndChar.getSelectedIndex() + minChar; }
	public int getCodeLineStart() { return Integer.parseInt(jtxtCodeLineStart.getText()); }
	public int getCharLineStart() { return Integer.parseInt(jtxtCharLineStart.getText()); }
	public int getMapLineStart() { return Integer.parseInt(jtxtMapLineStart.getText()); }
	public int getLineInterval() { return Integer.parseInt(jtxtLineInterval.getText()); }
	public boolean includeColorsets() { return jchkIncludeColorsets.isSelected(); }
	public boolean includeChardata() { return jchkIncludeChardata.isSelected(); }
	public boolean includeSpritedata() { return jchkIncludeSpritedata.isSelected(); }
	public boolean includeComments() { return jchkIncludeComments.isSelected(); }
	public boolean currentMapOnly() { return jchkCurrentMapOnly.isSelected(); }
	public boolean isOkay() { return clickedOkay; }

	/* PropertyChangeListener method */
	public void propertyChange(PropertyChangeEvent pce)
	{
		if(pce != null && pce.getNewValue() != null)
		{
			if(pce.getNewValue().equals(OK_TEXT))
			{
				clickedOkay = true;
				this.setVisible(false);
			}
			else if(pce.getNewValue().equals(CANCEL_TEXT))
			{
				clickedOkay = false;
				this.setVisible(false);
			}
		}
	}
}

