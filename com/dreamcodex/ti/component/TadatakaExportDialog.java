package com.dreamcodex.ti.component;

import java.beans.*;
import javax.swing.*;

import com.dreamcodex.ti.util.TIGlobals;

public class TadatakaExportDialog extends JDialog implements PropertyChangeListener
{
	public static int TYPE_BINARY   = 0;
	public static int TYPE_ASM      = 1;
	public static int TYPE_TIARTIST = 2;

	private String OK_TEXT = "Export";
	private String CANCEL_TEXT = "Cancel";

	private JCheckBox jchkIncludeComments;
	private boolean clickedOkay = false;

	public TadatakaExportDialog(int type, JFrame parent, boolean setCommentsOn)
	{
		super(parent, "Export Settings", true);
		jchkIncludeComments = new JCheckBox("Include Comments", setCommentsOn);

		Object[] objForm = new Object[1];
		int objCount = 0;
		if(type == TYPE_ASM)
		{
			objForm[objCount] = jchkIncludeComments; objCount++;
		}
		Object[] objButtons = { OK_TEXT, CANCEL_TEXT };

		JOptionPane joptMain = new JOptionPane(objForm, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null, objButtons, objButtons[0]);
		joptMain.addPropertyChangeListener(this);
		this.setContentPane(joptMain);

		this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		this.pack();
		this.setLocationRelativeTo(this);
		this.setVisible(true);
	}

	public boolean includeComments() { return jchkIncludeComments.isSelected(); }
	public boolean isOkay() { return clickedOkay; }

	/* PropertyChangeListener method */
	public void propertyChange(PropertyChangeEvent pce)
	{
		if(pce != null && pce.getNewValue() != null)
		{
			if(pce.getNewValue().equals(OK_TEXT))
			{
				clickedOkay = true;
				this.setVisible(false);
			}
			else if(pce.getNewValue().equals(CANCEL_TEXT))
			{
				clickedOkay = false;
				this.setVisible(false);
			}
		}
	}
}

