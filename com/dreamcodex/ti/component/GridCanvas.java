package com.dreamcodex.ti.component;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;

public class GridCanvas extends JPanel implements MouseListener, MouseMotionListener
{
// Constants -------------------------------------------------------------------------------/

	public static final Point PT_OFFGRID = new Point(-1, -1);
	public static final int NODATA = -1;

// Variables -------------------------------------------------------------------------------/

	protected int[][] gridData;
	protected Color clrDraw;
	protected Color clrBack;
	protected Color clrGrid;
	protected Color clrGrup;
	protected Color clrHigh;
	protected boolean paintOn = true;
	protected int viewScale = 1;
	protected int gridGroup = 0;
	protected boolean showGrid = true;

// Components ------------------------------------------------------------------------------/

	protected BufferedImage bufferDraw;
	protected Image currBufferScaled;

// Listeners -------------------------------------------------------------------------------/

	protected MouseListener parentMouseListener;
	protected MouseMotionListener parentMouseMotionListener;

// Convenience Variables -------------------------------------------------------------------/

	private Rectangle currBounds;
	private int gridOffsetX = 0;
	private int gridOffsetY = 0;
	private int optScale = 8;
	private Point hotCell = new Point(PT_OFFGRID);
	private Point ptLastGrid = new Point(PT_OFFGRID);

// Constructors ----------------------------------------------------------------------------/

	public GridCanvas(int gridWidth, int gridHeight, int cellSize, MouseListener mlParent, MouseMotionListener mmlParent)
	{
		super(true);
		this.setOpaque(true);
		this.setBackground(Color.white);

		gridData = new int[gridHeight][gridWidth];
		for(int y = 0; y < gridHeight; y++)
		{
			for(int x = 0; x < gridWidth; x++)
			{
				gridData[y][x] = 0;
			}
		}
		optScale = cellSize;
		clrDraw = Color.black;
		clrBack = Color.white;
		clrGrid = new Color(192, 192, 192);
		clrGrup = new Color(164, 164, 255);
		clrHigh = new Color(255, 164, 164);

		bufferDraw = getImageBuffer(gridWidth * optScale, gridHeight * optScale);

		this.addMouseListener(this);
		this.addMouseMotionListener(this);

		parentMouseListener = mlParent;
		parentMouseMotionListener = mmlParent;

		redrawCanvas();
	}

// Accessors -------------------------------------------------------------------------------/

	public int[][] getGridData()  { return gridData; }
	public Color   getColorDraw() { return clrDraw; }
	public Color   getColorBack() { return clrBack; }
	public Color   getColorGrid() { return clrGrid; }
	public Color   getColorHigh() { return clrHigh; }
	public boolean isPaintOn()    { return paintOn; }
	public int     getViewScale() { return viewScale; }
	public int     getGridGroup() { return gridGroup; }
	public boolean isShowGrid()   { return showGrid; }

	public void setGridData(int[][] gd) { gridData = gd; }
	public void setColorDraw(Color clr) { clrDraw = clr; }
	public void setColorBack(Color clr) { clrBack = clr; }
	public void setColorGrid(Color clr) { clrGrid = clr; }
	public void setColorHigh(Color clr) { clrHigh = clr; }
	public void setPaintOn(boolean b)   { paintOn = b; }
	public void setViewScale(int i)     { viewScale = i; }
	public void setGridGroup(int i)     { gridGroup = i; }
	public void setShowGrid(boolean b)  { showGrid = b; }

	public int getGridWidth()  { return gridData[0].length; }
	public int getGridHeight() { return gridData.length; }

	public int getGridAt(int x, int y)
	{
		if(x >= 0 && y >= 0 && y < gridData.length && x < gridData[y].length)
		{
			return gridData[y][x];
		}
		return NODATA;
	}

	public void setGridAt(int x, int y, int v)
	{
		if(x >= 0 && y >= 0 && y < gridData.length && x < gridData[y].length)
		{
			gridData[y][x] = v;
		}
	}

	public void setGrid(int[][] gd)
	{
		setGridData(gd);
		redrawCanvas();
	}

	public void toggleGrid()
	{
		this.setShowGrid(!this.isShowGrid());
		this.redrawCanvas();
	}

// Rendering Methods -----------------------------------------------------------------------/

	public void redrawCanvas()
	{
		if(this.getGraphics() != null && bufferDraw != null)
		{
			Graphics g = bufferDraw.getGraphics();
			g.setColor(clrBack);
			g.fillRect(0, 0, bufferDraw.getWidth(this), bufferDraw.getHeight(this));
			for(int y = 0; y < gridData.length; y++)
			{
				for(int x = 0; x < gridData[0].length; x++)
				{
					if(gridData[y][x] != NODATA)
					{
						g.setColor((gridData[y][x] == 1 ? clrDraw : clrBack));
						g.fillRect(x * optScale, y * optScale, optScale, optScale);
					}
				}
			}
			if(!hotCell.equals(PT_OFFGRID))
			{
				highlightCell(g, hotCell.getX(), hotCell.getY());
			}
			currBufferScaled = bufferDraw.getScaledInstance(gridData[0].length * optScale * viewScale, gridData.length * optScale * viewScale, BufferedImage.SCALE_REPLICATE);
			g.dispose();
			this.repaint();
		}
	}

	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		try
		{
			currBounds = this.getBounds();
			viewScale = (int)(Math.max((Math.floor(Math.min((currBounds.width - currBounds.x) / gridData[0].length, (currBounds.height - currBounds.y) / gridData.length)) / optScale), 1));
			gridOffsetX = (currBounds.width - (optScale * viewScale * gridData[0].length)) / 2;
			gridOffsetY = (currBounds.height - (optScale * viewScale * gridData.length)) / 2;
			g.drawRect(gridOffsetX - 1, gridOffsetY - 1, (optScale * viewScale * gridData[0].length) + 1, (optScale * viewScale * gridData.length) + 1);
			if(currBufferScaled != null)
			{
				g.drawImage(currBufferScaled, gridOffsetX, gridOffsetY, this);
				if(showGrid)
				{
					g.setColor(clrGrid);
					for(int y = 0; y < gridData.length; y++)
					{
						for(int x = 0; x < gridData[0].length; x++)
						{
							g.drawRect(x * optScale * viewScale + gridOffsetX, y * optScale * viewScale + gridOffsetY, (optScale * viewScale) - 1, (optScale * viewScale) - 1);
						}
					}
				}
				if(gridGroup > 0)
				{
					for(int y = 0; y < (gridData.length / gridGroup); y++)
					{
						for(int x = 0; x < (gridData[y].length / gridGroup); x++)
						{
							g.setColor(clrGrup);
							g.drawRect(gridOffsetX + ((x * optScale * viewScale) * gridGroup), gridOffsetY + ((y * optScale * viewScale) * gridGroup), optScale * viewScale * gridGroup, optScale * viewScale * gridGroup);
						}
					}
				}
			}
			g.dispose();
		}
		catch(NullPointerException npe) { /* component not yet initialised */ }
	}

	protected void highlightCell(Graphics g, int x, int y)
	{
		g.setColor(clrHigh);
		g.drawRect(x * optScale, y * optScale, optScale - 1, optScale - 1);
	}

	protected void highlightCell(Graphics g, double x, double y)
	{
		highlightCell(g, (int)x, (int)y);
	}

	public void setAllGrid(int v)
	{
		for(int y = 0; y < gridData.length; y++)
		{
			for(int x = 0; x < gridData[y].length; x++)
			{
				gridData[y][x] = v;
			}
		}
		redrawCanvas();
	}

	public void clearGrid() { setAllGrid(0); }
	public void fillGrid()  { setAllGrid(1); }

	protected BufferedImage getImageBuffer(int width, int height)
	{
		BufferedImage bimgNew = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		while(bimgNew == null)
		{
			bimgNew = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			try { Thread.sleep(100); } catch(InterruptedException ie) {}
		}
		return bimgNew;
	}

// Class Methods ---------------------------------------------------------------------------/

	public Point getMouseCell(Point pt)
	{
		if(
			pt.getX() <= gridOffsetX ||
			pt.getY() <= gridOffsetY ||
			pt.getX() >= (gridOffsetX + (optScale * viewScale * gridData[0].length)) ||
			pt.getY() >= (gridOffsetY + (optScale * viewScale * gridData.length))
		)
		{
			return PT_OFFGRID;
		}
		int ptx = (int)((pt.getX() - gridOffsetX) / (optScale * viewScale));
		int pty = (int)((pt.getY() - gridOffsetY) / (optScale * viewScale));
		return new Point(ptx, pty);
	}

	public void setPaintMode(Point pt)
	{
		Point mouseCell = getMouseCell(pt);
		paintOn = true;
		if(!mouseCell.equals(PT_OFFGRID))
		{
			paintOn = (gridData[(int)(mouseCell.getY())][(int)(mouseCell.getX())] == 0);
		}
	}

	public void processCellAtPoint(Point pt)
	{
		Point mouseCell = getMouseCell(pt);
		if(!mouseCell.equals(PT_OFFGRID))
		{
			gridData[(int)(mouseCell.getY())][(int)(mouseCell.getX())] = (paintOn ? 1 : 0);
			hotCell.setLocation(mouseCell);
			redrawCanvas();
		}
	}

// Listener Methods ------------------------------------------------------------------------/

	/* MouseListener methods */
	public void mousePressed(MouseEvent me) { setPaintMode(me.getPoint()); processCellAtPoint(me.getPoint()); parentMouseListener.mousePressed(me); }
	public void mouseReleased(MouseEvent me) { }
	public void mouseClicked(MouseEvent me) { }
	public void mouseEntered(MouseEvent me) { }
	public void mouseExited(MouseEvent me) { }

	/* MouseMotionListener methods */
	public void mouseMoved(MouseEvent me)
	{
		if(!me.getPoint().equals(ptLastGrid))
		{
			hotCell.setLocation(getMouseCell(me.getPoint()));
			ptLastGrid.setLocation(me.getPoint());
			redrawCanvas();
			parentMouseMotionListener.mouseMoved(me);
		}
	}
	public void mouseDragged(MouseEvent me) { processCellAtPoint(me.getPoint()); parentMouseMotionListener.mouseDragged(me); }

}
