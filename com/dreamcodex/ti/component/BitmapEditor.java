package com.dreamcodex.ti.component;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.net.URL;
import java.util.HashMap;
import java.util.ArrayList;
import javax.swing.*;

import com.dreamcodex.ti.component.BitmapCanvas;
import com.dreamcodex.ti.util.Globals;
import com.dreamcodex.ti.util.TIGlobals;

public class BitmapEditor extends JPanel implements ItemListener, ActionListener
{
// Constants -------------------------------------------------------------------------------/

	public static final String CMD_BRUSH_PIXEL   = "brush_pixel";
	public static final String CMD_BRUSH_THREE   = "brush_three";
	public static final String CMD_BRUSH_FIVE    = "brush_five";
	public static final String CMD_BRUSH_GRID    = "brush_grid";
	public static final String CMD_BRUSH_COLOR   = "brush_color";
	public static final String CMD_BRUSH_COPY    = "brush_copy";
	public static final String CMD_BRUSH_CLONE   = "brush_clone";
	public static final String CMD_CLRFORETOGGLE = "toggle_forecolor";
	public static final String CMD_CLRBACKTOGGLE = "toggle_backcolor";

	public static final String[] MAGNIFICATIONS = { "1x", "2x", "3x", "4x", "5x", "6x", "7x", "8x" };

// Components ------------------------------------------------------------------------------/

	protected BitmapCanvas bmCanvas;
	protected JScrollPane jsclCanvas;
	protected JComboBox jcmbMagnif;
	protected JButton jbtnFillBack;
	protected JButton jbtnFillFore;
	protected JButton jbtnPrevBitmap;
	protected JButton jbtnNextBitmap;
	protected JButton jbtnBackBitmap;
	protected JButton jbtnForwBitmap;
	protected JButton jbtnBrushPixel;
	protected JButton jbtnBrushThree;
	protected JButton jbtnBrushFive;
	protected JButton jbtnBrushGrid;
	protected JButton jbtnBrushColor;
	protected JButton jbtnBrushCopy;
	protected JButton jbtnBrushClone;
	protected JButton jbtnToggleFore;
	protected JButton jbtnToggleBack;
	protected JLabel jlblMapNum;
	protected JTextField jtxtWidth;
	protected JTextField jtxtHeight;
	protected JLabel jlblPosLabel;
	protected JLabel jlblPosIndic;

// Variables -------------------------------------------------------------------------------/

	protected ArrayList<int[][]> arMaps = new ArrayList<int[][]>();
	protected ArrayList<int[][]> arClrs = new ArrayList<int[][]>();
	protected int currMap = 0;
	protected boolean showPositionIndicator = true;

// Constructors ----------------------------------------------------------------------------/

	public BitmapEditor(int cols, int rows, int cellSize, MouseListener mlParent, MouseMotionListener mmlParent)
	{
		super(new BorderLayout(), true);
		this.setOpaque(true);

		bmCanvas = new BitmapCanvas(cols, rows, cellSize, mlParent, mmlParent);

		arMaps.add(bmCanvas.getBitmapData());
		arClrs.add(bmCanvas.getColorData());

		// Create Tool Palette
		jcmbMagnif = new JComboBox(MAGNIFICATIONS);
		jcmbMagnif.addItemListener(this);
		jcmbMagnif.setSelectedIndex(0);

		jbtnFillBack = getToolButton(Globals.CMD_CLEAR, "Fill image with background color");
		jbtnFillFore = getToolButton(Globals.CMD_FILL, "Fill image with foreground color");
		JButton jbtnToggleGrid = getToolButton(Globals.CMD_GRID, "Toggle grid on and off");
		JButton jbtnAddBitmap = getToolButton(Globals.CMD_ADDMAP, "Add new bitmap"); jbtnAddBitmap.setBackground(new Color(192, 255, 192));
		JButton jbtnDelBitmap = getToolButton(Globals.CMD_DELMAP, "Delete current bitmap"); jbtnDelBitmap.setBackground(new Color(255, 192, 192));
		jbtnPrevBitmap = getToolButton(Globals.CMD_PREVMAP, "Go to previous bitmap");
		jbtnNextBitmap = getToolButton(Globals.CMD_NEXTMAP, "Go to next bitmap");
		jbtnBackBitmap = getToolButton(Globals.CMD_BACKMAP, "Move bitmap backward in set");
		jbtnForwBitmap = getToolButton(Globals.CMD_FORWMAP, "Move bitmap forward in set");
		jlblMapNum = getLabel("", JLabel.CENTER);
		jtxtWidth = getTextField("" + bmCanvas.getGridWidth());
		jtxtHeight = getTextField("" + bmCanvas.getGridHeight());
		jlblPosLabel = getLabel(" Pos", JLabel.RIGHT);
		jlblPosIndic = getLabel("", JLabel.CENTER);

		JPanel jpnlTools = getPanel(new FlowLayout(FlowLayout.LEFT));
		jpnlTools.setOpaque(true);
		jpnlTools.setBackground(Globals.CLR_COMPONENTBACK);
		jpnlTools.add(getLabel("Mag", JLabel.RIGHT));
		jpnlTools.add(jcmbMagnif);
		jpnlTools.add(jbtnFillBack);
		jpnlTools.add(jbtnFillFore);
		jpnlTools.add(jbtnToggleGrid);
/*
		jpnlTools.add(jbtnAddBitmap);
		jpnlTools.add(jbtnPrevBitmap);
		jpnlTools.add(jlblMapNum);
		jpnlTools.add(jbtnNextBitmap);
		jpnlTools.add(jbtnDelBitmap);
		jpnlTools.add(jbtnBackBitmap);
		jpnlTools.add(jbtnForwBitmap);
*/
		jpnlTools.add(getLabel(" W:", JLabel.RIGHT));
		jpnlTools.add(jtxtWidth);
		jpnlTools.add(getLabel(" H:", JLabel.RIGHT));
		jpnlTools.add(jtxtHeight);
		jpnlTools.add(jlblPosLabel);
		jpnlTools.add(jlblPosIndic);

		// Create Brush Palette
		JPanel jpnlBrushPalette = getPanel(new GridLayout(7, 1, 0, 0));
		jbtnBrushPixel = getToolButton(CMD_BRUSH_PIXEL, "Pixel Brush");
		jbtnBrushThree = getToolButton(CMD_BRUSH_THREE, "3x3 Brush");
		jbtnBrushFive = getToolButton(CMD_BRUSH_FIVE, "5x5 Brush");
		jbtnBrushGrid = getToolButton(CMD_BRUSH_GRID, "Grid Fill Brush");
		jbtnBrushColor = getToolButton(CMD_BRUSH_COLOR, "Coloriser Brush");
		jbtnBrushCopy = getToolButton(CMD_BRUSH_COPY, "Copy Region");
		jbtnBrushClone = getToolButton(CMD_BRUSH_CLONE, "Clone Brush");

		jbtnBrushPixel.setBackground((bmCanvas.getBrushType() == bmCanvas.BRUSH_PIXEL ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushThree.setBackground((bmCanvas.getBrushType() == bmCanvas.BRUSH_THREE ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushFive.setBackground((bmCanvas.getBrushType() == bmCanvas.BRUSH_FIVE ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushGrid.setBackground((bmCanvas.getBrushType() == bmCanvas.BRUSH_GRID ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushColor.setBackground((bmCanvas.getBrushType() == bmCanvas.BRUSH_COLOR ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushCopy.setBackground(((bmCanvas.isCloneModeOn() && !bmCanvas.isCloneBrush()) ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushClone.setBackground(((bmCanvas.isCloneModeOn() && bmCanvas.isCloneBrush()) ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));

		jpnlBrushPalette.add(jbtnBrushPixel);
		jpnlBrushPalette.add(jbtnBrushThree);
		jpnlBrushPalette.add(jbtnBrushFive);
		jpnlBrushPalette.add(jbtnBrushGrid);
		jpnlBrushPalette.add(jbtnBrushColor);
		jpnlBrushPalette.add(jbtnBrushCopy);
		jpnlBrushPalette.add(jbtnBrushClone);

		// Create Color Dock
		JPanel jpnlColorDock = getPanel(new GridLayout(TIGlobals.TI_PALETTE.length + 2, 1, 0, 0));
		jbtnToggleFore = getToolButton(CMD_CLRFORETOGGLE, "Toggle Foreground Coloring"); jpnlColorDock.add(jbtnToggleFore); jbtnToggleFore.setBackground((bmCanvas.isColorFore() ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnToggleBack = getToolButton(CMD_CLRBACKTOGGLE, "Toggle Background Coloring"); jpnlColorDock.add(jbtnToggleBack); jbtnToggleBack.setBackground((bmCanvas.isColorBack() ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		for(int cd = 0; cd < TIGlobals.TI_PALETTE.length; cd++)
		{
			DualClickButton dbtnColorButton  = getPaletteButton(Globals.CMD_CLRFORE + cd, Globals.CMD_CLRBACK + cd, TIGlobals.TI_PALETTE_OPAQUE[cd]);
			jpnlColorDock.add(dbtnColorButton);
		}
		JPanel jpnlColorFlow = getPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		jpnlColorFlow.add(jpnlColorDock);

		// Assemble Sidebar Panel
		JPanel jpnlSidebar = getPanel(new BorderLayout());
		jpnlSidebar.add(jpnlBrushPalette, BorderLayout.NORTH);
		jpnlSidebar.add(jpnlColorFlow, BorderLayout.CENTER);

		// Create Scrolling Edit Canvas
		jsclCanvas = new JScrollPane(bmCanvas);
		jsclCanvas.setBorder(BorderFactory.createLineBorder(new Color(64, 64, 64), 1));

		// Assemble Interface
		this.add(jsclCanvas, BorderLayout.CENTER);
		this.add(jpnlTools, BorderLayout.NORTH);
		this.add(jpnlSidebar, BorderLayout.WEST);

		updateComponents();
	}

// Accessors -------------------------------------------------------------------------------/

	public int[][] getBitmapData()   { return bmCanvas.getBitmapData(); }
	public int[][] getColorData()    { return bmCanvas.getColorData(); }
	public int     getPaintFore()    { return bmCanvas.getPaintFore(); }
	public int     getBrushType()    { return bmCanvas.getBrushType(); }
	public int     getPaintBack()    { return bmCanvas.getPaintBack(); }
	public Color   getColorGrid()    { return bmCanvas.getColorGrid(); }
	public Color   getColorHigh()    { return bmCanvas.getColorHigh(); }
	public boolean isPaintOn()       { return bmCanvas.isPaintOn(); }
	public boolean isCloneModeOn()   { return bmCanvas.isCloneModeOn(); }
	public int[][] getCloneBitmapArray() { return bmCanvas.getCloneBitmapArray(); }
	public int[][] getCloneColorArray() { return bmCanvas.getCloneColorArray(); }
	public int     getViewScale()    { return bmCanvas.getViewScale(); }
	public boolean isShowGrid()      { return bmCanvas.isShowGrid(); }
	public Point   getHotPoint()     { return bmCanvas.getHotPoint(); }
	public Point   getClonePoint()   { return bmCanvas.getClonePoint(); }
	public Color   getBkgrndColor()  { return this.getBackground(); }
	public int     getMapCount()     { return arMaps.size(); }
	public int     getCurrentMapId() { return currMap; }
	public boolean showPosIndic()    { return showPositionIndicator; }
	public int[][] getMapData(int i)
	{
		if((i == currMap) || (i < 0) || (i >= arMaps.size()))
		{
			return getBitmapData();
		}
		else
		{
			return arMaps.get(i);
		}
	}
	public int[][] getColorData(int i)
	{
		if((i == currMap) || (i < 0) || (i >= arClrs.size()))
		{
			return getColorData();
		}
		else
		{
			return arClrs.get(i);
		}
	}

	public void setBitmapData(int[][] gd)  { bmCanvas.setBitmapData(gd); }
	public void setColorData(int[][] gd)   { bmCanvas.setColorData(gd); }
	public void setPaintFore(int i)        { bmCanvas.setPaintFore(i); }
	public void setPaintBack(int i)        { bmCanvas.setPaintBack(i); }
	public void setBrushType(int i)        { bmCanvas.setBrushType(i); }
	public void setColorGrid(Color clr)    { bmCanvas.setColorGrid(clr); }
	public void setColorHigh(Color clr)    { bmCanvas.setColorHigh(clr); }
	public void setPaintOn(boolean b)      { bmCanvas.setPaintOn(b); }
	public void setCloneModeOn(boolean b)  { bmCanvas.setCloneModeOn(b); }
	public void setCloneBrush(boolean b)   { bmCanvas.setCloneBrush(b); }
	public void toggleCloneMode()          { bmCanvas.toggleCloneMode(); }
	public void setViewScale(int i)        { bmCanvas.setViewScale(i); jcmbMagnif.setSelectedIndex(i - 1); }
	public void setShowGrid(boolean b)     { bmCanvas.setShowGrid(b); }
	public void setHotPoint(Point pt)      { bmCanvas.setHotPoint(pt); }
	public void setClonePoint(Point pt)    { bmCanvas.setClonePoint(pt); }
	public void setBkgrndColor(Color clr)  { this.setBackground(clr); bmCanvas.setBackground(clr); }
	public void setCurrentMapId(int i)     { currMap = Math.max(Math.min((arMaps.size() - 1), i), 0); }
	public void setShowPosIndic(boolean b)
	{
		showPositionIndicator = b;
		jlblPosLabel.setVisible(showPositionIndicator);
		jlblPosIndic.setVisible(showPositionIndicator);
		updateComponents();
	}
	public void toggleShowPosIndic()         { setShowPosIndic(!showPositionIndicator); }
	public void addMapData(int[][] m, int[][]c) { addMap(m, c); }
	public void setMapData(int i, int[][] m, int[][] c) { storeMap(i, m, c); }

	public int getGridCols()   { return bmCanvas.getGridCols(); }
	public int getGridRows()   { return bmCanvas.getGridRows(); }
	public int getGridWidth()  { return bmCanvas.getGridWidth(); }
	public int getGridHeight() { return bmCanvas.getGridHeight(); }

	public void setGridCols(int i)   { bmCanvas.setGridCols(i); }
	public void setGridRows(int i)   { bmCanvas.setGridRows(i); }
	public void setGridWidth(int i)  { bmCanvas.setGridWidth(i); }
	public void setGridHeight(int i) { bmCanvas.setGridHeight(i); }

	public int getGridAt(int x, int y) { return bmCanvas.getGridAt(x, y); }

	public void setGridByte(int c, int r, int d)       { bmCanvas.setGridByte(c, r, d); }
	public void setColorByte(int c, int r, int d)      { bmCanvas.setColorByte(c, r, d); }
	public void setGridAt(int x, int y, int v)         { bmCanvas.setGridAt(x, y, v); }
	public void setGridAt(Point pt, int v)             { bmCanvas.setGridAt(pt, v); }
	public void setColorAt(int x, int y, int c)        { bmCanvas.setColorAt(x, y, c); }
	public void setColorAt(int x, int y, int f, int b) { bmCanvas.setColorAt(x, y, f, b); }
	public void setColorAt(Point pt, int f, int b)     { bmCanvas.setColorAt(pt, f, b); }

	public BufferedImage getBuffer() { return bmCanvas.getBuffer(); }
	public void clearCloneBuffer() { bmCanvas.clearCloneBuffer(); }

	public void redrawCanvas(boolean forceRedraw) { bmCanvas.redrawCanvas(forceRedraw); }
	public void redrawCanvas()                    { redrawCanvas(false); }

	public void addMap(int[][] mapdata, int[][] clrdata)
	{
		arMaps.add(mapdata);
		arClrs.add(clrdata);
	}

	public void addBlankMap(int c, int r)
	{
		int[][] newMap = new int[r][c];
		int[][] newColors = new int[r][c];
		for(int y = 0; y < r; y++)
		{
			for(int x = 0; x < c; x++)
			{
				newMap[y][x] = 0;
				newColors[y][x] = bmCanvas.getPaintByte();
			}
		}
		arMaps.add(newMap);
		arClrs.add(newColors);
	}

	public void addBlankMap()
	{
		addBlankMap(getGridCols(), getGridRows());
	}

	public void delMap(int index)
	{
		if((index >= 0) && (index < arMaps.size()))
		{
			arMaps.remove(index);
			arClrs.remove(index);
		}
		index--;
		if(index < 0) { index = 0; }
		if(arMaps.size() < 1) { addBlankMap(); }
		goToMap(0);
	}

	public void delAllMaps()
	{
		clearGrid();
		for(int i = arMaps.size() - 1; i >= 0; i--)
		{
			delMap(i);
		}
	}

	public void storeMap(int index, int[][] mapdata, int[][] clrdata)
	{
		if((index >= 0) && (index < arMaps.size()))
		{
			arMaps.set(index, cloneMapArray(mapdata));
		}
		if((index >= 0) && (index < arClrs.size()))
		{
			arClrs.set(index, cloneColoray(clrdata));
		}
	}

	public void storeCurrentMap()
	{
		storeMap(currMap, bmCanvas.getBitmapData(), bmCanvas.getColorData());
	}

	public void updateState(int index)
	{
		bmCanvas.setBitmapData(arMaps.get(index));
		bmCanvas.setColorData(arClrs.get(index));
	}

	public int[][] cloneMapArray(int[][] mapsrc)
	{
		int[][] cloneArray = new int[mapsrc.length][mapsrc[0].length];
		for(int y = 0; y < mapsrc.length; y++)
		{
			for(int x = 0; x < mapsrc[y].length; x++)
			{
				cloneArray[y][x] = mapsrc[y][x];
			}
		}
		return cloneArray;
	}

	public int[][] cloneColoray(int[][] clrsrc)
	{
		int[][] cloneArray = new int[clrsrc.length][clrsrc[0].length];
		for(int y = 0; y < clrsrc.length; y++)
		{
			for(int x = 0; x < clrsrc[y].length; x++)
			{
				cloneArray[y][x] = clrsrc[y][x];
			}
		}
		return cloneArray;
	}

	public void goToMap(int index)
	{
		if((index >= 0) && (index < arMaps.size()))
		{
			storeCurrentMap();
			currMap = index;
			updateState(currMap);
			updateComponents(true);
		}
	}

	public void swapMaps(int mapA, int mapB)
	{
		if(mapA != mapB)
		{
			int[][] tmpMap = getMapData(mapA);
			int[][] tmpClr = getColorData(mapA);
			storeMap(mapA, getMapData(mapB), getColorData(mapB));
			storeMap(mapB, tmpMap, tmpClr);
		}
	}

	public int moveMap(int index, boolean forward)
	{
		if(index < 0 || index >= arMaps.size())
		{
			return index;
		}
		int newIndex = index + (forward ? 1 : -1);
		if     (newIndex < 0) { newIndex = arMaps.size() - 1; }
		else if(newIndex >= arMaps.size()) { newIndex = 0; }
		swapMaps(index, newIndex);
		return newIndex;
	}
	public int moveMapForward(int index) { return moveMap(index, true); }
	public int moveMapBackward(int index) { return moveMap(index, false); }

// Component Builder Methods ---------------------------------------------------------------/

	protected JPanel getPanel(LayoutManager layout)
	{
		JPanel jpnlRtn = new JPanel(layout);
		jpnlRtn.setOpaque(true);
		jpnlRtn.setBackground(Globals.CLR_COMPONENTBACK);
		return jpnlRtn;
	}

	protected ImageIcon getIcon(String name)
	{
		URL imageURL = getClass().getResource("images/icon_" + name + ".png");
		return new ImageIcon(Toolkit.getDefaultToolkit().getImage(imageURL));
	}

	protected JButton getToolButton(String actcmd, String tooltip)
	{
		JButton jbtnTool = new JButton(getIcon(actcmd));
		jbtnTool.setActionCommand(actcmd);
		jbtnTool.addActionListener(this);
		jbtnTool.setToolTipText(tooltip);
		jbtnTool.setMargin(new Insets(1, 1, 1, 1));
		jbtnTool.setOpaque(true);
		jbtnTool.setBackground(Globals.CLR_BUTTON_NORMAL);
		jbtnTool.setPreferredSize(Globals.DM_TOOL);
		return jbtnTool;
	}

	protected DualClickButton getPaletteButton(String forecmd, String backcmd, Color bgcolor)
	{
		DualClickButton dbtnPal = new DualClickButton("", forecmd, backcmd, (ActionListener)this);
		dbtnPal.addActionListener(this);
		dbtnPal.setOpaque(true);
		dbtnPal.setBackground(bgcolor);
		dbtnPal.setMargin(new Insets(0, 0, 0, 0));
		dbtnPal.setPreferredSize(Globals.DM_TOOL);
		return dbtnPal;
	}

	protected JLabel getLabel(String text, int align)
	{
		JLabel jlblRtn = new JLabel(text, align);
		jlblRtn.setOpaque(true);
		jlblRtn.setBackground(Globals.CLR_COMPONENTBACK);
		return jlblRtn;
	}

	protected JTextField getTextField(String text)
	{
		JTextField jtxtRtn = new JTextField(text);
		jtxtRtn.addActionListener(this);
		jtxtRtn.setPreferredSize(Globals.DM_TEXT);
		jtxtRtn.setBorder(BorderFactory.createLineBorder(new Color(64, 64, 64), 1));
		return jtxtRtn;
	}

// Rendering Methods -----------------------------------------------------------------------/

	public void setAllGrid(int v)
	{
		bmCanvas.setAllGrid(v);
	}

	public void clearGrid() { bmCanvas.clearGrid(); }
	public void fillGrid()  { bmCanvas.fillGrid(); }

	public void updateComponents(boolean forceRedraw)
	{
		redrawCanvas(forceRedraw);
		if(arMaps.size() > 1)
		{
			jbtnPrevBitmap.setEnabled(true);
			jbtnNextBitmap.setEnabled(true);
			jbtnBackBitmap.setEnabled(true);
			jbtnForwBitmap.setEnabled(true);
		}
		else
		{
			jbtnPrevBitmap.setEnabled(false);
			jbtnNextBitmap.setEnabled(false);
			jbtnBackBitmap.setEnabled(false);
			jbtnForwBitmap.setEnabled(false);
		}
		jbtnBrushPixel.setBackground((bmCanvas.getBrushType() == bmCanvas.BRUSH_PIXEL ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushThree.setBackground((bmCanvas.getBrushType() == bmCanvas.BRUSH_THREE ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushFive.setBackground((bmCanvas.getBrushType() == bmCanvas.BRUSH_FIVE ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushGrid.setBackground((bmCanvas.getBrushType() == bmCanvas.BRUSH_GRID ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushColor.setBackground((bmCanvas.getBrushType() == bmCanvas.BRUSH_COLOR ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushCopy.setBackground(((bmCanvas.isCloneModeOn() && !bmCanvas.isCloneBrush()) ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnBrushClone.setBackground(((bmCanvas.isCloneModeOn() && bmCanvas.isCloneBrush()) ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnToggleFore.setBackground((bmCanvas.isColorFore() ? TIGlobals.TI_PALETTE_OPAQUE[bmCanvas.getPaintFore()] : Globals.CLR_BUTTON_INACTIVE));
		jbtnToggleBack.setBackground((bmCanvas.isColorBack() ? TIGlobals.TI_PALETTE_OPAQUE[bmCanvas.getPaintBack()] : Globals.CLR_BUTTON_INACTIVE));
		jsclCanvas.revalidate();
		jlblMapNum.setText((currMap + 1) + " / " + getMapCount());
		jtxtWidth.setText("" + getGridWidth());
		jtxtHeight.setText("" + getGridHeight());
		if(showPositionIndicator)
		{
			if(getHotPoint().equals(BitmapCanvas.PT_OFFGRID))
			{
				jlblPosIndic.setText("-/-");
			}
			else
			{
				jlblPosIndic.setText((int)(getHotPoint().getX() + 1) + "/" + (int)(getHotPoint().getY() + 1));
			}
		}
	}

	public void updateComponents() { updateComponents(false); }

// Listener Methods ------------------------------------------------------------------------/

	/* ItemListener methods */
	public void itemStateChanged(ItemEvent ie)
	{
		if(ie.getSource().equals(jcmbMagnif))
		{
			bmCanvas.setViewScale(jcmbMagnif.getSelectedIndex() + 1);
			jsclCanvas.revalidate();
		}
		redrawCanvas();
	}

	/* ActionListener methods */
	public void actionPerformed(ActionEvent ae)
	{
		try
		{
			if(ae.getSource().equals(jtxtWidth))
			{
				try
				{
					int newWidth = Integer.parseInt(jtxtWidth.getText());
					setGridWidth(newWidth);
					jsclCanvas.revalidate();
				}
				catch(NumberFormatException nfe) { }
				jtxtWidth.setText("" + getGridWidth());
			}
			else if(ae.getSource().equals(jtxtHeight))
			{
				try
				{
					int newHeight = Integer.parseInt(jtxtHeight.getText());
					setGridHeight(newHeight);
					jsclCanvas.revalidate();
				}
				catch(NumberFormatException nfe) { }
				jtxtHeight.setText("" + getGridHeight());
			}
			else
			{
				String command = ae.getActionCommand();
				if(command.startsWith(Globals.CMD_CLRFORE))
				{
					int clrval = Integer.parseInt(command.substring(Globals.CMD_CLRFORE.length()));
					try
					{
						Graphics g = ((ImageIcon)(jbtnFillFore.getIcon())).getImage().getGraphics();
						g.setColor(TIGlobals.TI_PALETTE_OPAQUE[clrval]);
						g.fillRect(2, 2, 9, 9);
						g.dispose();
						jbtnFillFore.repaint();
					}
					catch(Exception e)
					{
						Image imgIcon = ((ImageIcon)(jbtnFillFore.getIcon())).getImage();
						Image imgTemp = this.createImage(imgIcon.getWidth(this), imgIcon.getHeight(this));
						Graphics g = imgTemp.getGraphics();
						g.setColor(Globals.CLR_BUTTON_NORMAL);
						g.fillRect(0, 0, imgIcon.getWidth(this), imgIcon.getHeight(this));
						g.drawImage(getIcon(Globals.CMD_CLEAR).getImage(), 0, 0, this);
						g.setColor(TIGlobals.TI_PALETTE_OPAQUE[clrval]);
						g.fillRect(2, 2, 9, 9);
						jbtnFillFore.setIcon(new ImageIcon(imgTemp));
						g.dispose();
					}
					setPaintFore(clrval);
					updateComponents();
				}
				else if(command.startsWith(Globals.CMD_CLRBACK))
				{
					int clrval = Integer.parseInt(command.substring(Globals.CMD_CLRBACK.length()));
					try
					{
						Graphics g = ((ImageIcon)(jbtnFillBack.getIcon())).getImage().getGraphics();
						g.setColor(TIGlobals.TI_PALETTE_OPAQUE[clrval]);
						g.fillRect(2, 2, 9, 9);
						g.dispose();
						jbtnFillBack.repaint();
					}
					catch(Exception e)
					{
						Image imgIcon = ((ImageIcon)(jbtnFillBack.getIcon())).getImage();
						Image imgTemp = this.createImage(imgIcon.getWidth(this), imgIcon.getHeight(this));
						Graphics g = imgTemp.getGraphics();
						g.setColor(Globals.CLR_BUTTON_NORMAL);
						g.fillRect(0, 0, imgIcon.getWidth(this), imgIcon.getHeight(this));
						g.drawImage(getIcon(Globals.CMD_CLEAR).getImage(), 0, 0, this);
						g.setColor(TIGlobals.TI_PALETTE_OPAQUE[clrval]);
						g.fillRect(2, 2, 9, 9);
						jbtnFillBack.setIcon(new ImageIcon(imgTemp));
						g.dispose();
					}
					setPaintBack(clrval);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_CLEAR))
				{
					clearGrid();
					updateComponents();
				}
				else if(command.equals(Globals.CMD_FILL))
				{
					fillGrid();
					updateComponents();
				}
				else if(command.equals(Globals.CMD_GRID))
				{
					bmCanvas.toggleGrid();
					updateComponents();
				}
				else if(command.equals(CMD_BRUSH_PIXEL))
				{
					bmCanvas.setBrushType(BitmapCanvas.BRUSH_PIXEL);
					updateComponents();
				}
				else if(command.equals(CMD_BRUSH_THREE))
				{
					bmCanvas.setBrushType(BitmapCanvas.BRUSH_THREE);
					updateComponents();
				}
				else if(command.equals(CMD_BRUSH_FIVE))
				{
					bmCanvas.setBrushType(BitmapCanvas.BRUSH_FIVE);
					updateComponents();
				}
				else if(command.equals(CMD_BRUSH_GRID))
				{
					bmCanvas.setBrushType(BitmapCanvas.BRUSH_GRID);
					updateComponents();
				}
				else if(command.equals(CMD_BRUSH_COLOR))
				{
					bmCanvas.setBrushType(BitmapCanvas.BRUSH_COLOR);
					updateComponents();
				}
				else if(command.equals(CMD_BRUSH_COPY))
				{
					toggleCloneMode();
					setCloneBrush(false);
					updateComponents();
				}
				else if(command.equals(CMD_BRUSH_CLONE))
				{
					toggleCloneMode();
					setCloneBrush(true);
					updateComponents();
				}
				else if(command.equals(CMD_CLRFORETOGGLE))
				{
					bmCanvas.toggleColorFore();
					jbtnToggleFore.setBackground((bmCanvas.isColorFore() ? TIGlobals.TI_PALETTE_OPAQUE[bmCanvas.getPaintFore()] : Globals.CLR_BUTTON_INACTIVE));
					updateComponents();
				}
				else if(command.equals(CMD_CLRBACKTOGGLE))
				{
					bmCanvas.toggleColorBack();
					jbtnToggleBack.setBackground((bmCanvas.isColorBack() ? TIGlobals.TI_PALETTE_OPAQUE[bmCanvas.getPaintBack()] : Globals.CLR_BUTTON_INACTIVE));
					updateComponents();
				}
				else if(command.equals(Globals.CMD_ADDMAP))
				{
					storeCurrentMap();
					clearGrid();
					addMap(bmCanvas.getBitmapData(), bmCanvas.getColorData());
					currMap = (arMaps.size() - 1);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_DELMAP))
				{
					clearGrid();
					delMap(currMap);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_PREVMAP))
				{
					storeCurrentMap();
					currMap--;
					if(currMap < 0) { currMap = (arMaps.size() - 1); }
					updateState(currMap);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_NEXTMAP))
				{
					storeCurrentMap();
					currMap++;
					if(currMap >= arMaps.size()) { currMap = 0; }
					updateState(currMap);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_BACKMAP))
				{
					storeCurrentMap();
					currMap = moveMapBackward(currMap);
					goToMap(currMap);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_FORWMAP))
				{
					storeCurrentMap();
					currMap = moveMapForward(currMap);
					goToMap(currMap);
					updateComponents();
				}
			}
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}
}
