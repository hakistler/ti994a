package com.dreamcodex.ti.component;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.HashMap;
import javax.swing.*;

import com.dreamcodex.ti.util.TIGlobals;

public class MapCanvas extends JPanel implements MouseListener, MouseMotionListener, KeyListener
{
// Constants -------------------------------------------------------------------------------/

	public static final Point PT_OFFGRID = new Point(-1, -1);
	public static final int NOCHAR = -1;

	public static Color CLR_COPY  = new Color(255, 128, 128, 128);
	public static Color CLR_PASTE = new Color(128, 128, 255, 128);

// Variables -------------------------------------------------------------------------------/

	protected int[][] gridData;
	protected Color clrGrid;
	protected Color clrHigh;
	protected Color clrType;
	protected boolean paintOn = true;
	protected HashMap<String,Image> hmCharImages;
	protected int activeChar = TIGlobals.SPACECHAR;
	protected int viewScale = 1;
	protected int colorScreen = 15;
	protected boolean showGrid = true;
	protected boolean lookModeOn = false;
	protected boolean cloneModeOn = false;
	protected int[][] cloneArray = null;
	protected int lookChar = NOCHAR;
	protected boolean typeCellOn = true;

// Components ------------------------------------------------------------------------------/

	protected BufferedImage bufferDraw;
	protected Image currBufferScaled;

// Listeners -------------------------------------------------------------------------------/

	protected MouseListener parentMouseListener;
	protected MouseMotionListener parentMouseMotionListener;

// Convenience Variables -------------------------------------------------------------------/

	private Rectangle currBounds = (Rectangle)null;
	private int gridOffsetX = 0;
	private int gridOffsetY = 0;
	private int optScale = 8;
	private Point hotCell = new Point(PT_OFFGRID);
	private Point typeCell = new Point(PT_OFFGRID);
	private Point cloneCell = new Point(PT_OFFGRID);
	private Rectangle rectClone = (Rectangle)null;
	private Point ptLastGrid = new Point(PT_OFFGRID);
	private boolean isTyping = false;

// Constructors ----------------------------------------------------------------------------/

	public MapCanvas(int gridWidth, int gridHeight, int cellSize, MouseListener mlParent, MouseMotionListener mmlParent)
	{
		super(true);
		this.setOpaque(true);

		gridData = new int[gridHeight][gridWidth];
		for(int y = 0; y < gridHeight; y++)
		{
			for(int x = 0; x < gridWidth; x++)
			{
				gridData[y][x] = TIGlobals.SPACECHAR;
			}
		}
		optScale = cellSize;
		clrGrid = new Color(192, 192, 192);
		clrHigh = new Color(255, 164, 164);
		clrType = new Color(164, 164, 255);

		bufferDraw = getImageBuffer(gridWidth * optScale, gridHeight * optScale);

		hmCharImages = new HashMap<String,Image>();

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addKeyListener(this);

		parentMouseListener = mlParent;
		parentMouseMotionListener = mmlParent;
	}

// Accessors -------------------------------------------------------------------------------/

	public int[][] getGridData()    { return gridData; }
	public Color   getColorGrid()   { return clrGrid; }
	public Color   getColorHigh()   { return clrHigh; }
	public Color   getColorType()   { return clrType; }
	public boolean isPaintOn()      { return paintOn; }
	public int     getActiveChar()  { return activeChar; }
	public int     getViewScale()   { return viewScale; }
	public int     getColorScreen() { return colorScreen; }
	public int     getColorScreenTI() { return colorScreen + 1; }
	public boolean isShowGrid()     { return showGrid; }
	public boolean isLookModeOn()   { return lookModeOn; }
	public boolean isCloneModeOn()  { return cloneModeOn; }
	public int[][] getCloneArray()  { return cloneArray; }
	public int     getLookChar()    { return lookChar; }
	public Point   getHotCell()     { return hotCell; }
	public Point   getCloneCell()   { return cloneCell; }
	public Point   getTypeCell()    { return typeCell; }
	public boolean showTypeCell()   { return typeCellOn; }

	public void setGridData(int[][] gd)   { gridData = gd; }
	public void setColorGrid(Color clr)   { clrGrid = clr; }
	public void setColorHigh(Color clr)   { clrHigh = clr; }
	public void setColorType(Color clr)   { clrType = clr; }
	public void setPaintOn(boolean b)     { paintOn = b; }
	public void setActiveChar(int i)      { activeChar = i; }
	public void setViewScale(int i)       { viewScale = i; }
	public void setColorScreen(int i)     { colorScreen = i; }
	public void setColorScreenTI(int i)   { colorScreen = i - 1; }
	public void setShowGrid(boolean b)    { showGrid = b; }
	public void setLookModeOn(boolean b)  { lookModeOn = b; lookChar = NOCHAR; }
	public void setCloneModeOn(boolean b) { cloneModeOn = b; cloneCell.setLocation(PT_OFFGRID); cloneArray = null; }
	public void setCloneArray(int[][] ar) { cloneArray = ar; }
	public void setLookChar(int i)        { lookChar = i; }
	public void setHotCell(Point pt)      { hotCell.setLocation(pt); }
	public void setCloneCell(Point pt)    { cloneCell.setLocation(pt); }
	public void setTypeCell(Point pt)     { typeCell.setLocation(pt); }
	public void setTypeCellOn(boolean b)  { typeCellOn = b; }

	public BufferedImage getBuffer() { redrawCanvas(); return bufferDraw; }

	public int getGridWidth()  { return gridData[0].length; }
	public int getGridHeight() { return gridData.length; }

	public void setGridWidth(int i)
	{
		int[][] newGrid = new int[this.getGridHeight()][i];
		for(int y = 0; y < this.getGridHeight(); y++)
		{
			for(int x = 0; x < i; x++)
			{
				if(x < this.getGridWidth())
				{
					newGrid[y][x] = gridData[y][x];
				}
				else
				{
					newGrid[y][x] = TIGlobals.SPACECHAR;
				}
			}
		}
		gridData = newGrid;
		bufferDraw = getImageBuffer(gridData[0].length * optScale, gridData.length * optScale);
		redrawCanvas();
	}
	public void setGridHeight(int i)
	{
		int[][] newGrid = new int[i][this.getGridWidth()];
		for(int y = 0; y < i; y++)
		{
			for(int x = 0; x < this.getGridWidth(); x++)
			{
				if(y < this.getGridHeight())
				{
					newGrid[y][x] = gridData[y][x];
				}
				else
				{
					newGrid[y][x] = TIGlobals.SPACECHAR;
				}
			}
		}
		gridData = newGrid;
		bufferDraw = getImageBuffer(gridData[0].length * optScale, gridData.length * optScale);
		redrawCanvas();
	}

	public int getGridAt(int x, int y)
	{
		if(x >= 0 && y >= 0 && y < gridData.length && x < gridData[y].length)
		{
			return gridData[y][x];
		}
		return NOCHAR;
	}

	public void setGridAt(int x, int y, int v)
	{
		if(x >= 0 && y >= 0 && y < gridData.length && x < gridData[y].length)
		{
			gridData[y][x] = v;
		}
	}

	public void setGridAt(Point pt, int v)
	{
		setGridAt((int)(pt.getX()), (int)(pt.getY()), v);
	}

	public void setCharImage(int charnum, Image img)
	{
		if(hmCharImages.containsKey(getCharKey(charnum)))
		{
			hmCharImages.remove(getCharKey(charnum));
		}
		hmCharImages.put(getCharKey(charnum), img);
	}

	public void toggleGrid()
	{
		this.setShowGrid(!this.isShowGrid());
		this.redrawCanvas();
	}

	public void toggleTextCursor()
	{
		this.setTypeCellOn(!this.showTypeCell());
		this.redrawCanvas();
	}

	public void toggleCloneMode()
	{
		this.setCloneModeOn(!isCloneModeOn());
	}

	public void advanceTypeCell()
	{
		if(!typeCell.equals(PT_OFFGRID))
		{
			int typeX = (int)(typeCell.getX());
			int typeY = (int)(typeCell.getY());
			typeX++;
			if(typeX >= gridData[typeY].length)
			{
				typeX = 0;
				typeY++;
				if(typeY >= gridData.length)
				{
					typeY = 0;
				}
			}
			typeCell.setLocation(typeX, typeY);
		}
	}

// Rendering Methods -----------------------------------------------------------------------/

	public void redrawCanvas()
	{
		if(this.getGraphics() != null && bufferDraw != null)
		{
			if(bufferDraw.getWidth() != (gridData[0].length * optScale) || bufferDraw.getHeight() != (gridData.length * optScale))
			{
				bufferDraw = getImageBuffer(gridData[0].length * optScale, gridData.length * optScale);
			}
			Graphics g = bufferDraw.getGraphics();
			g.setColor(TIGlobals.TI_PALETTE_OPAQUE[colorScreen]);
			g.fillRect(0, 0, bufferDraw.getWidth(this), bufferDraw.getHeight(this));
			boolean painted = false;
			for(int y = 0; y < gridData.length; y++)
			{
				for(int x = 0; x < gridData[0].length; x++)
				{
					painted = false;
					if(gridData[y][x] != NOCHAR)
					{
						if(hmCharImages.containsKey(getCharKey(gridData[y][x])))
						{
							if(hmCharImages.get(getCharKey(gridData[y][x])) != null)
							{
								g.drawImage(hmCharImages.get(getCharKey(gridData[y][x])), x * optScale, y * optScale, optScale, optScale, this);
								painted = true;
							}
						}
					}
					if(!painted)
					{
						g.setColor(TIGlobals.TI_PALETTE_OPAQUE[colorScreen]);
						g.fillRect(x * optScale, y * optScale, optScale, optScale);
					}
				}
			}
			if(cloneModeOn)
			{
				if(cloneArray != null)
				{
					if(!hotCell.equals(PT_OFFGRID))
					{
						g.setColor(CLR_PASTE);
						g.fillRect((int)(hotCell.getX() * optScale), (int)(hotCell.getY() * optScale), cloneArray[0].length * optScale, cloneArray.length * optScale);
					}
				}
				else if(rectClone != null)
				{
					g.setColor(CLR_COPY);
					g.fillRect(rectClone.x * optScale, rectClone.y * optScale, (rectClone.width - rectClone.x) * optScale, (rectClone.height - rectClone.y) * optScale);
				}
				else
				{
					Rectangle rectTmp = getCloneRectangle(hotCell);
					if(rectTmp != null)
					{
						g.setColor(CLR_COPY);
						g.fillRect(rectTmp.x * optScale, rectTmp.y * optScale, (rectTmp.width - rectTmp.x) * optScale, (rectTmp.height - rectTmp.y) * optScale);
					}
				}
			}
			if(typeCellOn && !typeCell.equals(PT_OFFGRID))
			{
				outlineCell(g, typeCell.getX(), typeCell.getY(), clrType);
			}
			if(!hotCell.equals(PT_OFFGRID))
			{
				highlightCell(g, hotCell.getX(), hotCell.getY());
			}
			currBufferScaled = bufferDraw.getScaledInstance(gridData[0].length * optScale * viewScale, gridData.length * optScale * viewScale, BufferedImage.SCALE_REPLICATE);
			this.setPreferredSize(new Dimension(gridData[0].length * optScale * viewScale, gridData.length * optScale * viewScale));
			g.dispose();
			this.repaint();
		}
	}

	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		try
		{
			this.setPreferredSize(new Dimension(gridData[0].length * optScale * viewScale, gridData.length * optScale * viewScale));
			currBounds = this.getBounds();
			gridOffsetX = (currBounds.width - (optScale * viewScale * gridData[0].length)) / 2;
			gridOffsetY = (currBounds.height - (optScale * viewScale * gridData.length)) / 2;
			g.drawRect(gridOffsetX - 1, gridOffsetY - 1, (optScale * viewScale * gridData[0].length) + 1, (optScale * viewScale * gridData.length) + 1);
			if(currBufferScaled != null)
			{
				g.drawImage(currBufferScaled, gridOffsetX, gridOffsetY, this);
				if(showGrid)
				{
					g.setColor(clrGrid);
					for(int y = 0; y < gridData.length; y++)
					{
						for(int x = 0; x < gridData[0].length; x++)
						{
//							g.drawRect(x * optScale, y * optScale, optScale - 1, optScale - 1);
							g.drawRect(x * optScale * viewScale + gridOffsetX, y * optScale * viewScale + gridOffsetY, (optScale * viewScale) - 1, (optScale * viewScale) - 1);
						}
					}
				}
			}
			g.dispose();
		}
		catch(NullPointerException npe) { /* component not yet initialised */ }
	}

	protected void outlineCell(Graphics g, int x, int y, Color clr)
	{
		g.setColor(clr);
		g.drawRect(x * optScale, y * optScale, optScale - 1, optScale - 1);
	}

	protected void outlineCell(Graphics g, double x, double y, Color clr)
	{
		outlineCell(g, (int)x, (int)y, clr);
	}

	protected void highlightCell(Graphics g, int x, int y)
	{
		if(lookModeOn || cloneModeOn || !paintOn || !hmCharImages.containsKey(getCharKey(activeChar)))
		{
			outlineCell(g, x, y, clrHigh);
		}
		else
		{
			if(hmCharImages.get(getCharKey(activeChar)) != null)
			{
				g.drawImage(hmCharImages.get(getCharKey(activeChar)), x * optScale, y * optScale, optScale, optScale, this);
			}
		}
	}

	protected void highlightCell(Graphics g, double x, double y)
	{
		highlightCell(g, (int)x, (int)y);
	}

	public void setAllGrid(int v)
	{
		for(int y = 0; y < gridData.length; y++)
		{
			for(int x = 0; x < gridData[y].length; x++)
			{
				gridData[y][x] = v;
			}
		}
		redrawCanvas();
	}

	public void wipeGrid()      { setAllGrid(NOCHAR); }
	public void clearGrid()     { setAllGrid(TIGlobals.SPACECHAR); }
	public void fillGrid(int v) { setAllGrid(v); }

	protected String getCharKey(int charnum)
	{
		return ("ch" + charnum);
	}

	protected void typeCharToGrid(int x, int y, char ch)
	{
		int charNum = -1;
		if(ch == ' ')
		{
			charNum = TIGlobals.SPACECHAR;
		}
		else
		{
			for(int i = 0; i < TIGlobals.CHARMAP.length; i++)
			{
				if(TIGlobals.CHARMAP[i] == ch)
				{
					charNum = i + TIGlobals.CHARMAPSTART;
				}
			}
		}
		if(charNum > -1)
		{
			setGridAt(x, y, charNum);
			advanceTypeCell();
			redrawCanvas();
		}
	}

	protected void typeCharToGrid(Point pt, char ch)
	{
		typeCharToGrid((int)(pt.getX()), (int)(pt.getY()), ch);
	}

	protected Rectangle getCloneRectangle(Point endpoint)
	{
		if(cloneCell.equals(PT_OFFGRID) || endpoint.equals(PT_OFFGRID))
		{
			return (Rectangle)null;
		}
		else
		{
			return new Rectangle((int)(Math.min(cloneCell.getX(), endpoint.getX())), (int)(Math.min(cloneCell.getY(), endpoint.getY())), (int)(Math.max(cloneCell.getX(), endpoint.getX())) + 1, (int)(Math.max(cloneCell.getY(), endpoint.getY())) + 1);
		}
	}

	protected BufferedImage getImageBuffer(int width, int height)
	{
		BufferedImage bimgNew = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		while(bimgNew == null)
		{
			bimgNew = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			try { Thread.sleep(100); } catch(InterruptedException ie) {}
		}
		return bimgNew;
	}

// Class Methods ---------------------------------------------------------------------------/

	public Point getMouseCell(Point pt)
	{
		if(
			pt.getX() <= gridOffsetX ||
			pt.getY() <= gridOffsetY ||
			pt.getX() >= (gridOffsetX + (optScale * viewScale * gridData[0].length)) ||
			pt.getY() >= (gridOffsetY + (optScale * viewScale * gridData.length))
		)
		{
			return new Point(PT_OFFGRID);
		}
		int ptx = (int)((pt.getX() - gridOffsetX) / (optScale * viewScale));
		int pty = (int)((pt.getY() - gridOffsetY) / (optScale * viewScale));
		return new Point(ptx, pty);
	}

	public void processCellAtPoint(Point pt, int btn)
	{
		if(btn > 0)
		{
			paintOn = (btn == MouseEvent.BUTTON3 ? false : true);
		}
		hotCell.setLocation(getMouseCell(pt));
		typeCell.setLocation(hotCell);
		if(!hotCell.equals(PT_OFFGRID))
		{
			if(lookModeOn)
			{
				lookChar = gridData[(int)(hotCell.getY())][(int)(hotCell.getX())];
			}
			else
			{
				gridData[(int)(hotCell.getY())][(int)(hotCell.getX())] = (paintOn ? activeChar : TIGlobals.SPACECHAR);
				redrawCanvas();
			}
		}
	}

// Listener Methods ------------------------------------------------------------------------/

	/* MouseListener methods */
	public void mousePressed(MouseEvent me)
	{
		if(cloneModeOn)
		{
			hotCell.setLocation(getMouseCell(me.getPoint()));
		}
		else
		{
			processCellAtPoint(me.getPoint(), me.getButton());
			parentMouseListener.mousePressed(me);
		}
	}
	public void mouseClicked(MouseEvent me)
	{
		if(cloneModeOn)
		{
			Point clickCell = getMouseCell(me.getPoint());
			if(cloneArray != null && !clickCell.equals(PT_OFFGRID))
			{
				for(int y = 0; y < cloneArray.length; y++)
				{
					int plotY = (int)(clickCell.getY() + y);
					if(plotY >= 0 && plotY < gridData.length)
					{
						for(int x = 0; x < cloneArray[y].length; x++)
						{
							int plotX = (int)(clickCell.getX() + x);
							if(plotX >= 0 && plotX < gridData[y].length)
							{
								gridData[plotY][plotX] = cloneArray[y][x];
							}
						}
					}
				}
				cloneModeOn = false;
				rectClone = (Rectangle)null;
				cloneArray = null;
				redrawCanvas();
				parentMouseListener.mouseClicked(me);
			}
			else if(!cloneCell.equals(PT_OFFGRID))
			{
				if(!clickCell.equals(PT_OFFGRID))
				{
					rectClone = getCloneRectangle(clickCell);
					cloneArray = new int[rectClone.height - rectClone.y][rectClone.width - rectClone.x];
					for(int y = 0; y < cloneArray.length; y++)
					{
						for(int x = 0; x < cloneArray[y].length; x++)
						{
							cloneArray[y][x] = gridData[rectClone.y + y][rectClone.x + x];
						}
					}
					rectClone = (Rectangle)null;
				}
				else
				{
					rectClone = (Rectangle)null;
					cloneArray = null;
				}
			}
			else
			{
				cloneCell.setLocation(clickCell);
			}
		}
	}
	public void mouseReleased(MouseEvent me) { paintOn = true; }
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}

	/* MouseMotionListener methods */
	public void mouseMoved(MouseEvent me)
	{
		if(!me.getPoint().equals(ptLastGrid))
		{
			hotCell.setLocation(getMouseCell(me.getPoint()));
			ptLastGrid.setLocation(me.getPoint());
			if(!hotCell.equals(PT_OFFGRID))
			{
				lookChar = gridData[(int)(hotCell.getY())][(int)(hotCell.getX())];
			}
			else
			{
				lookChar = NOCHAR;
			}
			redrawCanvas();
			parentMouseMotionListener.mouseMoved(me);
		}
	}
	public void mouseDragged(MouseEvent me)
	{
		if(cloneModeOn)
		{
			hotCell.setLocation(getMouseCell(me.getPoint()));
		}
		else
		{
			processCellAtPoint(me.getPoint(), me.getButton());
			parentMouseMotionListener.mouseDragged(me);
		}
	}

	/* KeyListener methods */
	public void keyTyped(KeyEvent ke)
	{
		if(!typeCell.equals(PT_OFFGRID) && !isTyping)
		{
			isTyping = true;
			typeCharToGrid(typeCell, ke.getKeyChar());
			isTyping = false;
		}
	}
	public void keyPressed(KeyEvent ke) {}
	public void keyReleased(KeyEvent ke) {}

}
