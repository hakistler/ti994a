package com.dreamcodex.ti.component;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ClickEditor extends JPanel implements MouseListener, MouseMotionListener
{
// Constants -------------------------------------------------------------------------------/

	protected static final Point PT_OFFGRID = new Point(-1, -1);

// Variables -------------------------------------------------------------------------------/

	protected int[][] gridData;
	protected Color clrCnvs;
	protected Color clrBack;
	protected Color clrDraw;
	protected Color clrGrid;
	protected Color clrGrup;
	protected Color clrHigh;
	protected boolean paintOn = true;

// Listeners -------------------------------------------------------------------------------/

	protected MouseListener parentMouseListener;
	protected MouseMotionListener parentMouseMotionListener;

// Convenience Variables -------------------------------------------------------------------/

	private Rectangle currBounds;
	private int gridOffsetX = 0;
	private int gridOffsetY = 0;
	private int optScale = 0;
	private int gridGroup = 0;
	private Point ptLastGrid = new Point();

// Constructors ----------------------------------------------------------------------------/

	public ClickEditor(int gridWidth, int gridHeight, MouseListener mlParent, MouseMotionListener mmlParent)
	{
		super(true);
		this.setOpaque(true);
		gridData = new int[gridHeight][gridWidth];
		for(int y = 0; y < gridHeight; y++)
		{
			for(int x = 0; x < gridWidth; x++)
			{
				gridData[y][x] = 0;
			}
		}
		clrCnvs = Color.white;
		clrBack = Color.white;
		clrDraw = Color.black;
		clrGrid = new Color(192, 192, 192);
		clrGrup = new Color(164, 164, 255);
		clrHigh = new Color(255, 164, 164);

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		parentMouseListener = mlParent;
		parentMouseMotionListener = mmlParent;
	}

// Accessors -------------------------------------------------------------------------------/

	public int[][] getGridData()  { return gridData; }
	public Color   getColorCnvs() { return clrCnvs; }
	public Color   getColorBack() { return clrBack; }
	public Color   getColorDraw() { return clrDraw; }
	public Color   getColorGrid() { return clrGrid; }
	public Color   getColorGrup() { return clrGrup; }
	public Color   getColorHigh() { return clrHigh; }
	public int     getGridGroup() { return gridGroup; }
	public boolean isPaintOn()    { return paintOn; }

	public void setGridData(int[][] gd) { gridData = gd; }
	public void setColorCnvs(Color clr) { clrCnvs = clr; }
	public void setColorBack(Color clr) { clrBack = clr; }
	public void setColorDraw(Color clr) { clrDraw = clr; }
	public void setColorGrid(Color clr) { clrGrid = clr; }
	public void setColorGrup(Color clr) { clrGrup = clr; }
	public void setColorHigh(Color clr) { clrHigh = clr; }
	public void setGridGroup(int i)     { gridGroup = i; }
	public void setPaintOn(boolean b)   { paintOn = b; }

	public void redrawCanvas()
	{
		try
		{
			if(this.getGraphics() != null)
			{
				Graphics g = this.getGraphics();
				g.setColor(clrCnvs);
				g.fillRect(0, 0, currBounds.width, currBounds.height);
				paintComponent(g);
			}
		}
		catch(NullPointerException npe) { /* not fully initialised yet */ }
	}

// Rendering Methods -----------------------------------------------------------------------/

	protected void paintComponent(Graphics g)
	{
		if(!this.getBounds().equals(currBounds))
		{
			g.setColor(clrCnvs);
			g.fillRect(0, 0, this.getBounds().width, this.getBounds().height);
			currBounds = this.getBounds();
			optScale = Math.min(currBounds.width / gridData[0].length, currBounds.height / gridData.length) - 1;
		}
		gridOffsetX = (currBounds.width - (optScale * gridData[0].length)) / 2;
		gridOffsetY = (currBounds.height - (optScale * gridData.length)) / 2;
		g.drawRect(gridOffsetX, gridOffsetY, (optScale * gridData[0].length), (optScale * gridData.length));
		g.setColor(clrBack);
		g.fillRect(gridOffsetX, gridOffsetY, (optScale * gridData[0].length), (optScale * gridData.length));
		g.setColor(clrGrid);
		g.drawRect(gridOffsetX, gridOffsetY, (optScale * gridData[0].length), (optScale * gridData.length));
		for(int y = 0; y < gridData.length; y++)
		{
			for(int x = 0; x < gridData[0].length; x++)
			{
				g.setColor(gridData[y][x] == 0 ? clrBack : clrDraw);
				g.fillRect(gridOffsetX + (x * optScale), gridOffsetY + (y * optScale), optScale, optScale);
				g.setColor(clrGrid);
				g.drawRect(gridOffsetX + (x * optScale), gridOffsetY + (y * optScale), optScale, optScale);
			}
		}
		if(gridGroup > 0)
		{
			for(int y = 0; y < (gridData.length / gridGroup); y++)
			{
				for(int x = 0; x < (gridData[y].length / gridGroup); x++)
				{
					g.setColor(clrGrup);
					g.drawRect(gridOffsetX + ((x * optScale) * gridGroup), gridOffsetY + ((y * optScale) * gridGroup), optScale * gridGroup, optScale * gridGroup);
				}
			}
		}
	}

	protected void highlightCell(int x, int y)
	{
		Graphics g = this.getGraphics();
		paintComponent(g);
		g.setColor(clrHigh);
		g.drawRect(gridOffsetX + (x * optScale), gridOffsetY + (y * optScale), optScale, optScale);
	}

	protected void highlightCell(double x, double y)
	{
		highlightCell((int)x, (int)y);
	}

	public void setAllGrid(int v)
	{
		for(int y = 0; y < gridData.length; y++)
		{
			for(int x = 0; x < gridData[y].length; x++)
			{
				gridData[y][x] = v;
			}
		}
		paintComponent(this.getGraphics());
	}

	public void clearGrid() { setAllGrid(0); }
	public void fillGrid()  { setAllGrid(1); }

// Class Methods ---------------------------------------------------------------------------/

	public Point getMouseCell(Point pt)
	{
		if(
			pt.getX() <= gridOffsetX ||
			pt.getY() <= gridOffsetY ||
			pt.getX() >= (gridOffsetX + (optScale * gridData[0].length)) ||
			pt.getY() >= (gridOffsetY + (optScale * gridData.length))
		)
		{
			return PT_OFFGRID;
		}
		int ptx = (int)((pt.getX() - gridOffsetX) / optScale);
		int pty = (int)((pt.getY() - gridOffsetY) / optScale);
		return new Point(ptx, pty);
	}

	public void processCellAtPoint(Point pt)
	{
		Point hotCell = getMouseCell(pt);
		if(!hotCell.equals(PT_OFFGRID))
		{
			gridData[(int)(hotCell.getY())][(int)(hotCell.getX())] = (paintOn ? 1 : 0);
			highlightCell(hotCell.getX(), hotCell.getY());
		}
	}

// Listener Methods ------------------------------------------------------------------------/

	/* MouseListener methods */
	public void mousePressed(MouseEvent me) { processCellAtPoint(me.getPoint()); parentMouseListener.mousePressed(me); }
	public void mouseReleased(MouseEvent me) {}
	public void mouseClicked(MouseEvent me) {}
	public void mouseEntered(MouseEvent me) { }
	public void mouseExited(MouseEvent me) {}

	/* MouseMotionListener methods */
	public void mouseMoved(MouseEvent me)
	{
		if(!me.getPoint().equals(ptLastGrid))
		{
			Point hotCell = getMouseCell(me.getPoint());
			if(!hotCell.equals(PT_OFFGRID))
			{
				highlightCell(hotCell.getX(), hotCell.getY());
				ptLastGrid.setLocation(me.getPoint().getX(), me.getPoint().getY());
			}
		}
	}
	public void mouseDragged(MouseEvent me) { processCellAtPoint(me.getPoint()); parentMouseMotionListener.mouseDragged(me); }

}

