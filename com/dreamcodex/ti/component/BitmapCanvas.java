package com.dreamcodex.ti.component;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.HashMap;
import javax.swing.*;

import com.dreamcodex.ti.util.TIGlobals;
import com.dreamcodex.ti.util.Globals;

public class BitmapCanvas extends JPanel implements MouseListener, MouseMotionListener
{
// Constants -------------------------------------------------------------------------------/

	public static final Point PT_OFFGRID = new Point(-1, -1);
	public static final int NOCHAR = -1;

	public static Color CLR_COPY  = new Color(255, 128, 128, 128);
	public static Color CLR_PASTE = new Color(128, 128, 255, 128);

	public final static int BRUSH_PIXEL = 0;
	public final static int BRUSH_THREE = 1;
	public final static int BRUSH_FIVE  = 2;
	public final static int BRUSH_GRID  = 3;
	public final static int BRUSH_COLOR = 4;

// Variables -------------------------------------------------------------------------------/

	protected int gridCols = 0;
	protected int gridRows = 0;
	protected int gridWidth = 0;
	protected int gridHeight = 0;
	protected int[][] bitmapData;
	protected int[][] colorData;
	protected int[][] cloneBitmapArray = null;
	protected int[][] cloneColorArray = null;
	protected int paintForeground;
	protected int paintBackground;
	protected boolean bColorForeground = true;
	protected boolean bColorBackground = true;
	protected int brushType = BRUSH_PIXEL;
	protected Color clrGrid;
	protected Color clrHigh;
	protected Color clrType;
	protected boolean paintOn = true;
	protected boolean cloneModeOn = false;
	protected boolean cloneBrush = false;
	protected int viewScale = 1;
	protected boolean showGrid = true;
	protected boolean dragging = false;
	protected int paintState = -1;

// Components ------------------------------------------------------------------------------/

	protected BufferedImage bufferDraw;
	protected Image currBufferScaled;

// Listeners -------------------------------------------------------------------------------/

	protected MouseListener parentMouseListener;
	protected MouseMotionListener parentMouseMotionListener;

// Convenience Variables -------------------------------------------------------------------/

	private Rectangle currBounds;
	private int gridOffsetX = 0;
	private int gridOffsetY = 0;
	private int cellSize = 8;
	private Point hotPoint = new Point(PT_OFFGRID);
	private Point clonePoint = new Point(PT_OFFGRID);
	private Rectangle rectClone = (Rectangle)null;
	private Point ptLastGrid = new Point(PT_OFFGRID);
	private boolean isTyping = false;
	private int pntX = 0;
	private int pntY = 0;

// Constructors ----------------------------------------------------------------------------/

	public BitmapCanvas(int cols, int rows, int cell, MouseListener mlParent, MouseMotionListener mmlParent)
	{
		super(true);
		this.setOpaque(true);

		cellSize = cell;
		gridCols = cols;
		gridRows = rows;
		gridWidth = gridCols * cellSize;
		gridHeight = gridRows;

		bitmapData = new int[gridRows][gridCols];
		colorData =  new int[gridRows][gridCols];
		paintForeground = 1;
		paintBackground = 15;
		brushType = BRUSH_PIXEL;
		for(int r = 0; r < gridRows; r++)
		{
			for(int c = 0; c < gridCols; c++)
			{
				bitmapData[r][c] = 0;
				colorData[r][c] = (paintForeground<<4 | paintBackground);
			}
		}
		clrGrid = new Color(192, 192, 192);
		clrHigh = new Color(255, 164, 164);
		clrType = new Color(164, 164, 255);

		bufferDraw = getImageBuffer(gridWidth, gridHeight);

		this.addMouseListener(this);
		this.addMouseMotionListener(this);

		parentMouseListener = mlParent;
		parentMouseMotionListener = mmlParent;
	}

// Accessors -------------------------------------------------------------------------------/

	public int     getGridCols()    { return gridCols; }
	public int     getGridRows()    { return gridRows; }
	public int     getGridWidth()   { return gridWidth; }
	public int     getGridHeight()  { return gridHeight; }
	public int[][] getBitmapData()  { return bitmapData; }
	public int[][] getColorData()   { return colorData; }
	public int     getPaintFore()   { return paintForeground; }
	public int     getPaintBack()   { return paintBackground; }
	public boolean isColorFore()    { return bColorForeground; }
	public boolean isColorBack()    { return bColorBackground; }
	public int     getBrushType()   { return brushType; }
	public int     getPaintByte()   { return (paintForeground<<4 | paintBackground); }
	public Color   getColorGrid()   { return clrGrid; }
	public Color   getColorHigh()   { return clrHigh; }
	public Color   getColorType()   { return clrType; }
	public boolean isPaintOn()      { return paintOn; }
	public boolean isCloneModeOn()  { return cloneModeOn; }
	public boolean isCloneBrush()  { return cloneBrush; }
	public int[][] getCloneBitmapArray() { return cloneBitmapArray; }
	public int[][] getCloneColorArray() { return cloneColorArray; }
	public int     getViewScale()   { return viewScale; }
	public boolean isShowGrid()     { return showGrid; }
	public Point   getHotPoint()    { return hotPoint; }
	public Point   getClonePoint()  { return clonePoint; }

	public void setBitmapData(int[][] gd) { bitmapData = gd; }
	public void setColorData(int[][] gd)  { colorData = gd; }
	public void setPaintFore(int i)       { paintForeground = i; }
	public void setPaintBack(int i)       { paintBackground = i; }
	public void setColorFore(boolean b)   { bColorForeground = b; }
	public void toggleColorFore()         { setColorFore(!isColorFore()); }
	public void setColorBack(boolean b)   { bColorBackground = b; }
	public void toggleColorBack()         { setColorBack(!isColorBack()); }
	public void setBrushType(int i)       { brushType = i; }
	public void setColorGrid(Color clr)   { clrGrid = clr; }
	public void setColorHigh(Color clr)   { clrHigh = clr; }
	public void setColorType(Color clr)   { clrType = clr; }
	public void setPaintOn(boolean b)     { paintOn = b; }
	public void setCloneModeOn(boolean b) { cloneModeOn = b; clonePoint.setLocation(PT_OFFGRID); cloneBitmapArray = null; cloneColorArray = null; rectClone = (Rectangle)null; }
	public void setCloneBrush(boolean b)  { cloneBrush = b; }
	public void toggleCloneMode()         { setCloneModeOn(!isCloneModeOn()); }
	public void setCloneBitmapArray(int[][] ar) { cloneBitmapArray = ar; }
	public void setCloneColorArray(int[][] ar) { cloneColorArray = ar; }
	public void setViewScale(int i)       { viewScale = i; }
	public void setShowGrid(boolean b)    { showGrid = b; }
	public void setHotPoint(Point pt)     { hotPoint.setLocation(pt); }
	public void setClonePoint(Point pt)   { clonePoint.setLocation(pt); }

	public BufferedImage getBuffer() { redrawCanvas(); return bufferDraw; }

	public void setGridCols(int i)
	{
		int[][] newGrid = new int[this.getGridRows()][i];
		int[][] newColor = new int[this.getGridRows()][i];
		for(int y = 0; y < this.getGridRows(); y++)
		{
			for(int x = 0; x < i; x++)
			{
				if(x < this.getGridCols())
				{
					newGrid[y][x] = bitmapData[y][x];
					newColor[y][x] = colorData[y][x];
				}
				else
				{
					newGrid[y][x] = 0;
					newColor[y][x] = getPaintByte();
				}
			}
		}
		bitmapData = newGrid;
		colorData = newColor;
		gridCols = i;
		gridWidth = gridCols * cellSize;
//		bufferDraw = getImageBuffer(gridWidth, gridHeight);
//		redrawCanvas(true);
	}
	public void setGridRows(int i)
	{
		int[][] newGrid = new int[i][this.getGridCols()];
		int[][] newColor = new int[i][this.getGridCols()];
		for(int y = 0; y < i; y++)
		{
			for(int x = 0; x < this.getGridCols(); x++)
			{
				if(y < this.getGridRows())
				{
					newGrid[y][x] = bitmapData[y][x];
					newColor[y][x] = colorData[y][x];
				}
				else
				{
					newGrid[y][x] = 0;
					newColor[y][x] = getPaintByte();
				}
			}
		}
		bitmapData = newGrid;
		colorData = newColor;
		gridRows = i;
		gridHeight = gridRows;
//		bufferDraw = getImageBuffer(gridWidth, gridHeight);
//		redrawCanvas(true);
	}
	public void setGridWidth(int i)
	{
		setGridCols((int)(Math.ceil(i / cellSize)));
	}
	public void setGridHeight(int i)
	{
		setGridRows(i);
	}

	public int getGridAt(int x, int y)
	{
		if(x >= 0 && y >= 0 && y < gridHeight && x < gridWidth)
		{
			int gridByte = bitmapData[y][(int)(Math.floor(x / cellSize))];
			int bytePos = (x % 8);
			return (gridByte & 1<<bytePos);
		}
		return 0;
	}

	public void setGridByte(int c, int r, int d)
	{
		if(c >= 0 && r >= 0 && r < gridRows && c < gridCols)
		{
			bitmapData[r][c] = d;
		}
	}

	public void setGridAt(int x, int y, int v)
	{
		if(x >= 0 && y >= 0 && y < gridHeight && x < gridWidth)
		{
			int gridByte = bitmapData[y][(int)(Math.floor(x / 8))];
			int bytePos = (x % 8);
			if(v != (gridByte & 1<<bytePos))
			{
				bitmapData[y][x] = (bitmapData[y][x] ^ 1<<bytePos);
			}
		}
	}

	public void setGridAt(Point pt, int v)
	{
		setGridAt((int)(pt.getX()), (int)(pt.getY()), v);
	}

	public void setColorByte(int c, int r, int d)
	{
		if(c >= 0 && r >= 0 && r < gridRows && c < gridCols)
		{
			colorData[r][c] = d;
		}
	}

	public void setColorAt(int x, int y, int c)
	{
		if(x >= 0 && y >= 0 && y < gridHeight && x < gridWidth)
		{
			colorData[y][(int)(Math.floor(x / cellSize))] = c;
		}
	}

	public void setColorAt(int x, int y, int f, int b)
	{
		setColorAt(x, y, (f<<4 | b));
	}

	public void setColorAt(Point pt, int f, int b)
	{
		setColorAt((int)(pt.getX()), (int)(pt.getY()), f, b);
	}

	public void toggleGrid()
	{
		this.setShowGrid(!this.isShowGrid());
		this.redrawCanvas(false);
	}

// Rendering Methods -----------------------------------------------------------------------/

	public void redrawCanvas(boolean forceRedraw)
	{
		if(this.getGraphics() != null && bufferDraw != null)
		{
			Graphics g = bufferDraw.getGraphics();
			if(forceRedraw || bufferDraw.getWidth() != gridWidth || bufferDraw.getHeight() != gridHeight)
			{
				bufferDraw = getImageBuffer(gridWidth, gridHeight);
				g = bufferDraw.getGraphics();
				for(int y = 0; y < gridHeight; y++)
				{
					for(int x = 0; x < gridWidth; x++)
					{
						int pixelColor = getColorFromByte(x, y, getDataBit(x, y));
						g.setColor(TIGlobals.TI_PALETTE_OPAQUE[pixelColor]);
						g.fillRect(x, y, 1, 1);
					}
				}
			}
			currBufferScaled = bufferDraw.getScaledInstance(gridWidth * viewScale, gridHeight * viewScale, BufferedImage.SCALE_REPLICATE);
			this.setPreferredSize(new Dimension(gridWidth * viewScale, gridHeight * viewScale));
			g.dispose();
			this.repaint();
		}
	}

	public void redrawCanvas()
	{
		redrawCanvas(false);
	}

	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		try
		{
			this.setPreferredSize(new Dimension(gridWidth * viewScale, gridHeight * viewScale));
			currBounds = this.getBounds();
			gridOffsetX = (currBounds.width - (viewScale * gridWidth)) / 2;
			gridOffsetY = (currBounds.height - (viewScale * gridHeight)) / 2;
			g.drawRect(gridOffsetX - 1, gridOffsetY - 1, (viewScale * gridWidth) + 1, (viewScale * gridHeight) + 1);
			if(currBufferScaled != null)
			{
				g.drawImage(currBufferScaled, gridOffsetX, gridOffsetY, this);
				if(showGrid)
				{
					g.setColor(clrGrid);
					for(int y = 0; y < (gridHeight / cellSize); y++)
					{
						for(int x = 0; x < (gridWidth / cellSize); x++)
						{
							g.drawRect(x * cellSize * viewScale + gridOffsetX, y * cellSize * viewScale + gridOffsetY, (cellSize * viewScale) - 1, (cellSize * viewScale) - 1);
						}
					}
				}
				if(cloneModeOn)
				{
					if(cloneBitmapArray != null)
					{
						if(!hotPoint.equals(PT_OFFGRID))
						{
							g.setColor(CLR_PASTE);
							g.fillRect((int)(hotPoint.getX()) * viewScale + gridOffsetX, (int)(hotPoint.getY()) * viewScale + gridOffsetY, cloneBitmapArray[0].length * cellSize * viewScale, cloneBitmapArray.length * viewScale);
						}
					}
					if(rectClone != null)
					{
						g.setColor(CLR_COPY);
						g.fillRect(rectClone.x * viewScale + gridOffsetX, rectClone.y * viewScale + gridOffsetY, (rectClone.width - rectClone.x) * viewScale, (rectClone.height - rectClone.y) * viewScale);
					}
					else
					{
						Rectangle rectTmp = getCloneRectangle(hotPoint);
						if(rectTmp != null)
						{
							g.setColor(CLR_COPY);
							g.fillRect(rectTmp.x * viewScale + gridOffsetX, rectTmp.y * viewScale + gridOffsetY, (rectTmp.width - rectTmp.x) * viewScale, (rectTmp.height - rectTmp.y) * viewScale);
						}
					}
				}
			}
			g.dispose();
		}
		catch(NullPointerException npe) { /* component not yet initialised */ }
	}

	public void setAllGrid(int v)
	{
		int pb = getPaintByte();
		int gb = ((v > 0) ? 255 : 0);
		for(int r = 0; r < gridRows; r++)
		{
			for(int c = 0; c < gridCols; c++)
			{
				setColorByte(c, r, pb);
				setGridByte(c, r, gb);
			}
		}
		redrawCanvas(true);
	}

	public void clearGrid() { setAllGrid(0); }
	public void fillGrid()  { setAllGrid(1); }

	protected Rectangle getCloneRectangle(Point endpoint)
	{
		if(clonePoint.equals(PT_OFFGRID) || endpoint.equals(PT_OFFGRID))
		{
			return (Rectangle)null;
		}
		else
		{
			return new Rectangle((int)(Math.min(clonePoint.getX(), endpoint.getX())), (int)(Math.min(clonePoint.getY(), endpoint.getY())), (int)(Math.max(clonePoint.getX(), endpoint.getX())) + cellSize, (int)(Math.max(clonePoint.getY(), endpoint.getY())) + cellSize);
		}
	}

	protected BufferedImage getImageBuffer(int width, int height)
	{
		BufferedImage bimgNew = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		while(bimgNew == null)
		{
			bimgNew = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			try { Thread.sleep(100); } catch(InterruptedException ie) {}
		}
		return bimgNew;
	}

// Class Methods ---------------------------------------------------------------------------/

	public Point getMousePoint(Point pt)
	{
		if(
			pt.getX() <= gridOffsetX ||
			pt.getY() <= gridOffsetY ||
			pt.getX() >= (gridOffsetX + (viewScale * gridWidth)) ||
			pt.getY() >= (gridOffsetY + (viewScale * gridHeight))
		)
		{
			return new Point(PT_OFFGRID);
		}
		int ptx = (int)((pt.getX() - gridOffsetX) / viewScale);
		int pty = (int)((pt.getY() - gridOffsetY) / viewScale);
		return new Point(ptx, pty);
	}

	public Point getMouseCell(Point pt)
	{
		if(
			pt.getX() <= gridOffsetX ||
			pt.getY() <= gridOffsetY ||
			pt.getX() >= (gridOffsetX + (viewScale * gridWidth)) ||
			pt.getY() >= (gridOffsetY + (viewScale * gridHeight))
		)
		{
			return new Point(PT_OFFGRID);
		}
		int ptx = (int)((pt.getX() - gridOffsetX) / (cellSize * viewScale));
		int pty = (int)((pt.getY() - gridOffsetY) / (cellSize * viewScale));
		return new Point(ptx, pty);
	}

	public void processCellAtPoint(Point pt)
	{
		hotPoint.setLocation(getMousePoint(pt));
		if(!hotPoint.equals(PT_OFFGRID))
		{
			pntX = (int)(hotPoint.getX());
			pntY = (int)(hotPoint.getY());
			if(paintState == -1)
			{
				paintState = (getDataBit(pntX, pntY) == 0 ? 1 : 0);
			}
			int colorBit = getColorByte(pntX, pntY);
			if(bColorForeground && bColorBackground)
			{
				colorBit = (paintForeground<<4 | paintBackground);
			}
			else if(bColorForeground)
			{
				colorBit = (paintForeground<<4 | (colorBit & Globals.MASK_LOBITS));
			}
			else if(bColorBackground)
			{
				colorBit = ((colorBit & Globals.MASK_HIBITS) | paintBackground);
			}
			if(brushType == BRUSH_PIXEL)
			{
				setColorBit(pntX, pntY, colorBit);
				setDataBit(pntX, pntY, paintState);
			}
			else if(brushType == BRUSH_THREE)
			{
				for(int y = Math.max(0, pntY - 1); y < Math.min(gridHeight - 1, pntY + 1); y++)
				{
					for(int x = Math.max(0, pntX - 1); x < Math.min(gridWidth - 1, pntX + 1); x++)
					{
						setColorBit(x, y, colorBit);
						setDataBit(x, y, paintState);
					}
				}
			}
			else if(brushType == BRUSH_FIVE)
			{
				for(int y = Math.max(0, pntY - 2); y < Math.min(gridHeight - 1, pntY + 2); y++)
				{
					for(int x = Math.max(0, pntX - 2); x < Math.min(gridWidth - 1, pntX + 2); x++)
					{
						setColorBit(x, y, colorBit);
						setDataBit(x, y, paintState);
					}
				}
			}
			else if(brushType == BRUSH_GRID)
			{
				int gridX = ((pntX - (pntX % cellSize)) / cellSize) * cellSize;
				int gridY = ((pntY - (pntY % cellSize)) / cellSize) * cellSize;
				for(int y = gridY; y < gridY + 8; y++)
				{
					setColorBit(gridX, y, colorBit);
					for(int x = gridX; x < gridX + 8; x++)
					{
						setDataBit(x, y, paintState);
					}
				}
			}
			else if(brushType == BRUSH_COLOR)
			{
				setColorBit(pntX, pntY, colorBit);
			}
			redrawCanvas(false);
		}
	}

	public void clearCloneBuffer()
	{
		cloneModeOn = false;
		rectClone = (Rectangle)null;
		cloneBitmapArray = null;
		cloneColorArray = null;
	}

// Byte Processing Methods -----------------------------------------------------------------/

	public int getDataBit(int x, int y)
	{
		int dByte = bitmapData[y][(int)(Math.floor(x / 8))];
		int bytePos = (x % 8);
		return ((dByte & 1<<bytePos) == (1<<bytePos) ? 1 : 0);
	}

	public int getColorByte(int x, int y)
	{
		return colorData[y][(int)(Math.floor(x / 8))];
	}

	public int getColorFromByte(int x, int y, int v)
	{
		int dByte = colorData[y][(int)(Math.floor(x / 8))];
		return (v == 1 ? ((dByte & Globals.MASK_HIBITS)>>4) : (dByte & Globals.MASK_LOBITS));
	}

	public void setDataBit(int x, int y, int v)
	{
		int xs = (int)(Math.floor(x / 8));
		int dByte = bitmapData[y][xs];
		int bytePos = (x % 8);
		int dFlag = ((dByte & 1<<bytePos) == (1<<bytePos) ? 1 : 0);
		if(v != dFlag)
		{
			bitmapData[y][xs] = (dByte ^ 1<<bytePos);
			if(this.getGraphics() != null && bufferDraw != null)
			{
				Graphics g = bufferDraw.getGraphics();
				int pixelColor = getColorFromByte(x, y, getDataBit(x, y));
				g.setColor(TIGlobals.TI_PALETTE_OPAQUE[pixelColor]);
				g.fillRect(x, y, 1, 1);
			}
		}
	}

	public void setColorBit(int x, int y, int c)
	{
		colorData[y][(int)(Math.floor(x / 8))] = c;
		if(this.getGraphics() != null && bufferDraw != null)
		{
			Graphics g = bufferDraw.getGraphics();
			int xp = x - (x % 8);
			for(int xd = 0; xd < 8; xd++)
			{
				int pixelColor = getColorFromByte(x, y, getDataBit(xp + xd, y));
				g.setColor(TIGlobals.TI_PALETTE_OPAQUE[pixelColor]);
				g.fillRect(xp + xd, y, 1, 1);
			}
		}
	}

// Listener Methods ------------------------------------------------------------------------/

	/* MouseListener methods */
	public void mousePressed(MouseEvent me)
	{
		if(cloneModeOn)
		{
			Point clickCell = getMouseCell(me.getPoint());
			if(!clickCell.equals(PT_OFFGRID))
			{
				hotPoint.setLocation(new Point((int)(clickCell.getX() * cellSize), (int)(clickCell.getY() * cellSize)));
			}
		}
		else
		{
			processCellAtPoint(me.getPoint());
			parentMouseListener.mousePressed(me);
		}
	}
	public void mouseReleased(MouseEvent me)
	{
		dragging = false;
		paintState = -1;
	}
	public void mouseClicked(MouseEvent me)
	{
		if(cloneModeOn)
		{
			Point clickPoint = getMouseCell(me.getPoint());
			if(!clickPoint.equals(PT_OFFGRID))
			{
				clickPoint.setLocation(new Point((int)(clickPoint.getX() * cellSize), (int)(clickPoint.getY() * cellSize)));
			}
			if(cloneBitmapArray != null && !clickPoint.equals(PT_OFFGRID))
			{
				int pasteX = (int)(Math.floor(clickPoint.getX() / cellSize));
				int pasteY = (int)(clickPoint.getY());
				for(int y = 0; y < cloneBitmapArray.length; y++)
				{
					int plotY = pasteY + y;
					if(plotY >= 0 && plotY < bitmapData.length)
					{
						for(int x = 0; x < cloneBitmapArray[y].length; x++)
						{
							int plotX = pasteX + x;
							if(plotX >= 0 && plotX < bitmapData[y].length)
							{
								bitmapData[plotY][plotX] = cloneBitmapArray[y][x];
								colorData[plotY][plotX] = cloneColorArray[y][x];
							}
						}
					}
				}
				if(!cloneBrush)
				{
					clearCloneBuffer();
				}
				redrawCanvas(true);
				parentMouseListener.mouseClicked(me);
			}
			else if(!clonePoint.equals(PT_OFFGRID))
			{
				if(!clickPoint.equals(PT_OFFGRID))
				{
					rectClone = getCloneRectangle(clickPoint);
					int rectPosX = (int)(Math.ceil(rectClone.x / cellSize));
					int rectPosY = rectClone.y;
					int rectCols = (int)(Math.ceil((rectClone.width - rectClone.x) / cellSize));
					int rectRows = rectClone.height - rectClone.y;
					cloneBitmapArray = new int[rectRows][rectCols];
					cloneColorArray = new int[rectRows][rectCols];
					for(int y = 0; y < rectRows; y++)
					{
						for(int x = 0; x < rectCols; x++)
						{
							cloneBitmapArray[y][x] = bitmapData[rectPosY + y][rectPosX + x];
							cloneColorArray[y][x] = colorData[rectPosY + y][rectPosX + x];
						}
					}
//					rectClone = (Rectangle)null;
					clonePoint.setLocation(PT_OFFGRID);
				}
				else
				{
					rectClone = (Rectangle)null;
					cloneBitmapArray = null;
					cloneColorArray = null;
				}
			}
			else
			{
				clonePoint.setLocation(clickPoint);
			}
		}
	}
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}

	/* MouseMotionListener methods */
	public void mouseMoved(MouseEvent me)
	{
		if(!me.getPoint().equals(ptLastGrid))
		{
			if(cloneModeOn)
			{
				Point clickCell = getMouseCell(me.getPoint());
				if(!clickCell.equals(PT_OFFGRID))
				{
					hotPoint.setLocation(new Point((int)(clickCell.getX() * cellSize), (int)(clickCell.getY() * cellSize)));
				}
			}
			else
			{
				hotPoint = getMousePoint(me.getPoint());
			}
			ptLastGrid.setLocation(me.getPoint());
			parentMouseMotionListener.mouseMoved(me);
		}
	}
	public void mouseDragged(MouseEvent me)
	{
		dragging = true;
		if(cloneModeOn)
		{
			Point clickCell = getMouseCell(me.getPoint());
			if(!clickCell.equals(PT_OFFGRID))
			{
				hotPoint.setLocation(new Point((int)(clickCell.getX() * cellSize), (int)(clickCell.getY() * cellSize)));
			}
		}
		else
		{
			processCellAtPoint(me.getPoint());
			parentMouseListener.mousePressed(me);
		}
	}
}
