package com.dreamcodex.ti.component;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.net.URL;
import java.util.HashMap;
import java.util.ArrayList;
import javax.swing.*;

import com.dreamcodex.ti.component.MapCanvas;
import com.dreamcodex.ti.component.ColorComboRenderer;
import com.dreamcodex.ti.util.Globals;
import com.dreamcodex.ti.util.TIGlobals;

public class MapEditor extends JPanel implements ItemListener, ActionListener, KeyListener
{
// Constants -------------------------------------------------------------------------------/

	public static final String[] MAGNIFICATIONS = { "1x", "2x", "3x", "4x", "5x", "6x", "7x", "8x" };

// Components ------------------------------------------------------------------------------/

	protected MapCanvas mpCanvas;
	protected JScrollPane jsclCanvas;
	protected JComboBox jcmbScreen;
	protected JComboBox jcmbMagnif;
	protected JButton jbtnPrevMap;
	protected JButton jbtnNextMap;
	protected JButton jbtnBackMap;
	protected JButton jbtnForwMap;
	protected JButton jbtnTextCursor;
	protected JLabel jlblMapNum;
	protected JTextField jtxtWidth;
	protected JTextField jtxtHeight;
	protected JLabel jlblPosLabel;
	protected JLabel jlblPosIndic;

// Variables -------------------------------------------------------------------------------/

	protected ArrayList<int[][]> arMaps = new ArrayList<int[][]>();
	protected ArrayList<Integer> arClrs = new ArrayList<Integer>();
	protected int currMap = 0;
	protected boolean showPositionIndicator = true;
    protected boolean basePositionIsZero    = false;

// Constructors ----------------------------------------------------------------------------/

	public MapEditor(int gridWidth, int gridHeight, int cellSize, MouseListener mlParent, MouseMotionListener mmlParent)
	{
		super(new BorderLayout(), true);
		this.setOpaque(true);

		mpCanvas = new MapCanvas(gridWidth, gridHeight, cellSize, mlParent, mmlParent);

		this.addKeyListener(this);

		arMaps.add(mpCanvas.getGridData());
		arClrs.add(new Integer(mpCanvas.getColorScreen()));

		jcmbScreen = new JComboBox(TIGlobals.TI_PALETTE_SELECT_VALUES);
		jcmbScreen.addItemListener(this);
		jcmbScreen.setSelectedIndex(mpCanvas.getColorScreen());
		jcmbScreen.setRenderer(new ColorComboRenderer());

		jcmbMagnif = new JComboBox(MAGNIFICATIONS);
		jcmbMagnif.addItemListener(this);
		jcmbMagnif.setSelectedIndex(0);

		JButton jbtnFillSpace = getToolButton(Globals.CMD_CLEAR, "Fill map with spaces");
		JButton jbtnFillActive = getToolButton(Globals.CMD_FILL, "Fill map with active character");
		JButton jbtnToggleGrid = getToolButton(Globals.CMD_GRID, "Toggle grid on and off");
		JButton jbtnAddMap = getToolButton(Globals.CMD_ADDMAP, "Add new map"); jbtnAddMap.setBackground(new Color(192, 255, 192));
		JButton jbtnDelMap = getToolButton(Globals.CMD_DELMAP, "Delete current map"); jbtnDelMap.setBackground(new Color(255, 192, 192));
		jbtnPrevMap = getToolButton(Globals.CMD_PREVMAP, "Go to previous map");
		jbtnNextMap = getToolButton(Globals.CMD_NEXTMAP, "Go to next map");
		jbtnBackMap = getToolButton(Globals.CMD_BACKMAP, "Move map backward in set");
		jbtnForwMap = getToolButton(Globals.CMD_FORWMAP, "Move map forward in set");
		jbtnTextCursor = getToolButton(Globals.CMD_TCURSOR, "Toggle text cursor display"); jbtnTextCursor.setBackground(mpCanvas.showTypeCell() ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL);
		jlblMapNum = getLabel("", JLabel.CENTER);
		jtxtWidth = getTextField("" + gridWidth);
		jtxtHeight = getTextField("" + gridHeight);
		jlblPosLabel = getLabel(" Pos X/Y: ", JLabel.RIGHT);
		jlblPosIndic = getLabel("", JLabel.CENTER);

		JPanel jpnlTools = getPanel(new FlowLayout(FlowLayout.LEFT));
		jpnlTools.setOpaque(true);
		jpnlTools.setBackground(Globals.CLR_COMPONENTBACK);
		jpnlTools.add(getLabel("Screen", JLabel.RIGHT));
		jpnlTools.add(jcmbScreen);
		jpnlTools.add(getLabel("Mag", JLabel.RIGHT));
		jpnlTools.add(jcmbMagnif);
		jpnlTools.add(jbtnFillSpace);
		jpnlTools.add(jbtnFillActive);
		jpnlTools.add(jbtnToggleGrid);
		jpnlTools.add(jbtnTextCursor);
		jpnlTools.add(jbtnAddMap);
		jpnlTools.add(jbtnPrevMap);
		jpnlTools.add(jlblMapNum);
		jpnlTools.add(jbtnNextMap);
		jpnlTools.add(jbtnDelMap);
		jpnlTools.add(jbtnBackMap);
		jpnlTools.add(jbtnForwMap);
		jpnlTools.add(getLabel(" W:", JLabel.RIGHT));
		jpnlTools.add(jtxtWidth);
		jpnlTools.add(getLabel(" H:", JLabel.RIGHT));
		jpnlTools.add(jtxtHeight);
		jpnlTools.add(jlblPosLabel);
		jpnlTools.add(jlblPosIndic);

		jsclCanvas = new JScrollPane(mpCanvas);
		jsclCanvas.setBorder(BorderFactory.createLineBorder(new Color(64, 64, 64), 1));

		this.add(jsclCanvas, BorderLayout.CENTER);
		this.add(jpnlTools, BorderLayout.NORTH);
	}

// Accessors -------------------------------------------------------------------------------/

	public int[][] getGridData()     { return mpCanvas.getGridData(); }
	public Color   getColorGrid()    { return mpCanvas.getColorGrid(); }
	public Color   getColorHigh()    { return mpCanvas.getColorHigh(); }
	public boolean isPaintOn()       { return mpCanvas.isPaintOn(); }
	public int     getActiveChar()   { return mpCanvas.getActiveChar(); }
	public int     getViewScale()    { return mpCanvas.getViewScale(); }
	public int     getColorScreen()  { return mpCanvas.getColorScreen(); }
	public boolean isShowGrid()      { return mpCanvas.isShowGrid(); }
	public boolean isLookModeOn()    { return mpCanvas.isLookModeOn(); }
	public boolean isCloneModeOn()   { return mpCanvas.isCloneModeOn(); }
	public int     getLookChar()     { return mpCanvas.getLookChar(); }
	public Point   getHotCell()      { return mpCanvas.getHotCell(); }
	public Point   getTypeCell()     { return mpCanvas.getTypeCell(); }
	public boolean showTypeCell()    { return mpCanvas.showTypeCell(); }
	public Color   getBkgrndColor()  { return this.getBackground(); }
	public int     getMapCount()     { return arMaps.size(); }
	public int     getCurrentMapId() { return currMap; }
	public boolean showPosIndic()    { return showPositionIndicator; }
	public boolean isBasePosZero()   { return basePositionIsZero; }
	public int[][] getMapData(int i)
	{
		if((i == currMap) || (i < 0) || (i >= arMaps.size()))
		{
			return getGridData();
		}
		else
		{
			return arMaps.get(i);
		}
	}
	public int getScreenColor(int i)
	{
		if((i == currMap) || (i < 0) || (i >= arClrs.size()))
		{
			return getColorScreen();
		}
		else
		{
			return arClrs.get(i).intValue();
		}
	}
	public int getScreenColorTI(int i)
	{
		return getScreenColor(i) + 1;
	}

	public void setGridData(int[][] gd)      { mpCanvas.setGridData(gd); }
	public void setColorGrid(Color clr)      { mpCanvas.setColorGrid(clr); }
	public void setColorHigh(Color clr)      { mpCanvas.setColorHigh(clr); }
	public void setPaintOn(boolean b)        { mpCanvas.setPaintOn(b); }
	public void setActiveChar(int i)         { mpCanvas.setActiveChar(i); }
	public void setViewScale(int i)          { mpCanvas.setViewScale(i); jcmbMagnif.setSelectedIndex(i - 1); }
	public void setColorScreen(int i)        { mpCanvas.setColorScreen(i); jcmbScreen.setSelectedIndex(i); }
	public void setShowGrid(boolean b)       { mpCanvas.setShowGrid(b); }
	public void setLookModeOn(boolean b)     { mpCanvas.setLookModeOn(b); }
	public void setCloneModeOn(boolean b)    { mpCanvas.setCloneModeOn(b); }
	public void toggleCloneMode()            { mpCanvas.toggleCloneMode(); }
	public void setLookChar(int i)           { mpCanvas.setLookChar(i); }
	public void setHotCell(Point pt)         { mpCanvas.setHotCell(pt); }
	public void setTypeCell(Point pt)        { mpCanvas.setTypeCell(pt); }
	public void setTypeCellOn(boolean b)     { mpCanvas.setTypeCellOn(b); }
	public void setBkgrndColor(Color clr)    { this.setBackground(clr); mpCanvas.setBackground(clr); }
	public void setCurrentMapId(int i)       { currMap = Math.max(Math.min((arMaps.size() - 1), i), 0); }
	public void setShowPosIndic(boolean b)
	{
		showPositionIndicator = b;
		jlblPosLabel.setVisible(showPositionIndicator);
		jlblPosIndic.setVisible(showPositionIndicator);
		updateComponents();
	}
	public void toggleShowPosIndic()         { setShowPosIndic(!showPositionIndicator); }
	public void setBasePosZero(boolean b)
	{
		basePositionIsZero = b;
		updateComponents();
	}
	public void toggleBasePosition()         { setBasePosZero(!basePositionIsZero); }
	public void addMapData(int[][] m, int c) { addMap(m, c); }
	public void setMapData(int i, int[][] m, int c) { storeMap(i, m, c); }
	public void setScreenColor(int i, int c) { arClrs.set(i, new Integer(c)); }

	public int getGridWidth()  { return mpCanvas.getGridWidth(); }
	public int getGridHeight() { return mpCanvas.getGridHeight(); }

	public void setGridWidth(int i)  { mpCanvas.setGridWidth(i); }
	public void setGridHeight(int i) { mpCanvas.setGridHeight(i); }

	public int getGridAt(int x, int y)
	{
		return mpCanvas.getGridAt(x, y);
	}

	public void setGridAt(int x, int y, int v)
	{
		mpCanvas.setGridAt(x, y, v);
	}

	public void setGridAt(Point pt, int v)
	{
		mpCanvas.setGridAt(pt, v);
	}

	public void setCharImage(int charnum, Image img)
	{
		mpCanvas.setCharImage(charnum, img);
	}

	public BufferedImage getBuffer()
	{
		return mpCanvas.getBuffer();
	}

	public void advanceTypeCell()
	{
		mpCanvas.advanceTypeCell();
	}

	public void redrawCanvas()
	{
		mpCanvas.redrawCanvas();
	}

	public void addMap(int[][] mapdata, int scrclr)
	{
		arMaps.add(mapdata);
		arClrs.add(new Integer(scrclr));
	}

	public void addBlankMap(int w, int h)
	{
		int[][] newMap = new int[h][w];
		for(int y = 0; y < newMap.length; y++)
		{
			for(int x = 0; x < newMap[y].length; x++)
			{
				newMap[y][x] = TIGlobals.SPACECHAR;
			}
		}
		arMaps.add(newMap);
		arClrs.add(new Integer((mpCanvas != null ? getColorScreen() : 15)));
	}

	public void addBlankMap()
	{
		addBlankMap(getGridWidth(), getGridHeight());
	}

	public void delMap(int index)
	{
		if((index >= 0) && (index < arMaps.size()))
		{
			arMaps.remove(index);
			arClrs.remove(index);
			index--;
			if(index < 0) { index = 0; }
			if(arMaps.size() < 1) { addBlankMap(); }
			goToMap(index, false);
		}
	}

	public void delAllMaps()
	{
		clearGrid();
		for(int i = arMaps.size() - 1; i >= 0; i--)
		{
			delMap(i);
		}
	}

	public void storeMap(int index, int[][] mapdata, int scrcolor)
	{
		if((index >= 0) && (index < arMaps.size()))
		{
			arMaps.set(index, cloneMapArray(mapdata));
		}
		if((index >= 0) && (index < arClrs.size()))
		{
			arClrs.set(index, new Integer(scrcolor));
		}
	}

	public void storeCurrentMap()
	{
		storeMap(currMap, mpCanvas.getGridData(), mpCanvas.getColorScreen());
	}

	public void updateState(int index)
	{
		mpCanvas.setGridData(arMaps.get(index));
		mpCanvas.setColorScreen(arClrs.get(index).intValue());
	}

	public int[][] cloneMapArray(int[][] mapsrc)
	{
		int[][] cloneArray = new int[mapsrc.length][mapsrc[0].length];
		for(int y = 0; y < mapsrc.length; y++)
		{
			for(int x = 0; x < mapsrc[y].length; x++)
			{
				cloneArray[y][x] = mapsrc[y][x];
			}
		}
		return cloneArray;
	}

	public void goToMap(int index, boolean storeCurr)
	{
		if((index >= 0) && (index < arMaps.size()))
		{
			if(storeCurr) { storeCurrentMap(); }
			currMap = index;
			updateState(currMap);
			updateComponents();
		}
	}

	public void goToMap(int index)
	{
		goToMap(index, true);
	}

	public void swapMaps(int mapA, int mapB)
	{
		if(mapA != mapB)
		{
			int[][] tmpMap = getMapData(mapA);
			Integer tmpClr = getScreenColor(mapA);
			storeMap(mapA, getMapData(mapB), getScreenColor(mapB));
			storeMap(mapB, tmpMap, tmpClr);
		}
	}

	public int moveMap(int index, boolean forward)
	{
		if(index < 0 || index >= arMaps.size())
		{
			return index;
		}
		int newIndex = index + (forward ? 1 : -1);
		if     (newIndex < 0) { newIndex = arMaps.size() - 1; }
		else if(newIndex >= arMaps.size()) { newIndex = 0; }
		swapMaps(index, newIndex);
		return newIndex;
	}
	public int moveMapForward(int index) { return moveMap(index, true); }
	public int moveMapBackward(int index) { return moveMap(index, false); }

// Component Builder Methods ---------------------------------------------------------------/

	protected JPanel getPanel(LayoutManager layout)
	{
		JPanel jpnlRtn = new JPanel(layout);
		jpnlRtn.setOpaque(true);
		jpnlRtn.setBackground(Globals.CLR_COMPONENTBACK);
		return jpnlRtn;
	}

	protected ImageIcon getIcon(String name)
	{
		URL imageURL = getClass().getResource("images/icon_" + name + ".png");
		return new ImageIcon(Toolkit.getDefaultToolkit().getImage(imageURL));
	}

	protected JButton getToolButton(String actcmd, String tooltip)
	{
		JButton jbtnTool = new JButton(getIcon(actcmd));
		jbtnTool.setActionCommand(actcmd);
		jbtnTool.addActionListener(this);
		jbtnTool.setToolTipText(tooltip);
		jbtnTool.setMargin(new Insets(1, 1, 1, 1));
		jbtnTool.setOpaque(true);
		jbtnTool.setBackground(Globals.CLR_BUTTON_NORMAL);
		jbtnTool.setPreferredSize(Globals.DM_TOOL);
		return jbtnTool;
	}

	protected JLabel getLabel(String text, int align)
	{
		JLabel jlblRtn = new JLabel(text, align);
		jlblRtn.setOpaque(true);
		jlblRtn.setBackground(Globals.CLR_COMPONENTBACK);
		return jlblRtn;
	}

	protected JTextField getTextField(String text)
	{
		JTextField jtxtRtn = new JTextField(text);
		jtxtRtn.addActionListener(this);
		jtxtRtn.setPreferredSize(Globals.DM_TOOL);
		jtxtRtn.setBorder(BorderFactory.createLineBorder(new Color(64, 64, 64), 1));
		return jtxtRtn;
	}

// Rendering Methods -----------------------------------------------------------------------/

	public void setAllGrid(int v)
	{
		mpCanvas.setAllGrid(v);
	}

	public void clearGrid()     { mpCanvas.clearGrid(); }
	public void fillGrid(int v) { mpCanvas.fillGrid(v); }

	public void updateComponents()
	{
		redrawCanvas();
		if(arMaps.size() > 1)
		{
			jbtnPrevMap.setEnabled(true);
			jbtnNextMap.setEnabled(true);
			jbtnBackMap.setEnabled(true);
			jbtnForwMap.setEnabled(true);
		}
		else
		{
			jbtnPrevMap.setEnabled(false);
			jbtnNextMap.setEnabled(false);
			jbtnBackMap.setEnabled(false);
			jbtnForwMap.setEnabled(false);
		}
		jcmbScreen.setSelectedIndex(mpCanvas.getColorScreen());
		jsclCanvas.revalidate();
		jlblMapNum.setText((currMap + 1) + " / " + getMapCount());
		jtxtWidth.setText("" + mpCanvas.getGridWidth());
		jtxtHeight.setText("" + mpCanvas.getGridHeight());
		jbtnTextCursor.setBackground(mpCanvas.showTypeCell() ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL);
		if(showPositionIndicator)
		{
			if(getHotCell().equals(MapCanvas.PT_OFFGRID))
			{
				jlblPosIndic.setText("-/-  (-/-)");
			}
			else
			{
                int posx     = (int) getHotCell().getX();
                int posy     = (int) getHotCell().getY();
                if(!basePositionIsZero) {
                   posx++;
                   posy++;
                }
                String hposx = Integer.toHexString(posx).toUpperCase();
                String hposy = Integer.toHexString(posy).toUpperCase();
                hposx        = "$000".substring(0,4 - hposx.length()) + hposx;
                hposy        = "$000".substring(0,4 - hposy.length()) + hposy;                
				jlblPosIndic.setText(posx + "/" + posy + "  (" + hposx + "/" + hposy + ")");
			}
		}
	}

// Listener Methods ------------------------------------------------------------------------/

	/* ItemListener methods */
	public void itemStateChanged(ItemEvent ie)
	{
		if(ie.getSource().equals(jcmbScreen))
		{
			mpCanvas.setColorScreen(jcmbScreen.getSelectedIndex());
		}
		else if(ie.getSource().equals(jcmbMagnif))
		{
			mpCanvas.setViewScale(jcmbMagnif.getSelectedIndex() + 1);
			jsclCanvas.revalidate();
		}
		redrawCanvas();
	}

	/* ActionListener methods */
	public void actionPerformed(ActionEvent ae)
	{
		try
		{
			if(ae.getSource().equals(jtxtWidth))
			{
				try
				{
					int newWidth = Integer.parseInt(jtxtWidth.getText());
					mpCanvas.setGridWidth(newWidth);
					jsclCanvas.revalidate();
				}
				catch(NumberFormatException nfe)
				{
					jtxtWidth.setText("" + mpCanvas.getGridWidth());
				}
			}
			else if(ae.getSource().equals(jtxtHeight))
			{
				try
				{
					int newHeight = Integer.parseInt(jtxtHeight.getText());
					mpCanvas.setGridHeight(newHeight);
					jsclCanvas.revalidate();
				}
				catch(NumberFormatException nfe)
				{
					jtxtHeight.setText("" + mpCanvas.getGridHeight());
				}
			}
			else
			{
				String command = ae.getActionCommand();
				if(command.equals(Globals.CMD_CLEAR))
				{
					clearGrid();
					updateComponents();
				}
				else if(command.equals(Globals.CMD_FILL))
				{
					fillGrid(mpCanvas.getActiveChar());
					updateComponents();
				}
				else if(command.equals(Globals.CMD_GRID))
				{
					mpCanvas.toggleGrid();
					updateComponents();
				}
				else if(command.equals(Globals.CMD_TCURSOR))
				{
					mpCanvas.toggleTextCursor();
					updateComponents();
					mpCanvas.requestFocus();
				}
				else if(command.equals(Globals.CMD_ADDMAP))
				{
					storeCurrentMap();
					clearGrid();
					addMap(mpCanvas.getGridData(), mpCanvas.getColorScreen());
					currMap = (arMaps.size() - 1);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_DELMAP))
				{
					clearGrid();
					delMap(currMap);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_PREVMAP))
				{
					storeCurrentMap();
					currMap--;
					if(currMap < 0) { currMap = (arMaps.size() - 1); }
					updateState(currMap);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_NEXTMAP))
				{
					storeCurrentMap();
					currMap++;
					if(currMap >= arMaps.size()) { currMap = 0; }
					updateState(currMap);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_BACKMAP))
				{
					storeCurrentMap();
					currMap = moveMapBackward(currMap);
					goToMap(currMap);
					updateComponents();
				}
				else if(command.equals(Globals.CMD_FORWMAP))
				{
					storeCurrentMap();
					currMap = moveMapForward(currMap);
					goToMap(currMap);
					updateComponents();
				}
			}
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	/* KeyListener methods */
	public void keyTyped(KeyEvent ke)    { mpCanvas.keyTyped(ke); }
	public void keyPressed(KeyEvent ke)  { mpCanvas.keyPressed(ke); }
	public void keyReleased(KeyEvent ke) { mpCanvas.keyReleased(ke); }
}
