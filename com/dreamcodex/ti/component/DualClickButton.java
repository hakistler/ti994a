package com.dreamcodex.ti.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;

public class DualClickButton extends JButton implements MouseListener
{
	private String leftClickCommand;
	private String rightClickCommand;
	private ActionListener parent;

	public DualClickButton(String textLabel, String leftCmd, String rightCmd, ActionListener aparent)
	{
		super(textLabel);
		leftClickCommand = new String(leftCmd);
		rightClickCommand = new String(rightCmd);
		parent = aparent;
		this.addMouseListener(this);
		this.addActionListener(parent);
	}

	public void mousePressed(MouseEvent me)
	{
		if(me.getButton() == MouseEvent.BUTTON3)
		{
			this.setActionCommand(rightClickCommand);
		}
		else
		{
			this.setActionCommand(leftClickCommand);
		}
	}
	public void mouseReleased(MouseEvent me)
	{
		ActionEvent aeInit = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, this.getActionCommand());
		parent.actionPerformed(aeInit);
	}
	public void mouseClicked(MouseEvent me) {}
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}
}
