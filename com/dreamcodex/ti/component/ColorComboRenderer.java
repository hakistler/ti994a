package com.dreamcodex.ti.component;

import java.awt.Component;
import javax.swing.*;

import com.dreamcodex.ti.util.TIGlobals;

public class ColorComboRenderer extends JLabel implements ListCellRenderer
{
	public ColorComboRenderer()
	{
		setOpaque(true);
		setHorizontalAlignment(LEFT);
		setVerticalAlignment(CENTER);
	}

	public Component getListCellRendererComponent(JList jlist, Object value, int index, boolean isSelected, boolean cellHasFocus)
	{
		int thisIndex = ((Integer)value).intValue();
		this.setText(TIGlobals.TI_PALETTE_NAMES[thisIndex]);
		this.setBackground(TIGlobals.TI_PALETTE_OPAQUE[thisIndex]);
		if(thisIndex == 1) { this.setForeground(TIGlobals.TI_PALETTE[15]); } else { this.setForeground(TIGlobals.TI_PALETTE[1]); }
		return this;
	}
}
