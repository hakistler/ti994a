package com.dreamcodex.ti.util;

import java.awt.Color;

public class TIGlobals
{
	public static final Color TI_COLOR_TRANSPARENT = new Color(255, 255, 255, 0);
	public static final Color TI_COLOR_BLACK       = new Color(  0,   0,   0);//000000
	public static final Color TI_COLOR_GREY        = new Color(204, 204, 204);//CCCCCC
	public static final Color TI_COLOR_WHITE       = new Color(255, 255, 255);//FFFFFF
	public static final Color TI_COLOR_RED_LIT     = new Color(255, 121, 120);//FF7978
	public static final Color TI_COLOR_RED_MED     = new Color(252,  85,  84);//FC5554
	public static final Color TI_COLOR_RED_DRK     = new Color(212,  82,  77);//D4524D
	public static final Color TI_COLOR_YELLOW_LIT  = new Color(230, 206, 128);//E6CE80
	public static final Color TI_COLOR_YELLOW_DRK  = new Color(212, 193,  84);//D4C154
	public static final Color TI_COLOR_GREEN_LIT   = new Color( 94, 220, 120);//5EDC78
	public static final Color TI_COLOR_GREEN_MED   = new Color( 33, 200,  66);//21C842
	public static final Color TI_COLOR_GREEN_DRK   = new Color( 33, 176,  59);//21B03B
	public static final Color TI_COLOR_CYAN        = new Color( 66, 235, 245);//42EBF5
	public static final Color TI_COLOR_BLUE_LIT    = new Color(125, 118, 252);//7D76FC
	public static final Color TI_COLOR_BLUE_DRK    = new Color( 84,  85, 237);//5455ED
	public static final Color TI_COLOR_MAGENTA     = new Color(201,  91, 186);//C95BBA

	public static final Color TI_COLOR_TRANSOPAQUE = new Color(212, 232, 255);
	public static final Color TI_COLOR_UNUSED      = new Color(160, 160, 160);

	public static final Color[] TI_PALETTE =
	{
		TI_COLOR_TRANSPARENT,
		TI_COLOR_BLACK,
		TI_COLOR_GREEN_MED,
		TI_COLOR_GREEN_LIT,
		TI_COLOR_BLUE_DRK,
		TI_COLOR_BLUE_LIT,
		TI_COLOR_RED_DRK,
		TI_COLOR_CYAN,
		TI_COLOR_RED_MED,
		TI_COLOR_RED_LIT,
		TI_COLOR_YELLOW_DRK,
		TI_COLOR_YELLOW_LIT,
		TI_COLOR_GREEN_DRK,
		TI_COLOR_MAGENTA,
		TI_COLOR_GREY,
		TI_COLOR_WHITE
	};

	public static final Color[] TI_PALETTE_OPAQUE =
	{
		TI_COLOR_TRANSOPAQUE,
		TI_COLOR_BLACK,
		TI_COLOR_GREEN_MED,
		TI_COLOR_GREEN_LIT,
		TI_COLOR_BLUE_DRK,
		TI_COLOR_BLUE_LIT,
		TI_COLOR_RED_DRK,
		TI_COLOR_CYAN,
		TI_COLOR_RED_MED,
		TI_COLOR_RED_LIT,
		TI_COLOR_YELLOW_DRK,
		TI_COLOR_YELLOW_LIT,
		TI_COLOR_GREEN_DRK,
		TI_COLOR_MAGENTA,
		TI_COLOR_GREY,
		TI_COLOR_WHITE
	};

	public static final String[] TI_PALETTE_NAMES =
	{
		"Transparent",
		"Black",
		"Medium Green",
		"Light Green",
		"Dark Blue",
		"Light Blue",
		"Dark Red",
		"Cyan",
		"Medium Red",
		"Light Red",
		"Dark Yellow",
		"Light Yellow",
		"Dark Green",
		"Magenta",
		"Grey",
		"White"
	};

	public static final Integer[] TI_PALETTE_SELECT_VALUES =
	{
		0,
		1,
		2,
		3,
		4,
		5,
		6,
		7,
		8,
		9,
		10,
		11,
		12,
		13,
		14,
		15
	};

	public static final char[] CHARMAP =
	{
		' ', '!', '"', '#', '$', '%', '&', '\'',
		'(', ')', '*', '+', ',', '-', '.', '/',
		'0', '1', '2', '3', '4', '5', '6', '7',
		'8', '9', ':', ';', '<', '=', '>', '?',
		'@', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
		'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
		'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
		'X', 'Y', 'Z', '[', '\\', ']', '^', '_',
		'`', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
		'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
		'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
		'x', 'y', 'z', '{', '|', '}', '~', (char)127
	};

	public static final int MINCHAR = 0;
	public static final int MAXCHAR = 255;
	public static final int CHARMAPSTART = 32;
	public static final int CHARMAPEND = 127;
	public static final int FIRSTCHAR = 32;
	public static final int LASTCHAR = FIRSTCHAR + (8 * 16) - 1;
	public static final int SPACECHAR = 32;
	public static final int CUSTOMCHAR = FIRSTCHAR + (8 * 12);
	public static final int FINALXBCHAR = FIRSTCHAR + (8 * 14) - 1;
}
