package com.dreamcodex.ti.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.ImageFilter;
import java.awt.image.RGBImageFilter;
import javax.swing.BorderFactory;
import javax.swing.border.Border;

public class Globals
{
	public static final String CMD_NEW      = "new";
	public static final String CMD_OPEN     = "open";
	public static final String CMD_SAVE     = "save";
	public static final String CMD_SAVEAS   = "saveas";
	public static final String CMD_APPEND   = "append";
	public static final String CMD_EXIT     = "exit";
	public static final String CMD_MPIMGMN  = "importimagemono";
	public static final String CMD_MPIMGCL  = "importimagecolor";
	public static final String CMD_MPCIMGMN = "importcharimagemono";
	public static final String CMD_MPCIMGCL = "importcharimagecolor";
	public static final String CMD_MPVDP    = "importvramdump";
	public static final String CMD_XPDATA   = "exportdata";
	public static final String CMD_XPEXEC   = "exportexec";
	public static final String CMD_XPASM    = "exportasm";
	public static final String CMD_XPBIN    = "exportbin";
	public static final String CMD_XPCIMGMN = "exportcharimagemono";
	public static final String CMD_XPCIMGCL = "exportcharimagecolor";
	public static final String CMD_XPMAPIMG = "exportmapimage";
	public static final String CMD_GRID     = "togglegrid";
	public static final String CMD_LOOK     = "look";
	public static final String CMD_CLONE    = "clone";
	public static final String CMD_FILL     = "fill";
	public static final String CMD_CLEAR    = "clear";
	public static final String CMD_ROTATEL  = "rotatel";
	public static final String CMD_ROTATER  = "rotater";
	public static final String CMD_FLIPH    = "fliph";
	public static final String CMD_FLIPV    = "flipv";
	public static final String CMD_SHIFTU   = "shiftu";
	public static final String CMD_SHIFTD   = "shiftd";
	public static final String CMD_SHIFTL   = "shiftl";
	public static final String CMD_SHIFTR   = "shiftr";
	public static final String CMD_INVERT   = "invert";
	public static final String CMD_EDITCHR  = "editchar";
	public static final String CMD_UPDATE   = "updategraphic";
	public static final String CMD_UPDCHAR  = "updatechar";
	public static final String CMD_UPDSPR   = "updatesprite";
	public static final String CMD_CLRFORE  = "clrfore";
	public static final String CMD_CLRBACK  = "clrback";
	public static final String CMD_ADDMAP   = "addmap";
	public static final String CMD_DELMAP   = "delmap";
	public static final String CMD_PREVMAP  = "prevmap";
	public static final String CMD_NEXTMAP  = "nextmap";
	public static final String CMD_BACKMAP  = "backmap";
	public static final String CMD_FORWMAP  = "forwmap";
	public static final String CMD_TCURSOR  = "textcursor";
	public static final String CMD_SHOWPOS  = "toggleshowpos";
    public static final String CMD_BASEPOS  = "baseposition";

	public static final String KEY_SCRBACK  = "SB:"; // phasing out
	public static final String KEY_COLORSET = "CC:";
	public static final String KEY_CHARDATA = "CH:";
	public static final String KEY_CHARRANG = "CR:"; // needed temporarily until all mag files are new format
	public static final String KEY_MAPCOUNT = "MC:";
	public static final String KEY_MAPSTART = "M+";
	public static final String KEY_MAPSIZE  = "MS:";
	public static final String KEY_MAPBACK  = "MB:";
	public static final String KEY_MAPDATA  = "MP:";
	public static final String KEY_MAPEND   = "M-";

	public static final Color CLR_COMPONENTBACK   = new Color(255, 255, 255);
	public static final Color CLR_BUTTON_NORMAL   = new Color(255, 255, 255);
	public static final Color CLR_BUTTON_ACTIVE   = new Color(232, 212, 255);
	public static final Color CLR_BUTTON_INACTIVE = new Color(96, 96, 96);
	public static final Color CLR_BUTTON_TRANS    = new Color(232, 232, 192);
	public static final Color CLR_BUTTON_SHIFT    = new Color(192, 255, 232);

	public static final int INDEX_CLR_BACK = 0;
	public static final int INDEX_CLR_FORE = 1;

	public static final int INDEX_SPR_UL = 0;
	public static final int INDEX_SPR_UR = 1;
	public static final int INDEX_SPR_LL = 2;
	public static final int INDEX_SPR_LR = 3;

	public static final int MASK_HIBITS = 240;
	public static final int MASK_LOBITS = 15;

	public static Dimension DM_TOOL = new Dimension(24, 24);
	public static Dimension DM_TEXT = new Dimension(48, 24);

	public static Border bordButtonNormal = BorderFactory.createLineBorder(new Color(164, 164, 164), 2);
	public static Border bordButtonActive = BorderFactory.createLineBorder(new Color(255, 164, 164), 2);

	public static final String BLANKCHAR = "0000000000000000";

	public static String getByteString(int[][] binarray)
	{
		StringBuffer sbBytes = new StringBuffer();
		int bytepos = 0;
		int byteval = 0;
		int charnum = 0;
		int charrow = (binarray.length / 8);
		int charcol = (binarray[0].length / 8);
		for(int cc = 0; cc < charcol; cc++)
		{
			for(int cr = 0; cr < charrow; cr++)
			{
				for(int y = 0; y < 8; y++)
				{
					int cy = y + (cr * 8);
					bytepos = 0;
					for(int x = 0; x < 8; x++)
					{
						int cx = x + (cc * 8);
						if(binarray[cy][cx] > 0) { byteval += (bytepos == 0 ? 8 : (bytepos == 1 ? 4 : (bytepos == 2 ? 2 : 1))); }
						bytepos++;
						if(bytepos > 3)
						{
							sbBytes.append(Integer.toHexString(byteval));
							byteval = 0;
							bytepos = 0;
						}
					}
					if(bytepos != 0)
					{
						sbBytes.append(Integer.toHexString(byteval));
						byteval = 0;
						bytepos = 0;
					}
				}
			}
		}
		return sbBytes.toString();
	}

	public static int[][] getByteIntArray(String sbtyes, int rows)
	{
		int charcol = (sbtyes.length() / rows) * 4;
		int[][] barray = new int[rows][charcol];
		int charpos = 0;
		int rowpos = 0;
		int colpos = 0;
		for(int cc = 0; cc < sbtyes.length(); cc++)
		{
			String ch = "" + sbtyes.charAt(cc);
			int chi = Integer.parseInt(ch, 16);
			barray[rowpos][charpos+colpos] = (((chi & 8) == 8) ? 1 : 0); charpos++;
			barray[rowpos][charpos+colpos] = (((chi & 4) == 4) ? 1 : 0); charpos++;
			barray[rowpos][charpos+colpos] = (((chi & 2) == 2) ? 1 : 0); charpos++;
			barray[rowpos][charpos+colpos] = (((chi & 1) == 1) ? 1 : 0); charpos++;
			if(charpos >= 8)
			{
				charpos = 0;
				rowpos++;
				if(rowpos >= rows)
				{
					rowpos = 0;
					colpos += 8;
				}
			}
		}
		return barray;
	}

	public static int[][] flipGrid(int[][] grid, boolean isVertical)
	{
		int height = grid.length;
		int width  = grid[0].length;
		int flipx = 0;
		int flipy = 0;
		int[][] flippedGrid = new int[width][height];
		for(int y = 0; y < height; y++)
		{
			for(int x = 0; x < width; x++)
			{
				if(isVertical)
				{
					flipx = x;
					flipy = (height - 1) - y;
				}
				else
				{
					flipx = (width - 1) - x;
					flipy = y;
				}
				flippedGrid[flipy][flipx] = grid[y][x];
			}
		}
		grid = flippedGrid;
		return grid;
	}

	public static int[][] rotateGrid(int[][] grid, boolean isLeft)
	{
		int height = grid.length;
		int width  = grid[0].length;
		int rotx = 0;
		int roty = 0;
		int[][] rotatedGrid = new int[width][height];
		for(int y = 0; y < height; y++)
		{
			for(int x = 0; x < width; x++)
			{
				if(isLeft)
				{
					rotx = y;
					roty = (height - 1) - x;
				}
				else
				{
					rotx = (width - 1) - y;
					roty = x;
				}
				rotatedGrid[roty][rotx] = grid[y][x];
			}
		}
		grid = rotatedGrid;
		return grid;
	}

	public static int[][] invertGrid(int[][] grid)
	{
		int height = grid.length;
		int width  = grid[0].length;
		for(int y = 0; y < height; y++)
		{
			for(int x = 0; x < width; x++)
			{
				grid[y][x] = (grid[y][x] == 0 ? 1 : 0);
			}
		}
		return grid;
	}

	public static int[][] shiftGrid(int[][] grid, int xshift, int yshift)
	{
		int y = (yshift < 0 ? grid.length - 1 : 0);
		while(y >= 0 && y <= (grid.length - 1))
		{
			int getY = y + yshift;
			int x = (xshift < 0 ? grid[y].length - 1 : 0);
			while(x >= 0 && x <= (grid[y].length - 1))
			{
				int getX = x + xshift;
				if(getY >= 0 && getY < grid.length && getX >= 0 && getX < grid[y].length)
				{
					grid[y][x] = grid[getY][getX];
				}
				else
				{
					grid[y][x] = 0;
				}
				x = x + (xshift < 0 ? -1 : 1);
			}
			y = y + (yshift < 0 ? -1 : 1);
		}
		return grid;
	}

	public static ImageFilter ifTrans = new RGBImageFilter()
	{
		public int tagTrans = TIGlobals.TI_COLOR_TRANSOPAQUE.getRGB();
		public final int filterRGB(int x, int y, int rgb)
		{
			if(rgb == tagTrans)
			{
				return 0x00000000;
			}
			return rgb;
		}
	};

}