package com.dreamcodex.ti;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.net.URL;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

import com.dreamcodex.ti.component.GridCanvas;
import com.dreamcodex.ti.component.ImagePreview;
import com.dreamcodex.ti.util.Globals;
import com.dreamcodex.ti.util.TIGlobals;
import com.dreamcodex.ti.util.MutableFilter;

/** Charcot
  * TI-99/4A graphical character editor
  *
  * @author Howard Kistler
  */

public class Charcot extends JFrame implements WindowListener, KeyListener, ActionListener, MouseListener, MouseMotionListener
{
// Local Constants -------------------------------------------------------------------------/

	private final String APPTITLE = "Charcot : TI-99/4A Character & Sprite Editor";
	private final String FILEEXT = "mag";
	private final String[] FILEEXTS = { FILEEXT };
	private final String XBEXT = "xb";
	private final String[] XBEXTS = { XBEXT, "bas", "txt" };
	private final String ASMEXT = "asm";
	private final String[] ASMEXTS = { ASMEXT };
	private final String IMGEXT = "png";
	private final String[] IMGEXTS = { IMGEXT };

// Components ------------------------------------------------------------------------------/

	private JTabbedPane jtabEditors;

	private GridCanvas gcChar;
	private JTextField jtxtChar;
	private JButton jbtnUpdateChar;

	private GridCanvas gcSprite;
	private JTextField jtxtSprite;
	private JButton jbtnUpdateSprite;

	private JButton[] jbtnChar;
	private JPanel jpnlFont;

	private JLabel jlblCharChar;
	private JLabel jlblCharInt;
	private JLabel jlblCharHex;

	private JTextField jtxtSprChUL;
	private JTextField jtxtSprChUR;
	private JTextField jtxtSprChLL;
	private JTextField jtxtSprChLR;
	private JButton jbtnSprLookUL;
	private JButton jbtnSprLookUR;
	private JButton jbtnSprLookLL;
	private JButton jbtnSprLookLR;

// Variables -------------------------------------------------------------------------------/

	private final int FONT_ROWS = 32;
	private final int FONT_COLS = 8;

	protected HashMap<String,int[][]> hmCharGrids;
	protected HashMap<String,Image> hmCharImages;

	protected int[][] clrSets = new int[FONT_ROWS][2];

	protected int activeChar = TIGlobals.CUSTOMCHAR;
	protected int lastActiveChar = -1;

	protected int[] sprChars = new int[4];

	protected File srcDataFile;

// Constructors ----------------------------------------------------------------------------/

	public Charcot()
	{
		super("Charcot : TI-99/4A Character & Sprite Editor");

		sprChars[Globals.INDEX_SPR_UL] = TIGlobals.CUSTOMCHAR;
		sprChars[Globals.INDEX_SPR_UR] = TIGlobals.CUSTOMCHAR + 1;
		sprChars[Globals.INDEX_SPR_LL] = TIGlobals.CUSTOMCHAR + 2;
		sprChars[Globals.INDEX_SPR_LR] = TIGlobals.CUSTOMCHAR + 3;

		// Create the menus
		JMenuBar jMenuBar = new JMenuBar();
		JMenu jmenFile = new JMenu("File");
		JMenuItem jmitNew    = new JMenuItem("New Project"); jmitNew.setActionCommand(Globals.CMD_NEW); jmitNew.addActionListener(this); jmenFile.add(jmitNew);
		JMenuItem jmitOpen   = new JMenuItem("Open Project"); jmitOpen.setActionCommand(Globals.CMD_OPEN); jmitOpen.addActionListener(this); jmenFile.add(jmitOpen);
		JMenuItem jmitSave   = new JMenuItem("Save Project"); jmitSave.setActionCommand(Globals.CMD_SAVE); jmitSave.addActionListener(this); jmenFile.add(jmitSave);
		jmenFile.addSeparator();
		JMenuItem jmitExit = new JMenuItem("Exit"); jmitExit.setActionCommand(Globals.CMD_EXIT); jmitExit.addActionListener(this); jmenFile.add(jmitExit);
		jMenuBar.add(jmenFile);

		JMenu jmenImport = new JMenu("Import");
		JMenuItem jmitImportChrImgMono  = new JMenuItem("Character Image"); jmitImportChrImgMono.setActionCommand(Globals.CMD_MPCIMGMN); jmitImportChrImgMono.addActionListener(this); jmenImport.add(jmitImportChrImgMono);
		jMenuBar.add(jmenImport);

		JMenu jmenExport = new JMenu("Export");
		JMenuItem jmitExportData = new JMenuItem("BASIC Data"); jmitExportData.setActionCommand(Globals.CMD_XPDATA); jmitExportData.addActionListener(this); jmenExport.add(jmitExportData);
		JMenuItem jmitExportExec = new JMenuItem("XB Program"); jmitExportExec.setActionCommand(Globals.CMD_XPEXEC); jmitExportExec.addActionListener(this); jmenExport.add(jmitExportExec);
		JMenuItem jmitExportAsm  = new JMenuItem("Assembler Data"); jmitExportAsm.setActionCommand(Globals.CMD_XPASM); jmitExportAsm.addActionListener(this); jmenExport.add(jmitExportAsm);
		jmenExport.addSeparator();
		JMenuItem jmitExportChrImgMono  = new JMenuItem("Character Image (Mono)"); jmitExportChrImgMono.setActionCommand(Globals.CMD_XPCIMGMN); jmitExportChrImgMono.addActionListener(this); jmenExport.add(jmitExportChrImgMono);
		JMenuItem jmitExportChrImgColor = new JMenuItem("Character Image (Color)"); jmitExportChrImgColor.setActionCommand(Globals.CMD_XPCIMGCL); jmitExportChrImgColor.addActionListener(this); jmenExport.add(jmitExportChrImgColor);
		jMenuBar.add(jmenExport);

		// Create the toolkit
		JToolBar jToolkitBar = new JToolBar(JToolBar.VERTICAL);
		jToolkitBar.setFloatable(false);
		jToolkitBar.setOpaque(true);
		jToolkitBar.setBackground(Globals.CLR_COMPONENTBACK);
		jToolkitBar.add(getToolButton(Globals.CMD_FLIPH, "Flip Horizontal"));
		jToolkitBar.add(getToolButton(Globals.CMD_FLIPV, "Flip Vertical"));
		jToolkitBar.add(getToolButton(Globals.CMD_ROTATEL, "Rotate Left"));
		jToolkitBar.add(getToolButton(Globals.CMD_ROTATER, "Rotate Right"));
		jToolkitBar.add(new JToolBar.Separator());
		jToolkitBar.add(getToolButton(Globals.CMD_CLEAR, "Clear Canvas"));
		jToolkitBar.add(getToolButton(Globals.CMD_FILL, "Fill Canvas"));
		jToolkitBar.add(getToolButton(Globals.CMD_INVERT, "Invert Image"));
		jToolkitBar.add(new JToolBar.Separator());
		jToolkitBar.add(getToolButton(Globals.CMD_GRID, "Toggle Grid"));

		// Create character editor panel
		JPanel jpnlChar = getPanel(new BorderLayout());
		gcChar = new GridCanvas(8, 8, 8, (MouseListener)this, (MouseMotionListener)this);
		jpnlChar.add(gcChar, BorderLayout.CENTER);
		JPanel jpnlCharTool = getPanel(new BorderLayout());
		jtxtChar = getTextField("");
		jpnlCharTool.add(jtxtChar, BorderLayout.CENTER);
		jbtnUpdateChar = new JButton("Set Char"); jbtnUpdateChar.setActionCommand(Globals.CMD_UPDCHAR); jbtnUpdateChar.addActionListener(this); jpnlCharTool.add(jbtnUpdateChar, BorderLayout.EAST);
		jpnlChar.add(jpnlCharTool, BorderLayout.SOUTH);
		JPanel jpnlCharLabels = getPanel(new FlowLayout());
		jpnlCharLabels.add(getLabel("Character:", JLabel.RIGHT));
		jlblCharChar = getLabel("", JLabel.LEFT); jlblCharChar.setPreferredSize(Globals.DM_TOOL); jpnlCharLabels.add(jlblCharChar);
		jpnlCharLabels.add(getLabel("Decimal:", JLabel.RIGHT));
		jlblCharInt = getLabel("", JLabel.LEFT); jlblCharInt.setPreferredSize(Globals.DM_TOOL); jpnlCharLabels.add(jlblCharInt);
		jpnlCharLabels.add(getLabel("Hexadecimal:", JLabel.RIGHT));
		jlblCharHex = getLabel("", JLabel.LEFT); jlblCharHex.setPreferredSize(Globals.DM_TOOL); jpnlCharLabels.add(jlblCharHex);
		jpnlChar.add(jpnlCharLabels, BorderLayout.NORTH);

		// Create sprite editor panel
		JPanel jpnlSprite = getPanel(new BorderLayout());
		gcSprite = new GridCanvas(16, 16, 8, (MouseListener)this, (MouseMotionListener)this);
		gcSprite.setGridGroup(8);
		jpnlSprite.add(gcSprite, BorderLayout.CENTER);
		JPanel jpnlSpriteTool = getPanel(new BorderLayout());
		jtxtSprite = getTextField("");
		jpnlSpriteTool.add(jtxtSprite, BorderLayout.CENTER);
		jbtnUpdateSprite = new JButton("Set Sprite"); jbtnUpdateSprite.setActionCommand(Globals.CMD_UPDSPR); jbtnUpdateSprite.addActionListener(this); jpnlSpriteTool.add(jbtnUpdateSprite, BorderLayout.EAST);
		jpnlSprite.add(jpnlSpriteTool, BorderLayout.SOUTH);
		JPanel jpnlSpriteChars = getPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(0, 0, 8, 8);
		gbc.gridy = 1;
		jpnlSpriteChars.add(getLabel("Upper Left:", JLabel.RIGHT), gbc);
		jtxtSprChUL = getTextField("" + sprChars[Globals.INDEX_SPR_UL]); jpnlSpriteChars.add(jtxtSprChUL, gbc);
		jbtnSprLookUL = getToolButton(Globals.CMD_LOOK, "Pick Upper Left Character"); jpnlSpriteChars.add(jbtnSprLookUL, gbc);
		jpnlSpriteChars.add(getLabel("Upper Right:", JLabel.RIGHT), gbc);
		jtxtSprChUR = getTextField("" + sprChars[Globals.INDEX_SPR_UR]); jpnlSpriteChars.add(jtxtSprChUR, gbc);
		jbtnSprLookUR = getToolButton(Globals.CMD_LOOK, "Pick Upper Right Character"); jpnlSpriteChars.add(jbtnSprLookUR, gbc);
		gbc.gridy = 2;
		jpnlSpriteChars.add(getLabel("Lower Left:", JLabel.RIGHT), gbc);
		jtxtSprChLL = getTextField("" + sprChars[Globals.INDEX_SPR_LL]); jpnlSpriteChars.add(jtxtSprChLL, gbc);
		jbtnSprLookLL = getToolButton(Globals.CMD_LOOK, "Pick Lower Left Character"); jpnlSpriteChars.add(jbtnSprLookLL, gbc);
		jpnlSpriteChars.add(getLabel("Lower Right:", JLabel.RIGHT), gbc);
		jtxtSprChLR = getTextField("" + sprChars[Globals.INDEX_SPR_LR]); jpnlSpriteChars.add(jtxtSprChLR, gbc);
		jbtnSprLookLR = getToolButton(Globals.CMD_LOOK, "Pick Lower Right Character"); jpnlSpriteChars.add(jbtnSprLookLR, gbc);
		JPanel jpnlWrap = getPanel(new FlowLayout());
		jpnlWrap.add(jpnlSpriteChars);
		jpnlSprite.add(jpnlWrap, BorderLayout.NORTH);

		// Create Character Dock
		int charNum  = 0;
		hmCharGrids = new HashMap<String,int[][]>();
		hmCharImages = new HashMap<String,Image>();
		jpnlFont = getPanel(new GridLayout(FONT_ROWS, FONT_COLS + 1));
		jbtnChar = new JButton[FONT_ROWS*FONT_COLS];
		for(int r = 0; r < FONT_ROWS; r++)
		{
			String sLabel = "";
			if((r * FONT_COLS) < TIGlobals.FIRSTCHAR)
			{
				JLabel jlblRow = getLabel("L" + (r + 1) + " ", JLabel.RIGHT);
				jlblRow.setBackground(new Color(196, 212, 255));
				jpnlFont.add(jlblRow);
			}
			else if((r * FONT_COLS) > TIGlobals.LASTCHAR)
			{
				JLabel jlblRow = getLabel("U" + (r - 19) + " ", JLabel.RIGHT);
				jlblRow.setBackground(new Color(255, 196, 232));
				jpnlFont.add(jlblRow);
			}
			else
			{
				JLabel jlblRow = getLabel((r - 3) + " ", JLabel.RIGHT);
				jlblRow.setBackground(new Color(255, 232, 196));
				jpnlFont.add(jlblRow);
			}
			clrSets[r][Globals.INDEX_CLR_BACK] = 0;
			clrSets[r][Globals.INDEX_CLR_FORE] = 1;
			for(int c = 0; c < FONT_COLS; c++)
			{
				charNum = (r * FONT_COLS) + c;
				jbtnChar[charNum] = getDockButton(((charNum >= TIGlobals.CHARMAPSTART) && (charNum <= TIGlobals.CHARMAPEND) ? "" + TIGlobals.CHARMAP[charNum - TIGlobals.CHARMAPSTART] : "?"), Globals.CMD_EDITCHR + charNum, TIGlobals.TI_PALETTE_OPAQUE[clrSets[r][Globals.INDEX_CLR_BACK]]);
				jbtnChar[charNum].setForeground(TIGlobals.TI_COLOR_UNUSED);
				jpnlFont.add(jbtnChar[charNum]);
				int[][] emtpyGrid = new int[8][8];
				for(int y = 0; y < emtpyGrid.length; y++) { for(int x = 0; x < emtpyGrid[y].length; x++) { emtpyGrid[y][x] = 0; } }
				hmCharGrids.put(getCharKey(charNum), emtpyGrid);
			}
		}

		// Assemble tabbed interface
		jtabEditors = new JTabbedPane();
		jtabEditors.add("Character", jpnlChar);
		jtabEditors.add("Sprite", jpnlSprite);

		// Assemble the application
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(jtabEditors, BorderLayout.CENTER);
		this.getContentPane().add(jMenuBar, BorderLayout.NORTH);
		this.getContentPane().add(jToolkitBar, BorderLayout.WEST);
		this.getContentPane().add(jpnlFont, BorderLayout.EAST);
		this.addKeyListener(this);
		this.addWindowListener(this);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.pack();
		Insets insets = this.getInsets();
		this.setSize(600, 600);
		this.setVisible(true);

		// Initialise the border objects for button selection
		Globals.bordButtonNormal = jbtnChar[0].getBorder();
	}

// Component Builder Methods ---------------------------------------------------------------/

	protected JPanel getPanel(LayoutManager layout)
	{
		JPanel jpnlRtn = new JPanel(layout);
		jpnlRtn.setOpaque(true);
		jpnlRtn.setBackground(Globals.CLR_COMPONENTBACK);
		return jpnlRtn;
	}

	protected ImageIcon getIcon(String name)
	{
		URL imageURL = getClass().getResource("images/icon_" + name + "_mono.png");
		return new ImageIcon(Toolkit.getDefaultToolkit().getImage(imageURL));
	}

	protected JButton getToolButton(String buttonkey, String tooltip)
	{
		JButton jbtnTool = new JButton(getIcon(buttonkey));
		jbtnTool.setActionCommand(buttonkey);
		jbtnTool.addActionListener(this);
		jbtnTool.setToolTipText(tooltip);
		jbtnTool.setMargin(new Insets(1, 1, 1, 1));
		jbtnTool.setBackground(Globals.CLR_COMPONENTBACK);
		jbtnTool.setPreferredSize(Globals.DM_TOOL);
		return jbtnTool;
	}

	protected JButton getDockButton(String buttonlabel, String actcmd, Color bgcolor)
	{
		JButton jbtnDock = new JButton(buttonlabel);
		jbtnDock.setActionCommand(actcmd);
		jbtnDock.addActionListener(this);
		jbtnDock.setOpaque(true);
		jbtnDock.setBackground(bgcolor);
		jbtnDock.setMargin(new Insets(0, 0, 0, 0));
		jbtnDock.setPreferredSize(Globals.DM_TOOL);
		return jbtnDock;
	}

	protected JLabel getLabel(String text, int align)
	{
		JLabel jlblRtn = new JLabel(text, align);
		jlblRtn.setOpaque(true);
		jlblRtn.setBackground(Globals.CLR_COMPONENTBACK);
		return jlblRtn;
	}

	protected JTextField getTextField(String text)
	{
		JTextField jtxtRtn = new JTextField(text);
		jtxtRtn.addActionListener(this);
		jtxtRtn.setPreferredSize(Globals.DM_TOOL);
		jtxtRtn.setBorder(BorderFactory.createLineBorder(new Color(64, 64, 64), 1));
		return jtxtRtn;
	}

// Listeners -------------------------------------------------------------------------------/

	/* KeyListener methods */
	public void keyPressed(KeyEvent ke)
	{
		if(ke.getKeyCode() == KeyEvent.VK_ESCAPE)
		{
			exitApp();
		}
	}
	public void keyReleased(KeyEvent ke) { ; }
	public void keyTyped(KeyEvent ke)    { ; }

	/* ActionListener methods */
	public void actionPerformed(ActionEvent ae)
	{
		try
		{
			String command = ae.getActionCommand();
			if(command.equals(Globals.CMD_EXIT))
			{
				exitApp();
			}
			else if(command.equals(Globals.CMD_GRID))
			{
				if(gcChar.isShowing())
				{
					gcChar.toggleGrid();
				}
				else if(gcSprite.isShowing())
				{
					gcSprite.toggleGrid();
				}
			}
			else if(command.equals(Globals.CMD_CLEAR))
			{
				if(gcChar.isShowing())
				{
					gcChar.clearGrid();
				}
				else if(gcSprite.isShowing())
				{
					gcSprite.clearGrid();
				}
			}
			else if(command.equals(Globals.CMD_FILL))
			{
				if(gcChar.isShowing())
				{
					gcChar.fillGrid();
				}
				else if(gcSprite.isShowing())
				{
					gcSprite.fillGrid();
				}
			}
			else if(command.equals(Globals.CMD_INVERT))
			{
				if(gcChar.isShowing())
				{
					gcChar.setGrid(Globals.invertGrid(gcChar.getGridData()));
				}
				else if(gcSprite.isShowing())
				{
					gcSprite.setGrid(Globals.invertGrid(gcSprite.getGridData()));
				}
			}
			else if(command.equals(Globals.CMD_FLIPH))
			{
				if(gcChar.isShowing())
				{
					gcChar.setGrid(Globals.flipGrid(gcChar.getGridData(),false));
				}
				else if(gcSprite.isShowing())
				{
					gcSprite.setGrid(Globals.flipGrid(gcSprite.getGridData(),false));
				}
			}
			else if(command.equals(Globals.CMD_FLIPV))
			{
				if(gcChar.isShowing())
				{
					gcChar.setGrid(Globals.flipGrid(gcChar.getGridData(),true));
				}
				else if(gcSprite.isShowing())
				{
					gcSprite.setGrid(Globals.flipGrid(gcSprite.getGridData(),true));
				}
			}
			else if(command.equals(Globals.CMD_ROTATEL))
			{
				if(gcChar.isShowing())
				{
					gcChar.setGrid(Globals.rotateGrid(gcChar.getGridData(), true));
				}
				else if(gcSprite.isShowing())
				{
					gcSprite.setGrid(Globals.rotateGrid(gcSprite.getGridData(), true));
				}
			}
			else if(command.equals(Globals.CMD_ROTATER))
			{
				if(gcChar.isShowing())
				{
					gcChar.setGrid(Globals.rotateGrid(gcChar.getGridData(), false));
				}
				else if(gcSprite.isShowing())
				{
					gcSprite.setGrid(Globals.rotateGrid(gcSprite.getGridData(), false));
				}
			}
			else if(command.startsWith(Globals.CMD_EDITCHR))
			{
				activeChar = Integer.parseInt(command.substring(Globals.CMD_EDITCHR.length()));
				gcChar.setGridData(hmCharGrids.get(getCharKey(activeChar)));
				int cset = (int)(Math.floor(activeChar / 8));
				gcChar.setColorBack(TIGlobals.TI_PALETTE_OPAQUE[clrSets[cset][Globals.INDEX_CLR_BACK]]);
				gcChar.setColorDraw(TIGlobals.TI_PALETTE_OPAQUE[clrSets[cset][Globals.INDEX_CLR_FORE]]);
				gcChar.redrawCanvas();
				updateComponents();
			}
			else if(command.equals(Globals.CMD_UPDCHAR))
			{
				gcChar.setGrid(Globals.getByteIntArray(jtxtChar.getText(), 8));
			}
			else if(command.equals(Globals.CMD_UPDSPR))
			{
				gcSprite.setGrid(Globals.getByteIntArray(jtxtSprite.getText(), 16));
			}
			else if(command.equals(Globals.CMD_NEW))
			{
				int userResponse = confirmationAction(this, "Confirm New Project", "This will delete all current data.\n\rAre you sure?");
				if(userResponse == JOptionPane.YES_OPTION)
				{
					newProject();
				}
			}
			else if(command.equals(Globals.CMD_OPEN))
			{
				openDataFile();
			}
		}
		catch(Exception e) { ; }
	}

	/* WindowListener methods */
	public void windowClosing(WindowEvent we)
	{
		exitApp();
	}
	public void windowOpened(WindowEvent we)      { ; }
	public void windowClosed(WindowEvent we)      { ; }
	public void windowActivated(WindowEvent we)   { ; }
	public void windowDeactivated(WindowEvent we) { ; }
	public void windowIconified(WindowEvent we)   { ; }
	public void windowDeiconified(WindowEvent we) { ; }

	/* MouseListener methods */
	public void mousePressed(MouseEvent me) { updateComponents(); }
	public void mouseReleased(MouseEvent me) {}
	public void mouseClicked(MouseEvent me) {}
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}

	/* MouseMotionListener methods */
	public void mouseMoved(MouseEvent me) {}
	public void mouseDragged(MouseEvent me) { updateComponents(); }

// File Handling Methods -------------------------------------------------------------------/

	protected void newProject()
	{
		int cellNum = 0;
		int charNum = TIGlobals.MINCHAR;
		for(int r = 0; r < FONT_ROWS; r++)
		{
			clrSets[r][Globals.INDEX_CLR_BACK] = 0;
			clrSets[r][Globals.INDEX_CLR_FORE] = 1;
			for(int c = 0; c < FONT_COLS; c++)
			{
				cellNum = (r * FONT_COLS) + c;
				jbtnChar[cellNum].setBackground(TIGlobals.TI_PALETTE_OPAQUE[clrSets[r][Globals.INDEX_CLR_BACK]]);
				jbtnChar[cellNum].setForeground(TIGlobals.TI_COLOR_UNUSED);
				gcChar.clearGrid();
				hmCharGrids.put(getCharKey(charNum), gcChar.getGridData());
				updateFontButton(charNum);
				charNum++;
			}
		}
		srcDataFile = (File)null;
		updateComponents();
	}

	protected void openDataFile()
	{
		File whatFile = getFileFromChooser((srcDataFile != null ? srcDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, FILEEXTS, "Map Data Files");
		if(whatFile != null)
		{
			newProject();
			srcDataFile = whatFile;
			this.setTitle(APPTITLE + " : " + srcDataFile.getName());
			readDataFile(srcDataFile);
		}
		for(int cn = 0; cn < jbtnChar.length; cn++)
		{
			jbtnChar[cn].setIcon((ImageIcon)null);
			jbtnChar[cn].setText((((cn - TIGlobals.CHARMAPSTART) >= 0 && (cn - TIGlobals.CHARMAPSTART) < TIGlobals.CHARMAP.length) ? "" + TIGlobals.CHARMAP[cn - TIGlobals.CHARMAPSTART] : "?"));
		}
		for(int i = TIGlobals.MINCHAR; i <= TIGlobals.MAXCHAR; i++)
		{
			updateFontButton(i);
		}
		updateComponents();
	}

	private File getFileFromChooser(String startDir, int dialogType, String[] exts, String desc, boolean hasImagePreview)
	{
		JFileChooser jfileDialog = new JFileChooser(startDir);
		jfileDialog.setDialogType(dialogType);
		jfileDialog.setFileFilter(new MutableFilter(exts, desc));
		if(hasImagePreview)
		{
			jfileDialog.setAccessory(new ImagePreview(jfileDialog));
		}
		int optionSelected = JFileChooser.CANCEL_OPTION;
		if(dialogType == JFileChooser.OPEN_DIALOG)
		{
			optionSelected = jfileDialog.showOpenDialog(this);
		}
		else if(dialogType == JFileChooser.SAVE_DIALOG)
		{
			optionSelected = jfileDialog.showSaveDialog(this);
		}
		else // default to an OPEN_DIALOG
		{
			optionSelected = jfileDialog.showOpenDialog(this);
		}
		if(optionSelected == JFileChooser.APPROVE_OPTION)
		{
			return jfileDialog.getSelectedFile();
		}
		return (File)null;
	}

	private File getFileFromChooser(String startDir, int dialogType, String[] exts, String desc)
	{
		return getFileFromChooser(startDir, dialogType, exts, desc, false);
	}

	protected void readDataFile(File mapDataFile)
	{
		hmCharGrids.clear();
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(mapDataFile));
			String lineIn = "";
			int charStart = TIGlobals.MINCHAR;
			int charEnd = TIGlobals.MAXCHAR;
			int charRead = charStart;
			int cset = (int)(Math.floor(charStart / 8));
			do
			{
				lineIn = br.readLine();
				if(lineIn == null)
				{
					break;
				}
				else
				{
					if(lineIn.startsWith(Globals.KEY_COLORSET))
					{
						lineIn = lineIn.substring(Globals.KEY_COLORSET.length());
						clrSets[cset][Globals.INDEX_CLR_FORE] = Integer.parseInt(lineIn.substring(0, lineIn.indexOf("|")));
						clrSets[cset][Globals.INDEX_CLR_BACK] = Integer.parseInt(lineIn.substring(lineIn.indexOf("|") + 1));
						cset++;
					}
					else if(lineIn.startsWith(Globals.KEY_CHARDATA))
					{
						lineIn = lineIn.substring(Globals.KEY_CHARDATA.length());
						hmCharGrids.put(getCharKey(charRead), Globals.getByteIntArray(lineIn, 8));
						charRead++;
					}
					else if(lineIn.startsWith(Globals.KEY_CHARRANG))
					{
						lineIn = lineIn.substring(Globals.KEY_CHARRANG.length());
						charStart = Integer.parseInt(lineIn.substring(0, lineIn.indexOf("|")));
						charEnd = Integer.parseInt(lineIn.substring(lineIn.indexOf("|") + 1));
						charRead = charStart;
						cset = (int)(Math.floor(charStart / 8));
					}
				}
			} while (lineIn != null);
			br.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	protected int confirmationAction(Frame parent, String title, String message) 
	{
		return JOptionPane.showConfirmDialog(parent, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
	}

// Convenience Methods ---------------------------------------------------------------------/

	protected String getCharKey(int charnum)
	{
		return ("ch" + charnum);
	}

	protected void updateComponents()
	{
		updateFontButton(activeChar);
		if(gcChar.isShowing())
		{
			jtxtChar.setText(Globals.getByteString(gcChar.getGridData()).toUpperCase());
		}
		else if(gcSprite.isShowing())
		{
			jtxtSprite.setText(Globals.getByteString(gcSprite.getGridData()).toUpperCase());
		}
	}

	protected void updateFontButton(int charnum)
	{
		if(lastActiveChar != charnum && lastActiveChar != -1)
		{
			jbtnChar[lastActiveChar].setBorder(Globals.bordButtonNormal);
		}
		lastActiveChar = charnum;
		jbtnChar[charnum].setBorder(Globals.bordButtonActive);
		jlblCharChar.setText("" + (char)charnum);
		jlblCharInt.setText((charnum < 100 ? " " : "") + charnum);
		jlblCharHex.setText(Integer.toHexString(charnum));
		int cset = (int)(Math.floor(charnum / 8));
		jbtnChar[charnum].setBackground(TIGlobals.TI_PALETTE_OPAQUE[clrSets[cset][Globals.INDEX_CLR_BACK]]);
		if(hmCharGrids.get(getCharKey(charnum)) == null)
		{
			if(jbtnChar[charnum].getIcon() != null)
			{
				jbtnChar[charnum].setIcon(null);
				jbtnChar[charnum].setText((((charnum - TIGlobals.CHARMAPSTART) >= 0 && (charnum - TIGlobals.CHARMAPSTART) < TIGlobals.CHARMAP.length) ? "" + TIGlobals.CHARMAP[charnum - TIGlobals.CHARMAPSTART] : "?"));
			}
			return;
		}
		Graphics g;
		if(jbtnChar[charnum].getIcon() != null)
		{
		}
		else
		{
			jbtnChar[charnum].setText("");
			Image imgTemp = this.createImage(gcChar.getGridData().length, gcChar.getGridData()[0].length);
			if(imgTemp == null) { return; }
			jbtnChar[charnum].setIcon(new ImageIcon(imgTemp));
		}
		g = ((ImageIcon)(jbtnChar[charnum].getIcon())).getImage().getGraphics();
		int[][] charGrid = hmCharGrids.get(getCharKey(charnum));
		for(int y = 0; y < charGrid.length; y++)
		{
			for(int x = 0; x < charGrid[y].length; x++)
			{
				g.setColor(charGrid[y][x] == 1 ? TIGlobals.TI_PALETTE_OPAQUE[clrSets[cset][Globals.INDEX_CLR_FORE]] : TIGlobals.TI_PALETTE_OPAQUE[clrSets[cset][Globals.INDEX_CLR_BACK]]);
				g.fillRect(x, y, 1, 1);
			}
		}
		Image imgTrans = makeImageTransparent(((ImageIcon)(jbtnChar[charnum].getIcon())).getImage());
		hmCharImages.put(getCharKey(charnum), imgTrans);
		if(Globals.getByteString(hmCharGrids.get(getCharKey(charnum))).equals(Globals.BLANKCHAR))
		{
			jbtnChar[charnum].setIcon(null);
			jbtnChar[charnum].setText((((charnum - TIGlobals.CHARMAPSTART) >= 0 && (charnum - TIGlobals.CHARMAPSTART) < TIGlobals.CHARMAP.length) ? "" + TIGlobals.CHARMAP[charnum - TIGlobals.CHARMAPSTART] : "?"));
		}
		jbtnChar[charnum].repaint();
	}

	protected Image makeImageTransparent(Image imgSrc)
	{
		ImageProducer ipTrans = new FilteredImageSource(imgSrc.getSource(), Globals.ifTrans);
		return Toolkit.getDefaultToolkit().createImage(ipTrans);
	}

	protected void exitApp()
	{
		this.dispose();
		System.exit(0);
	}

// Main Method -----------------------------------------------------------------------------/

	public static void main(String[] args)
	{
		Charcot chcoal = new Charcot();
	}

}
