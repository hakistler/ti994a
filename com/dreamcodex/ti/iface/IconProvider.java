package com.dreamcodex.ti.iface;

import javax.swing.Icon;

public abstract interface IconProvider
{
	public Icon getDefaultIcon();
	public Icon getIconForObject(Object obj);
}
