package com.dreamcodex.ti;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

import com.dreamcodex.ti.component.TIClickEditor;
import com.dreamcodex.ti.util.TIGlobals;
import com.dreamcodex.ti.util.MutableFilter;

/** FonTI
  * TI-99/4A graphical character editor
  *
  * @author Howard Kistler
  */

public class FonTI extends JFrame implements WindowListener, KeyListener, ActionListener, MouseListener, MouseMotionListener
{
// Local Constants -------------------------------------------------------------------------/

	private final String RSCPATH = "com" + File.separator + "dreamcodex" + File.separator + "ti" + File.separator;
	private final String IMGPATH = RSCPATH + "images" + File.separator;
	private final String APPTITLE = "FonTI TI-99/4A Font Editor";
	private final String BLANKCHAR = "0000000000000000";
	private final String FILEEXT = "fonti";
	private final String[] FILEEXTS = { FILEEXT };
	private final String[] XBEXTS = { "xb", "bas", "txt" };

// Variables -------------------------------------------------------------------------------/

	private HashMap<String,int[][]> hmCharGrids;
	
	private int activeChar = -1;

	private File fontDataFile;

// Components ------------------------------------------------------------------------------/

	private TIClickEditor ceChar;
	private JTextField jtxtChar;
	private JButton jbtnUpdateChar;
	private JButton jbtnDraw;
	private JButton jbtnErase;
	private JButton jbtnRotL;
	private JButton jbtnRotR;
	private JButton jbtnClear;
	private JButton jbtnFill;

	private JButton[] jbtnChar;

// Constructors ----------------------------------------------------------------------------/

	public FonTI()
	{
		super();
		this.setTitle(APPTITLE);

		// Create the menus
		JMenuBar jMenuBar = new JMenuBar();
		JMenu jmenFile = new JMenu("File");
		JMenuItem jmitOpen = new JMenuItem("Open Font Data"); jmitOpen.setActionCommand("open"); jmitOpen.addActionListener(this); jmenFile.add(jmitOpen);
		JMenuItem jmitSave = new JMenuItem("Save Font Data"); jmitSave.setActionCommand("save"); jmitSave.addActionListener(this); jmenFile.add(jmitSave);
		jmenFile.addSeparator();
		JMenuItem jmitExport = new JMenuItem("Export To XB"); jmitExport.setActionCommand("export"); jmitExport.addActionListener(this); jmenFile.add(jmitExport);
		jmenFile.addSeparator();
		JMenuItem jmitExit = new JMenuItem("Exit"); jmitExit.setActionCommand("exit"); jmitExit.addActionListener(this); jmenFile.add(jmitExit);
		jMenuBar.add(jmenFile);

		// Create the toolkit
		JToolBar jToolkitBar = new JToolBar(JToolBar.VERTICAL);
		jToolkitBar.setFloatable(false);
		jbtnDraw = new JButton(getIcon("draw")); jbtnDraw.setActionCommand("draw"); jbtnDraw.addActionListener(this); jbtnDraw.setToolTipText("Draw"); jToolkitBar.add(jbtnDraw);
		jbtnErase = new JButton(getIcon("erase")); jbtnErase.setActionCommand("erase"); jbtnErase.addActionListener(this); jbtnErase.setToolTipText("Erase"); jToolkitBar.add(jbtnErase);
		jToolkitBar.add(new JToolBar.Separator());
		jbtnRotL = new JButton(getIcon("rotateleft"));  jbtnRotL.setActionCommand("rotateleft");  jbtnRotL.addActionListener(this); jbtnRotL.setToolTipText("Rotate Left");  jToolkitBar.add(jbtnRotL);
		jbtnRotR = new JButton(getIcon("rotateright")); jbtnRotR.setActionCommand("rotateright"); jbtnRotR.addActionListener(this); jbtnRotR.setToolTipText("Rotate Right"); jToolkitBar.add(jbtnRotR);
		jToolkitBar.add(new JToolBar.Separator());
		jbtnClear = new JButton(getIcon("clear")); jbtnClear.setActionCommand("clear"); jbtnClear.addActionListener(this); jbtnClear.setToolTipText("Clear Canvas"); jToolkitBar.add(jbtnClear);
		jbtnFill  = new JButton(getIcon("fill"));  jbtnFill.setActionCommand("fill");   jbtnFill.addActionListener(this);  jbtnFill.setToolTipText("Fill Canvas");   jToolkitBar.add(jbtnFill);

		// Create character editor panel
		JPanel jpnlChar = new JPanel(new BorderLayout());
		ceChar = new TIClickEditor(8, 8, 1, 15, (MouseListener)this, (MouseMotionListener)this);
		jpnlChar.add(ceChar, BorderLayout.CENTER);
		JPanel jpnlCharTool = new JPanel(new BorderLayout());
		jtxtChar = new JTextField();
		jpnlCharTool.add(jtxtChar, BorderLayout.CENTER);
		jbtnUpdateChar = new JButton("Set Char"); jbtnUpdateChar.setActionCommand("updategraphic"); jbtnUpdateChar.addActionListener(this); jpnlCharTool.add(jbtnUpdateChar, BorderLayout.EAST);
		jpnlChar.add(jpnlCharTool, BorderLayout.SOUTH);

		// Create character selector panel
		int fontRows = 16;
		int fontCols = 8;
		int charNum  = TIGlobals.FIRSTCHAR;
		int cellNum  = 0;
		hmCharGrids = new HashMap<String,int[][]>();
		JPanel jpnlFont = new JPanel(new GridLayout(fontRows, fontCols + 1));
		jbtnChar = new JButton[fontRows*fontCols];
		for(int r = 0; r < fontRows; r++)
		{
			jpnlFont.add(new JLabel((r + 1) + " ", JLabel.RIGHT));
			for(int c = 0; c < fontCols; c++)
			{
				cellNum = (r * fontCols) + c;
				jbtnChar[cellNum] = new JButton(((charNum - 30) < TIGlobals.CHARMAP.length ? "" + TIGlobals.CHARMAP[charNum - 30] : "?"));
				jbtnChar[cellNum].setActionCommand("editchar:" + charNum);
				jbtnChar[cellNum].addActionListener(this);
				jbtnChar[cellNum].setOpaque(true);
				jbtnChar[cellNum].setBackground(TIGlobals.TI_COLOR_WHITE);
				jbtnChar[cellNum].setForeground(TIGlobals.TI_COLOR_GREY);
				jbtnChar[cellNum].setMargin(new Insets(4, 4, 4, 4));
				jbtnChar[cellNum].setPreferredSize(new Dimension(32, 32));
				jpnlFont.add(jbtnChar[cellNum]);
				int[][] emtpyGrid = new int[8][8];
				for(int y = 0; y < emtpyGrid.length; y++) { for(int x = 0; x < emtpyGrid[y].length; x++) { emtpyGrid[y][x] = 0; } }
				hmCharGrids.put(getCharKey(charNum), emtpyGrid);
				charNum++;
			}
		}
		jpnlChar.add(jpnlFont, BorderLayout.EAST);

		// Assemble the application
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(jpnlChar, BorderLayout.CENTER);
		this.getContentPane().add(jMenuBar, BorderLayout.NORTH);
		this.getContentPane().add(jToolkitBar, BorderLayout.EAST);
		this.addKeyListener(this);
		this.addWindowListener(this);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.pack();
		Insets insets = this.getInsets();
		this.setSize(800, 600);
		this.setVisible(true);

		activeChar = TIGlobals.FIRSTCHAR;
		jbtnChar[0].setSelected(true);
		ceChar.setGridData(hmCharGrids.get(getCharKey(activeChar)));
	}

// Listeners -------------------------------------------------------------------------------/

	/* KeyListener methods */
	public void keyPressed(KeyEvent ke)
	{
		if(ke.getKeyCode() == KeyEvent.VK_ESCAPE)
		{
			this.dispose();
			System.exit(0);
		}
	}
	public void keyReleased(KeyEvent ke) { ; }
	public void keyTyped(KeyEvent ke)    { ; }

	/* ActionListener methods */
	public void actionPerformed(ActionEvent ae)
	{
		try
		{
			String command = ae.getActionCommand();
			if(command.equals("exit"))
			{
				this.dispose();
				System.exit(0);
			}
			else if(command.equals("draw"))
			{
				ceChar.setPaintOn(true);
			}
			else if(command.equals("erase"))
			{
				ceChar.setPaintOn(false);
			}
			else if(command.equals("clear"))
			{
				ceChar.clearGrid();
				hmCharGrids.put(getCharKey(activeChar), ceChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals("fill"))
			{
				ceChar.fillGrid();
				hmCharGrids.put(getCharKey(activeChar), ceChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals("rotateleft"))
			{
				ceChar.setGridData(rotateGrid(ceChar.getGridData(),true));
				ceChar.getEditor().redrawCanvas();
				hmCharGrids.put(getCharKey(activeChar), ceChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals("rotateright"))
			{
				ceChar.setGridData(rotateGrid(ceChar.getGridData(),false));
				ceChar.getEditor().redrawCanvas();
				hmCharGrids.put(getCharKey(activeChar), ceChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals("open"))
			{
				openDataFile();
			}
			else if(command.equals("save"))
			{
				saveDataFile();
			}
			else if(command.equals("export"))
			{
				exportDataFile();
			}
			else if(command.startsWith("editchar:"))
			{
				activeChar = Integer.parseInt(command.substring(command.indexOf(":") + 1));
				ceChar.setGridData(hmCharGrids.get(getCharKey(activeChar)));
				ceChar.getEditor().redrawCanvas();
				updateComponents();
			}
			else if(command.equals("updategraphic"))
			{
				hmCharGrids.put(getCharKey(activeChar), getByteIntArray(jtxtChar.getText(), 8));
				ceChar.setGridData(hmCharGrids.get(getCharKey(activeChar)));
				ceChar.getEditor().redrawCanvas();
				updateFontButton(activeChar);
			}
		}
		catch(Exception e) { ; }
	}

	/* WindowListener methods */
	public void windowClosing(WindowEvent we)
	{
		this.dispose();
		System.exit(0);
	}
	public void windowOpened(WindowEvent we)      { ; }
	public void windowClosed(WindowEvent we)      { ; }
	public void windowActivated(WindowEvent we)   { ; }
	public void windowDeactivated(WindowEvent we) { ; }
	public void windowIconified(WindowEvent we)   { ; }
	public void windowDeiconified(WindowEvent we) { ; }

	/* MouseListener methods */
	public void mousePressed(MouseEvent me)
	{
		updateComponents();
	}
	public void mouseReleased(MouseEvent me) {}
	public void mouseClicked(MouseEvent me) {}
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}

	/* MouseMotionListener methods */
	public void mouseMoved(MouseEvent me) {}
	public void mouseDragged(MouseEvent me)
	{
		updateComponents();
	}

// File Handling Methods -------------------------------------------------------------------/

	protected void openDataFile()
	{
		File whatFile = getFileFromChooser((fontDataFile != null ? fontDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, FILEEXTS, "Font Data Files");
		if(whatFile != null)
		{
			fontDataFile = whatFile;
			this.setTitle(APPTITLE + " : " + fontDataFile.getName());
			readDataFile(fontDataFile);
		}
		int charNum = TIGlobals.FIRSTCHAR;
		for(int cn = 0; cn < jbtnChar.length; cn++)
		{
			jbtnChar[cn].setIcon((ImageIcon)null);
			jbtnChar[cn].setText(((charNum - 30) < TIGlobals.CHARMAP.length ? "" + TIGlobals.CHARMAP[charNum - 30] : "?"));
			charNum++;
		}
		for(int i = TIGlobals.FIRSTCHAR; i <= TIGlobals.LASTCHAR; i++)
		{
			updateFontButton(i);
		}
		activeChar = TIGlobals.FIRSTCHAR;
		updateComponents();
	}

	protected void saveDataFile()
	{
		File whatFile = getFileFromChooser((fontDataFile != null ? fontDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, FILEEXTS, "Font Data Files");
		if(whatFile != null)
		{
			if(!whatFile.getAbsolutePath().toLowerCase().endsWith("." + FILEEXT))
			{
				whatFile = new File(whatFile.getAbsolutePath() + "." + FILEEXT);
			}
			fontDataFile = whatFile;
			this.setTitle(APPTITLE + " : " + fontDataFile.getName());
			writeDataFile(fontDataFile);
		}
	}

	protected void exportDataFile()
	{
		File whatFile = getFileFromChooser((fontDataFile != null ? fontDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, XBEXTS, "XB Data Files");
		if(whatFile != null)
		{
			ExportDialog exporter = new ExportDialog(this);
			if(exporter.isOkay())
			{
				int sChar = exporter.getStartChar() + TIGlobals.FIRSTCHAR;
				int eChar = exporter.getEndChar() + TIGlobals.FIRSTCHAR;
				int sLine = exporter.getLineStart();
				int iLine = exporter.getLineInterval();
				writeXBDataFile(whatFile, Math.min(sChar, eChar), Math.max(sChar, eChar), sLine, iLine);
			}
			exporter.dispose();
		}
	}

	private File getFileFromChooser(String startDir, int dialogType, String[] exts, String desc)
	{
		JFileChooser jfileDialog = new JFileChooser(startDir);
		jfileDialog.setDialogType(dialogType);
		jfileDialog.setFileFilter(new MutableFilter(exts, desc));
		int optionSelected = JFileChooser.CANCEL_OPTION;
		if(dialogType == JFileChooser.OPEN_DIALOG)
		{
			optionSelected = jfileDialog.showOpenDialog(this);
		}
		else if(dialogType == JFileChooser.SAVE_DIALOG)
		{
			optionSelected = jfileDialog.showSaveDialog(this);
		}
		else // default to an OPEN_DIALOG
		{
			optionSelected = jfileDialog.showOpenDialog(this);
		}
		if(optionSelected == JFileChooser.APPROVE_OPTION)
		{
			return jfileDialog.getSelectedFile();
		}
		return (File)null;
	}

	protected void readDataFile(File fontDataFile)
	{
		hmCharGrids.clear();
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(fontDataFile));
			String lineIn = "";
			int charRead = TIGlobals.FIRSTCHAR;
			do
			{
				lineIn = br.readLine();
				if(lineIn == null)
				{
					break;
				}
				else
				{
					hmCharGrids.put(getCharKey(charRead), getByteIntArray(lineIn, 8));
					charRead++;
				}
			} while (lineIn != null);
			br.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	protected void writeDataFile(File fontDataFile)
	{
		try
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(fontDataFile));
			for(int i = TIGlobals.FIRSTCHAR; i <= TIGlobals.LASTCHAR; i++)
			{
				if(hmCharGrids.get(getCharKey(i)) != null)
				{
					String hexstr = getByteString(hmCharGrids.get(getCharKey(i)));
					bw.write(hexstr, 0, hexstr.length());
				}
				else
				{
					bw.write(BLANKCHAR, 0, BLANKCHAR.length());
				}
				bw.newLine();
			}
			bw.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
		updateComponents();
	}

	protected void writeXBDataFile(File fontDataFile, int startChar, int endChar, int startLine, int interLine)
	{
		int currLine = startLine;
		int itemCount = 0;
		StringBuffer sbOutLine = new StringBuffer();
		try
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(fontDataFile));
			for(int i = startChar; i <= endChar; i++)
			{
				String hexstr;
				if(hmCharGrids.get(getCharKey(i)) != null)
				{
					hexstr = getByteString(hmCharGrids.get(getCharKey(i)));
				}
				else
				{
					hexstr = BLANKCHAR;
				}
				if(itemCount == 0) { sbOutLine.append(currLine + " DATA "); }
				else { sbOutLine.append(", "); }
				sbOutLine.append('"' + hexstr + '"');
				itemCount++;
				if(itemCount >= 6)
				{
					bw.write(sbOutLine.toString());
					bw.newLine();
					sbOutLine.delete(0, sbOutLine.length());
					currLine = currLine + interLine;
					itemCount = 0;
				}
			}
			if(sbOutLine.length() > 0) { bw.write(sbOutLine.toString()); bw.newLine(); }
			bw.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

// Convenience Methods ---------------------------------------------------------------------/

	public String getByteString(int[][] binarray)
	{
		StringBuffer sbBytes = new StringBuffer();
		int bytepos = 0;
		int byteval = 0;
		int charnum = 0;
		int charrow = (binarray.length / 8);
		int charcol = (binarray[0].length / 8);
		for(int cc = 0; cc < charcol; cc++)
		{
			for(int cr = 0; cr < charrow; cr++)
			{
				for(int y = 0; y < 8; y++)
				{
					int cy = y + (cr * 8);
					bytepos = 0;
					for(int x = 0; x < 8; x++)
					{
						int cx = x + (cc * 8);
						if(binarray[cy][cx] > 0) { byteval += (bytepos == 0 ? 8 : (bytepos == 1 ? 4 : (bytepos == 2 ? 2 : 1))); }
						bytepos++;
						if(bytepos > 3)
						{
							sbBytes.append(Integer.toHexString(byteval));
							byteval = 0;
							bytepos = 0;
						}
					}
					if(bytepos != 0)
					{
						sbBytes.append(Integer.toHexString(byteval));
						byteval = 0;
						bytepos = 0;
					}
				}
			}
		}
		return sbBytes.toString();
	}

	public int[][] getByteIntArray(String sbtyes, int rows)
	{
		int charcol = (sbtyes.length() / rows) * 4;
		int[][] barray = new int[rows][charcol];
		int charpos = 0;
		int rowpos = 0;
		int colpos = 0;
		for(int cc = 0; cc < sbtyes.length(); cc++)
		{
			String ch = "" + sbtyes.charAt(cc);
			int chi = Integer.parseInt(ch, 16);
			barray[rowpos][charpos+colpos] = (((chi & 8) == 8) ? 1 : 0); charpos++;
			barray[rowpos][charpos+colpos] = (((chi & 4) == 4) ? 1 : 0); charpos++;
			barray[rowpos][charpos+colpos] = (((chi & 2) == 2) ? 1 : 0); charpos++;
			barray[rowpos][charpos+colpos] = (((chi & 1) == 1) ? 1 : 0); charpos++;
			if(charpos >= 8)
			{
				charpos = 0;
				rowpos++;
				if(rowpos >= rows)
				{
					rowpos = 0;
					colpos += 8;
				}
			}
		}
		return barray;
	}

	protected ImageIcon getIcon(String name)
	{
		return new ImageIcon(IMGPATH + "icon_" + name + ".png");
	}

	protected int[][] rotateGrid(int[][] grid, boolean isLeft)
	{
		int height = grid.length;
		int width  = grid[0].length;
		int rotx = 0;
		int roty = 0;
		int[][] rotatedGrid = new int[width][height];
		for(int y = 0; y < height; y++)
		{
			for(int x = 0; x < width; x++)
			{
				if(isLeft)
				{
					rotx = y;
					roty = (height - 1) - x;
				}
				else
				{
					rotx = (width - 1) - y;
					roty = x;
				}
				rotatedGrid[roty][rotx] = grid[y][x];
			}
		}
		return rotatedGrid;
	}

	protected String getCharKey(int charnum)
	{
		return ("ch" + charnum);
	}

	protected void updateComponents()
	{
		updateFontButton(activeChar);
		jtxtChar.setText(getByteString(ceChar.getGridData()).toUpperCase());
	}

	protected void updateFontButton(int charnum)
	{
		int index = charnum - TIGlobals.FIRSTCHAR;
		if(
			hmCharGrids.get(getCharKey(charnum)) == null || 
			getByteString(hmCharGrids.get(getCharKey(charnum))).equals(BLANKCHAR)
		)
		{
			if(jbtnChar[index].getIcon() != null)
			{
				jbtnChar[index].setIcon(null);
				jbtnChar[index].setText((charnum + 2) < TIGlobals.CHARMAP.length ? "" + TIGlobals.CHARMAP[charnum - 30] : "?");
			}
			return;
		}
		Graphics g;
		if(jbtnChar[index].getIcon() != null)
		{
		}
		else
		{
			jbtnChar[index].setText("");
			Image imgTemp = this.createImage(ceChar.getGridData().length, ceChar.getGridData()[0].length);
			jbtnChar[index].setIcon(new ImageIcon(imgTemp));
		}
		g = ((ImageIcon)(jbtnChar[index].getIcon())).getImage().getGraphics();
		int[][] charGrid = hmCharGrids.get(getCharKey(charnum));
		for(int y = 0; y < charGrid.length; y++)
		{
			for(int x = 0; x < charGrid[y].length; x++)
			{
				g.setColor(charGrid[y][x] == 1 ? Color.black : Color.white);
				g.fillRect(x, y, 1, 1);
			}
		}
		jbtnChar[index].repaint();
	}

// Inner Classes ---------------------------------------------------------------------------/

	class ExportDialog extends JDialog implements PropertyChangeListener
	{
		private String OK_TEXT = "Export";
		private String CANCEL_TEXT = "Cancel";

		private JComboBox jcmbStartChar;
		private JComboBox jcmbEndChar;
		private JTextField jtxtLineStart;
		private JTextField jtxtLineInterval;
		private boolean clickedOkay = false;;

		public ExportDialog(JFrame parent)
		{
			super(parent, "Export Settings", true);
			jcmbStartChar = new JComboBox();
			jcmbEndChar = new JComboBox();
			jtxtLineStart = new JTextField("9000");
			jtxtLineInterval = new JTextField("10");
			int chardex = 0;
			for(int i = TIGlobals.FIRSTCHAR; i <= TIGlobals.LASTCHAR; i++)
			{
				chardex = i - 30;
				if(chardex < TIGlobals.CHARMAP.length)
				{
					jcmbStartChar.addItem(new String("" + TIGlobals.CHARMAP[chardex]));
					jcmbEndChar.addItem(new String("" + TIGlobals.CHARMAP[chardex]));
				}
				else
				{
					jcmbStartChar.addItem(new String("x" + (chardex - TIGlobals.CHARMAP.length)));
					jcmbEndChar.addItem(new String("x" + (chardex - TIGlobals.CHARMAP.length)));
				}
			}
			jcmbStartChar.setSelectedIndex(0);
			jcmbEndChar.setSelectedIndex(jcmbEndChar.getItemCount() - 1);

			Object[] objForm = new Object[9];
			objForm[0] = new JLabel("Character Range");
			objForm[1] = new JLabel("From Char #");
			objForm[2] = jcmbStartChar;
			objForm[3] = new JLabel("To Char #");
			objForm[4] = jcmbEndChar;
			objForm[5] = new JLabel("Line Number Start");
			objForm[6] = jtxtLineStart;
			objForm[7] = new JLabel("Line Number Interval");
			objForm[8] = jtxtLineInterval;
			Object[] objButtons = { OK_TEXT, CANCEL_TEXT };

			JOptionPane joptMain = new JOptionPane(objForm, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null, objButtons, objButtons[0]);
			joptMain.addPropertyChangeListener(this);
			this.setContentPane(joptMain);

			this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
			this.pack();
			this.setLocationRelativeTo(this);
			this.setVisible(true);
		}

		public int getStartChar() { return jcmbStartChar.getSelectedIndex(); }
		public int getEndChar()   { return jcmbEndChar.getSelectedIndex(); }
		public int getLineStart() { return Integer.parseInt(jtxtLineStart.getText()); }
		public int getLineInterval() { return Integer.parseInt(jtxtLineInterval.getText()); }
		public boolean isOkay() { return clickedOkay; }

		/* PropertyChangeListener method */
		public void propertyChange(PropertyChangeEvent pce)
		{
			if(pce != null && pce.getNewValue() != null)
			{
				if(pce.getNewValue().equals(OK_TEXT))
				{
					clickedOkay = true;
					this.setVisible(false);
				}
				else if(pce.getNewValue().equals(CANCEL_TEXT))
				{
					clickedOkay = false;
					this.setVisible(false);
				}
			}
		}
	}

// Main Method -----------------------------------------------------------------------------/

	public static void main(String[] args)
	{
		FonTI fonti = new FonTI();
	}

}
