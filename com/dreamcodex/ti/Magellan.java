package com.dreamcodex.ti;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.net.URL;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.*;

import com.dreamcodex.ti.component.GridCanvas;
import com.dreamcodex.ti.component.MapCanvas;
import com.dreamcodex.ti.component.MapEditor;
import com.dreamcodex.ti.component.ImagePreview;
import com.dreamcodex.ti.component.DualClickButton;
import com.dreamcodex.ti.component.MagellanExportDialog;
import com.dreamcodex.ti.component.CharacterSwapDialog;
import com.dreamcodex.ti.component.VramImportDialog;
import com.dreamcodex.ti.iface.IconProvider;
import com.dreamcodex.ti.util.Globals;
import com.dreamcodex.ti.util.TIGlobals;
import com.dreamcodex.ti.util.MutableFilter;

/** Magellan
  * TI-99/4A graphical map editor
  *
  * @author Howard Kistler
  */

public class Magellan extends JFrame implements WindowListener, ActionListener, MouseListener, MouseMotionListener, IconProvider
{
// Constants -------------------------------------------------------------------------------/

	public static final String CMD_LOADDEFS  = "loaddefaultchars";
	public static final String CMD_CHARTOOL  = "togglechartool";
	public static final String CMD_SWAPCHARS = "swapcharacters";
	public static final String CMD_MODESET   = "changemode";
	public static final String CMD_ABOUT     = "helpabout";
    public static final String CMD_BASEPOS   = "baseposition";

	public static final byte[] BIN_HEADER_MAG = { (byte)'M', (byte)'G' };
	public static final byte[] BIN_HEADER_VER = { (byte)'0', (byte)'1' };

	public static final byte BIN_CHUNK_COLORS  = 1<<1;
	public static final byte BIN_CHUNK_CHARS   = 1<<2;
	public static final byte BIN_CHUNK_SPRITES = 1<<3;

	public static final byte BIN_MAP_HEADER_RESERVED1 = 0;
	public static final byte BIN_MAP_HEADER_RESERVED2 = 0;
	public static final byte BIN_MAP_HEADER_RESERVED3 = 0;
	public static final byte BIN_MAP_HEADER_RESERVED4 = 0;

	public static final double VERSION_NUMBER = 1.4;

// Local Constants -------------------------------------------------------------------------/

	private final String APPTITLE = "Magellan : TI-99/4A Map Editor (version " + VERSION_NUMBER + ")";
	private final String FILEEXT = "mag";
	private final String[] FILEEXTS = { FILEEXT };
	private final String XBEXT = "xb";
	private final String[] XBEXTS = { XBEXT, "bas", "txt" };
	private final String ASMEXT = "asm";
	private final String[] ASMEXTS = { ASMEXT };
	private final String IMGEXT = "png";
	private final String[] IMGEXTS = { IMGEXT };
	private final String BINEXT = "mgb";
	private final String[] BINEXTS = { BINEXT };
	private final String VDPEXT = "vdp";
	private final String[] VDPEXTS = { VDPEXT, "vram" };

	private final int FONT_ROWS = 32;
	private final int BAS_FONT_ROWS = 16;
	private final int ADV_FONT_ROWS = 32;
	private final int FONT_COLS = 8;

	private final int MAP_ROWS = 24;
	private final int MAP_COLS = 32;
	private final int MAP_CELL = 8;

	private final int ASM_LINELEN = 40;

	private final Color CLR_CHARS_BASE1 = new Color(232, 232, 232);
	private final Color CLR_CHARS_BASE2 = new Color(196, 196, 196);
	private final Color CLR_CHARS_LOWER = new Color(222, 242, 255);
	private final Color CLR_CHARS_UPPER = new Color(255, 222, 242);

// Variables -------------------------------------------------------------------------------/

	protected boolean advancedModeOn = false;
    protected boolean basePositionIsZero = false;

	protected HashMap<String,int[][]> hmCharGrids;
	protected HashMap<String,int[][]> hmDefaultChars;
	protected HashMap<String,Image> hmCharImages;

	protected int[][] clrSets = new int[FONT_ROWS][2];

	protected int activeChar = TIGlobals.CUSTOMCHAR;
	protected int lastActiveChar = MapCanvas.NOCHAR;

	protected File mapDataFile;

	protected boolean bExportComments = true;
	protected int     defStartChar    = TIGlobals.FIRSTCHAR;
	protected int     defEndChar      = TIGlobals.LASTCHAR;

	protected String[] sDefaultTIChars =
	{
		"0000000000000000",
		"0010101010100010",
		"0028282800000000",
		"0028287C287C2828",
		"0038545038145438",
		"0060640810204C0C",
		"0020505020544834",
		"0008081000000000",
		"0008102020201008",
		"0020100808081020",
		"000028107C102800",
		"000010107C101000",
		"0000000000301020",
		"000000007C000000",
		"0000000000003030",
		"0000040810204000",
		"0038444444444438",
		"0010301010101038",
		"003844040810207C",
		"0038440418044438",
		"00081828487C0808",
		"007C407804044438",
		"0018204078444438",
		"007C040810202020",
		"0038444438444438",
		"003844443C040830",
		"0000303000303000",
		"0000303000301020",
		"0008102040201008",
		"0000007C007C0000",
		"0020100804081020",
		"0038440408100010",
		"0038445C545C4038",
		"003844447C444444",
		"0078242438242478",
		"0038444040404438",
		"0078242424242478",
		"007C40407840407C",
		"007C404078404040",
		"003C40405C444438",
		"004444447C444444",
		"0038101010101038",
		"0004040404044438",
		"0044485060504844",
		"004040404040407C",
		"00446C5454444444",
		"00446464544C4C44",
		"007C44444444447C",
		"0078444478404040",
		"0038444444544834",
		"0078444478504844",
		"0038444038044438",
		"007C101010101010",
		"0044444444444438",
		"0044444428281010",
		"0044444454545428",
		"0044442810284444",
		"0044442810101010",
		"007C04081020407C",
		"0038202020202038",
		"0000402010080400",
		"0038080808080838",
		"0000102844000000",
		"000000000000007C",
		"0000201008000000",
		"00000038447C4444",
		"0000007824382478",
		"0000003C4040403C",
		"0000007824242478",
		"0000007C4078407C",
		"0000007C40784040",
		"0000003C405C4438",
		"00000044447C4444",
		"0000003810101038",
		"0000000808084830",
		"0000002428302824",
		"000000404040407C",
		"000000446C544444",
		"0000004464544C44",
		"0000007C4444447C",
		"0000007844784040",
		"0000003844544834",
		"0000007844784844",
		"0000003C40380478",
		"0000007C10101010",
		"0000004444444438",
		"0000004444282810",
		"0000004444545428",
		"0000004428102844",
		"0000004428101010",
		"0000007C0810207C",
		"0018202040202018",
		"0010101000101010",
		"0030080804080830",
		"0000205408000000",
		"0000000000000000"
	};

// Components ------------------------------------------------------------------------------/

	private JPanel jpnlMain;

	private JPanel jpnlEdit;

	private JPanel jpnlChar;
	private GridCanvas gcChar;
	private JTextField jtxtChar;
	private JButton jbtnUpdateChar;
	private JButton jbtnLook;
	private JButton jbtnClone;
	private JLabel jlblCharInt;
	private JLabel jlblCharHex;

	private JPanel jpnlCharacterEditor;

	private JButton[] jbtnChar;
	private JPanel jpnlFont;
	private JScrollPane jsclFont;

	private MapEditor mapdMain;

	private Properties appProperties = new Properties();

// Constructors ----------------------------------------------------------------------------/

	public Magellan()
	{
		super();
		this.setTitle(APPTITLE);

		// obtain application properties (if exist)
		try
		{
			FileInputStream fis = new FileInputStream(new File("Magellan.prefs"));
			appProperties.load(fis);
			fis.close();
		}
		catch(Exception e)
		{
		}

		hmDefaultChars = new HashMap<String,int[][]>();
		for(int ch = TIGlobals.CHARMAPSTART; ch <= TIGlobals.CHARMAPEND; ch++)
		{
			hmDefaultChars.put(getCharKey(ch), Globals.getByteIntArray(sDefaultTIChars[ch - TIGlobals.CHARMAPSTART], 8));
		}

		if(appProperties.getProperty("exportComments") != null) { bExportComments = appProperties.getProperty("exportComments").toLowerCase().equals("true"); }
		if(appProperties.getProperty("expandCharacters") != null) { advancedModeOn = appProperties.getProperty("expandCharacters").toLowerCase().equals("true"); }
		if(appProperties.getProperty("defStartChar") != null) { defStartChar = Integer.parseInt(appProperties.getProperty("defStartChar")); }
		if(appProperties.getProperty("defEndChar") != null) { defEndChar = Integer.parseInt(appProperties.getProperty("defEndChar")); }

		// Create map editor panel (needs to initialise early for the listeners)
		mapdMain = new MapEditor(MAP_COLS, MAP_ROWS, MAP_CELL, (MouseListener)this, (MouseMotionListener)this);
		mapdMain.fillGrid(TIGlobals.SPACECHAR);
		mapdMain.setBkgrndColor(Globals.CLR_COMPONENTBACK);
		if(appProperties.getProperty("magnif") != null) { mapdMain.setViewScale(Integer.parseInt(appProperties.getProperty("magnif"))); }
		if(appProperties.getProperty("textCursor") != null) { mapdMain.setTypeCellOn(appProperties.getProperty("textCursor").toLowerCase().equals("true")); }
		if(appProperties.getProperty("showGrid") != null) { mapdMain.setShowGrid(appProperties.getProperty("showGrid").toLowerCase().equals("true")); }
		if(appProperties.getProperty("showPosition") != null) { mapdMain.setShowPosIndic(appProperties.getProperty("showPosition").toLowerCase().equals("true")); }

		// Create the menus
		JMenuBar jMenuBar = new JMenuBar();
		JMenu jmenFile = new JMenu("File");
		JMenuItem jmitNew = new JMenuItem("New Map Project"); jmitNew.setActionCommand(Globals.CMD_NEW); jmitNew.addActionListener(this); jmenFile.add(jmitNew);
		JMenuItem jmitOpen = new JMenuItem("Open Map Project"); jmitOpen.setActionCommand(Globals.CMD_OPEN); jmitOpen.addActionListener(this); jmenFile.add(jmitOpen);
		JMenuItem jmitSave = new JMenuItem("Save Map Project"); jmitSave.setActionCommand(Globals.CMD_SAVE); jmitSave.addActionListener(this); jmenFile.add(jmitSave);
		jmenFile.addSeparator();
		JMenuItem jmitAppend = new JMenuItem("Append Maps"); jmitAppend.setActionCommand(Globals.CMD_APPEND); jmitAppend.addActionListener(this); jmenFile.add(jmitAppend);
		jmenFile.addSeparator();
//		JMenuItem jmitLoadDefs = new JMenuItem("Load Default Characters"); jmitLoadDefs.setActionCommand(CMD_LOADDEFS); jmitLoadDefs.addActionListener(this); jmenFile.add(jmitLoadDefs);
//		jmenFile.addSeparator();
		JMenuItem jmitExit = new JMenuItem("Exit"); jmitExit.setActionCommand(Globals.CMD_EXIT); jmitExit.addActionListener(this); jmenFile.add(jmitExit);
		jMenuBar.add(jmenFile);

		JMenu jmenImport = new JMenu("Import");
		JMenuItem jmitImportChrImgMono = new JMenuItem("Character Image (Mono)"); jmitImportChrImgMono.setActionCommand(Globals.CMD_MPCIMGMN); jmitImportChrImgMono.addActionListener(this); jmenImport.add(jmitImportChrImgMono);
		JMenuItem jmitImportChrImgColor = new JMenuItem("Character Image (Color)"); jmitImportChrImgColor.setActionCommand(Globals.CMD_MPCIMGCL); jmitImportChrImgColor.addActionListener(this); jmenImport.add(jmitImportChrImgColor);
		JMenuItem jmitImportVramDump = new JMenuItem("VRAM Dump"); jmitImportVramDump.setActionCommand(Globals.CMD_MPVDP); jmitImportVramDump.addActionListener(this); jmenImport.add(jmitImportVramDump);
		jMenuBar.add(jmenImport);

		JMenu jmenExport = new JMenu("Export");
		JMenuItem jmitExportData = new JMenuItem("BASIC Data"); jmitExportData.setActionCommand(Globals.CMD_XPDATA); jmitExportData.addActionListener(this); jmenExport.add(jmitExportData);
		JMenuItem jmitExportExec = new JMenuItem("XB Program"); jmitExportExec.setActionCommand(Globals.CMD_XPEXEC); jmitExportExec.addActionListener(this); jmenExport.add(jmitExportExec);
		JMenuItem jmitExportAsm = new JMenuItem("Assembler Data"); jmitExportAsm.setActionCommand(Globals.CMD_XPASM); jmitExportAsm.addActionListener(this); jmenExport.add(jmitExportAsm);
		JMenuItem jmitExportBin = new JMenuItem("Binary Data"); jmitExportBin.setActionCommand(Globals.CMD_XPBIN); jmitExportBin.addActionListener(this); jmenExport.add(jmitExportBin);
		jmenExport.addSeparator();
		JMenuItem jmitExportChrImgMono = new JMenuItem("Character Image (Mono)"); jmitExportChrImgMono.setActionCommand(Globals.CMD_XPCIMGMN); jmitExportChrImgMono.addActionListener(this); jmenExport.add(jmitExportChrImgMono);
		JMenuItem jmitExportChrImgColor = new JMenuItem("Character Image (Color)"); jmitExportChrImgColor.setActionCommand(Globals.CMD_XPCIMGCL); jmitExportChrImgColor.addActionListener(this); jmenExport.add(jmitExportChrImgColor);
		JMenuItem jmitExportMapImg = new JMenuItem("Map Image"); jmitExportMapImg.setActionCommand(Globals.CMD_XPMAPIMG); jmitExportMapImg.addActionListener(this); jmenExport.add(jmitExportMapImg);
		jMenuBar.add(jmenExport);

		JMenu jmenTools = new JMenu("Tools");
		JMenuItem jmitSwapChars = new JMenuItem("Replace Characters"); jmitSwapChars.setActionCommand(CMD_SWAPCHARS); jmitSwapChars.addActionListener(this); jmenTools.add(jmitSwapChars);
		jMenuBar.add(jmenTools);

		JMenu jmenOptions = new JMenu("Options");
		JMenuItem jmitShowPos = new JCheckBoxMenuItem("Show Position", mapdMain.showPosIndic()); jmitShowPos.setActionCommand(Globals.CMD_SHOWPOS); jmitShowPos.addActionListener(this); jmenOptions.add(jmitShowPos);
		JMenuItem jmitBasePos = new JCheckBoxMenuItem("Base Position = 0", mapdMain.isBasePosZero()); jmitBasePos.setActionCommand(Globals.CMD_BASEPOS); jmitBasePos.addActionListener(this); jmenOptions.add(jmitBasePos);        
		JMenuItem jmitChangeMode = new JCheckBoxMenuItem("Expanded Characters", advancedModeOn); jmitChangeMode.setActionCommand(CMD_MODESET); jmitChangeMode.addActionListener(this); jmenOptions.add(jmitChangeMode);
		jmenOptions.addSeparator();
		JMenuItem jmitHelpAbout = new JMenuItem("About Magellan"); jmitHelpAbout.setActionCommand(CMD_ABOUT); jmitHelpAbout.addActionListener(this); jmenOptions.add(jmitHelpAbout);
		jMenuBar.add(jmenOptions);

		// Create the main panel
		jpnlMain = getPanel(new BorderLayout());

		// Create Character Editor
		JPanel jpnlChTools = getPanel(new GridBagLayout());
		GridBagConstraints gbci = new GridBagConstraints();
		gbci.anchor = GridBagConstraints.CENTER;
		gbci.fill = GridBagConstraints.NONE;
		gbci.gridheight = 1;
		gbci.gridwidth = 1;
		gbci.gridx = 1;
		gbci.gridy = 1;
		gbci.insets = new Insets(1, 1, 1, 1);
		gbci.ipadx = 2;
		gbci.ipady = 2;
		gbci.weightx = 1;
		gbci.weighty = 1;

		gbci.gridx = 1; gbci.gridy = 1; gbci.gridheight = 5; gbci.gridwidth = 1; gbci.anchor = GridBagConstraints.NORTH;
		JPanel jpnlToolButtons = getPanel(new GridLayout(6, 1, 0, 2));
		jpnlToolButtons.add(getToolButton(Globals.CMD_FILL, "Fill"));
		jpnlToolButtons.add(getToolButton(Globals.CMD_CLEAR, "Clear"));
		jpnlToolButtons.add(getToolButton(Globals.CMD_INVERT, "Invert Image"));
		jpnlToolButtons.add(getToolButton(Globals.CMD_GRID, "Toggle Grid"));
		jbtnLook = getToolButton(Globals.CMD_LOOK, "Look At Character"); jpnlToolButtons.add(jbtnLook);
		jbtnClone = getToolButton(Globals.CMD_CLONE, "Select Region To Clone"); jpnlToolButtons.add(jbtnClone);
		jpnlChTools.add(jpnlToolButtons, gbci);

		gbci.gridx = 2; gbci.gridy = 1; gbci.gridheight = 1; gbci.gridwidth = 1; gbci.anchor = GridBagConstraints.CENTER;
		jpnlChTools.add(getToolButton(Globals.CMD_ROTATEL, "Rotate Left", Globals.CLR_BUTTON_TRANS), gbci);
		gbci.gridx = 3; gbci.gridy = 1;
		jlblCharInt = getLabel("", JLabel.CENTER); jlblCharInt.setPreferredSize(Globals.DM_TOOL); jpnlChTools.add(jlblCharInt, gbci);
		gbci.gridx = 4; gbci.gridy = 1;
		jpnlChTools.add(getToolButton(Globals.CMD_SHIFTU, "Shift Up", Globals.CLR_BUTTON_SHIFT), gbci);
		gbci.gridx = 5; gbci.gridy = 1;
		jlblCharHex = getLabel("", JLabel.CENTER); jlblCharHex.setPreferredSize(Globals.DM_TOOL); jpnlChTools.add(jlblCharHex, gbci);
		gbci.gridx = 6; gbci.gridy = 1;
		jpnlChTools.add(getToolButton(Globals.CMD_ROTATER, "Rotate Right", Globals.CLR_BUTTON_TRANS), gbci);
		gbci.gridx = 2; gbci.gridy = 3;
		jpnlChTools.add(getToolButton(Globals.CMD_SHIFTL, "Shift Left", Globals.CLR_BUTTON_SHIFT), gbci);
		gbci.gridx = 6; gbci.gridy = 3;
		jpnlChTools.add(getToolButton(Globals.CMD_SHIFTR, "Shift Right", Globals.CLR_BUTTON_SHIFT), gbci);
		gbci.gridx = 2; gbci.gridy = 5;
		jpnlChTools.add(getToolButton(Globals.CMD_FLIPH, "Flip Horizontal", Globals.CLR_BUTTON_TRANS), gbci);
		gbci.gridx = 4; gbci.gridy = 5;
		jpnlChTools.add(getToolButton(Globals.CMD_SHIFTD, "Shift Down", Globals.CLR_BUTTON_SHIFT), gbci);
		gbci.gridx = 6; gbci.gridy = 5;
		jpnlChTools.add(getToolButton(Globals.CMD_FLIPV, "Flip Vertical", Globals.CLR_BUTTON_TRANS), gbci);

		gbci.gridx = 3; gbci.gridy = 2; gbci.gridheight = 3; gbci.gridwidth = 3; gbci.fill = GridBagConstraints.BOTH; gbci.weightx = 2; gbci.weighty = 2; gbci.anchor = GridBagConstraints.CENTER;
		jpnlChar = getPanel(new BorderLayout());
		gcChar = new GridCanvas(8, 8, 8, (MouseListener)this, (MouseMotionListener)this);
		jpnlChar.add(gcChar, BorderLayout.CENTER);
		jpnlChar.setPreferredSize(new Dimension(128, 128));
		jpnlChTools.add(jpnlChar, gbci);

		JPanel jpnlCharTool = getPanel(new BorderLayout());
		jtxtChar = new JTextField();
		jpnlCharTool.add(jtxtChar, BorderLayout.CENTER);
		jbtnUpdateChar = new JButton("Set Char"); jbtnUpdateChar.setActionCommand(Globals.CMD_UPDATE); jbtnUpdateChar.addActionListener(this); jpnlCharTool.add(jbtnUpdateChar, BorderLayout.EAST);
		gbci.gridx = 1; gbci.gridy = 6; gbci.gridheight = 1; gbci.gridwidth = 6; gbci.fill = GridBagConstraints.BOTH; gbci.weightx = 1; gbci.weighty = 1;
		jpnlChTools.add(jpnlCharTool, gbci);

		// Create Color Dock
		JPanel jpnlColorDock = getPanel(new GridLayout(2, 8, 0, 0));
		for(int cd = 0; cd < 16; cd++)
		{
			DualClickButton dbtnColorButton  = getPaletteButton(Globals.CMD_CLRFORE + cd, Globals.CMD_CLRBACK + cd, TIGlobals.TI_PALETTE_OPAQUE[cd]);
			jpnlColorDock.add(dbtnColorButton);
		}

		gbci.gridx = 1; gbci.gridy = 7; gbci.gridheight = 1; gbci.gridwidth = 6; gbci.fill = GridBagConstraints.BOTH; gbci.weightx = 1; gbci.weighty = 1;
		jpnlChTools.add(jpnlColorDock, gbci);

		// Create Character Dock Buttons
		hmCharGrids = new HashMap<String,int[][]>();
		hmCharImages = new HashMap<String,Image>();
		jbtnChar = new JButton[(TIGlobals.MAXCHAR - TIGlobals.MINCHAR) + 1];
		int charNum = 0;
		int rowNum = 0;
		for(int ch = TIGlobals.MINCHAR; ch <= TIGlobals.MAXCHAR; ch++)
		{
			rowNum = (int)(Math.floor(ch / 8));
			clrSets[rowNum][Globals.INDEX_CLR_BACK] = 0;
			clrSets[rowNum][Globals.INDEX_CLR_FORE] = 1;
			jbtnChar[ch] = getDockButton(((ch >= TIGlobals.CHARMAPSTART) && (ch <= TIGlobals.CHARMAPEND) ? "" + TIGlobals.CHARMAP[ch - TIGlobals.CHARMAPSTART] : "?"), Globals.CMD_EDITCHR + ch, TIGlobals.TI_PALETTE_OPAQUE[clrSets[rowNum][Globals.INDEX_CLR_BACK]]);
			jbtnChar[ch].setForeground(TIGlobals.TI_COLOR_UNUSED);
			int[][] emtpyGrid = new int[8][8];
			for(int y = 0; y < emtpyGrid.length; y++)
			{
				for(int x = 0; x < emtpyGrid[y].length; x++)
				{
					if(ch >= TIGlobals.CHARMAPSTART && ch <= TIGlobals.CHARMAPEND)
					{
						emtpyGrid[y][x] = hmDefaultChars.get(getCharKey(ch))[y][x];
					}
					else
					{
						emtpyGrid[y][x] = 0;
					}
				}
			}
			hmCharGrids.put(getCharKey(ch), emtpyGrid);
		}

		// Create Character Dock
		buildCharacterDock();
		JPanel jpnlNochange = getPanel(new FlowLayout());
		jpnlNochange.add(jpnlFont);
		jsclFont = new JScrollPane(jpnlNochange, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		// Assemble Editor Panel
		jpnlEdit = getPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(4, 0, 0, 0);
		gbc.weightx = 0; gbc.weighty = 0;
		gbc.gridx = 1; gbc.gridy = 1;
		jpnlEdit.add(jpnlChTools, gbc);
		gbc.gridx = 1; gbc.gridy = 2;
		jpnlEdit.add(jpnlColorDock, gbc);
		gbc.gridx = 1; gbc.gridy = 3;
		gbc.weightx = 1; gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		jpnlEdit.add(jsclFont, gbc);

		jpnlMain.add(mapdMain, BorderLayout.CENTER);
		jpnlMain.add(jpnlEdit, BorderLayout.WEST);

		// Assemble the application
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(jpnlMain, BorderLayout.CENTER);
		this.getContentPane().add(jMenuBar, BorderLayout.NORTH);
		this.addWindowListener(this);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.pack();
		this.setVisible(true);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);

		// Initialise the border objects for button selection
		Globals.bordButtonNormal = jbtnChar[0].getBorder();

		activeChar = TIGlobals.CUSTOMCHAR;
		ActionEvent aeInit = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, Globals.CMD_EDITCHR + activeChar);
		this.actionPerformed(aeInit);

		gcChar.redrawCanvas();

		jpnlChTools.setMinimumSize(jpnlChTools.getPreferredSize());
		jpnlChTools.setPreferredSize(jpnlChTools.getPreferredSize());
		jpnlChTools.setSize(jpnlChTools.getPreferredSize());

		jpnlColorDock.setMinimumSize(jpnlColorDock.getPreferredSize());
		jpnlColorDock.setPreferredSize(jpnlColorDock.getPreferredSize());
		jpnlColorDock.setSize(jpnlColorDock.getPreferredSize());

		updateFontButtons();
		updateComponents();
	}

// Listeners -------------------------------------------------------------------------------/

	/* ActionListener methods */
	public void actionPerformed(ActionEvent ae)
	{
		try
		{
			String command = ae.getActionCommand();
			if(command.equals(Globals.CMD_EXIT))
			{
				exitApp(0);
			}
			else if(command.equals(Globals.CMD_CLEAR))
			{
				gcChar.clearGrid();
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_FILL))
			{
				gcChar.fillGrid();
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_GRID))
			{
				gcChar.toggleGrid();
				updateComponents();
			}
			else if(command.equals(Globals.CMD_LOOK))
			{
				mapdMain.setLookModeOn(!mapdMain.isLookModeOn());
				jbtnLook.setBackground((mapdMain.isLookModeOn() ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
			}
			else if(command.equals(Globals.CMD_CLONE))
			{
				mapdMain.toggleCloneMode();
				jbtnClone.setBackground((mapdMain.isCloneModeOn() ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
			}
			else if(command.equals(Globals.CMD_FLIPH))
			{
				gcChar.setGrid(Globals.flipGrid(gcChar.getGridData(),false));
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_FLIPV))
			{
				gcChar.setGrid(Globals.flipGrid(gcChar.getGridData(),true));
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_ROTATEL))
			{
				gcChar.setGrid(Globals.rotateGrid(gcChar.getGridData(),true));
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_ROTATER))
			{
				gcChar.setGrid(Globals.rotateGrid(gcChar.getGridData(),false));
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_INVERT))
			{
				gcChar.setGrid(Globals.invertGrid(gcChar.getGridData()));
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_SHIFTU))
			{
				gcChar.setGrid(shiftGrid(gcChar.getGridData(), 0, 1));
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_SHIFTD))
			{
				gcChar.setGrid(shiftGrid(gcChar.getGridData(), 0, -1));
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_SHIFTL))
			{
				gcChar.setGrid(shiftGrid(gcChar.getGridData(), 1, 0));
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_SHIFTR))
			{
				gcChar.setGrid(shiftGrid(gcChar.getGridData(), -1, 0));
				hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				updateFontButton(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_NEW))
			{
				int userResponse = confirmationAction(this, "Confirm New Project", "This will delete all current data.\n\rAre you sure?");
				if(userResponse == JOptionPane.YES_OPTION)
				{
					newProject();
				}
			}
			else if(command.equals(Globals.CMD_OPEN))
			{
				openDataFile();
			}
			else if(command.equals(Globals.CMD_SAVE))
			{
				saveDataFile();
			}
			else if(command.equals(Globals.CMD_APPEND))
			{
				appendDataFile();
			}
			else if(command.equals(CMD_LOADDEFS))
			{
				loadDefaultCharacters();
			}
			else if(command.equals(Globals.CMD_MPCIMGMN))
			{
				importCharImage(false);
			}
			else if(command.equals(Globals.CMD_MPCIMGCL))
			{
				importCharImage(true);
			}
			else if(command.equals(Globals.CMD_MPVDP))
			{
				importVramDump();
			}
			else if(command.equals(Globals.CMD_XPDATA))
			{
				exportDataFile(false);
			}
			else if(command.equals(Globals.CMD_XPEXEC))
			{
				exportDataFile(true);
			}
			else if(command.equals(Globals.CMD_XPASM))
			{
				exportAssemblerFile();
			}
			else if(command.equals(Globals.CMD_XPBIN))
			{
				exportBinaryFile();
			}
			else if(command.equals(Globals.CMD_XPCIMGMN))
			{
				exportCharImage(false);
			}
			else if(command.equals(Globals.CMD_XPCIMGCL))
			{
				exportCharImage(true);
			}
			else if(command.equals(Globals.CMD_XPMAPIMG))
			{
				exportMapImage();
			}
			else if(command.startsWith(Globals.CMD_EDITCHR))
			{
				activeChar = Integer.parseInt(command.substring(Globals.CMD_EDITCHR.length()));
				if(hmCharGrids.get(getCharKey(activeChar)) == null)
				{
					gcChar.clearGrid();
					hmCharGrids.put(getCharKey(activeChar), gcChar.getGridData());
				}
				gcChar.setGridData(hmCharGrids.get(getCharKey(activeChar)));
				int cset = (int)(Math.floor(activeChar / 8));
				gcChar.setColorBack(TIGlobals.TI_PALETTE_OPAQUE[clrSets[cset][Globals.INDEX_CLR_BACK]]);
				gcChar.setColorDraw(TIGlobals.TI_PALETTE_OPAQUE[clrSets[cset][Globals.INDEX_CLR_FORE]]);
				gcChar.redrawCanvas();
				mapdMain.setActiveChar(activeChar);
				updateComponents();
			}
			else if(command.equals(Globals.CMD_UPDATE))
			{
				if(jtxtChar.getText().length() > 16)
				{
					jtxtChar.setText(jtxtChar.getText().substring(0, 16));
				}
				else if(jtxtChar.getText().length() < 16)
				{
					jtxtChar.setText(jtxtChar.getText() + "0000000000000000");
					jtxtChar.setText(jtxtChar.getText().substring(0, 16));
				}
				hmCharGrids.put(getCharKey(activeChar), Globals.getByteIntArray(jtxtChar.getText(), 8));
				gcChar.setGrid(hmCharGrids.get(getCharKey(activeChar)));
				updateFontButton(activeChar);
			}
			else if(command.startsWith(Globals.CMD_CLRFORE))
			{
				int clrval = Integer.parseInt(command.substring(Globals.CMD_CLRFORE.length()));
				int cset = (int)(Math.floor(activeChar / 8));
				clrSets[cset][Globals.INDEX_CLR_FORE] = clrval;
				for(int c = 0; c < FONT_COLS; c++)
				{
					updateFontButton((cset * 8) + c);
				}
				updateFontButton(activeChar);
				gcChar.setColorDraw(TIGlobals.TI_PALETTE_OPAQUE[clrval]);
				gcChar.redrawCanvas();
			}
			else if(command.startsWith(Globals.CMD_CLRBACK))
			{
				int clrval = Integer.parseInt(command.substring(Globals.CMD_CLRBACK.length()));
				int cset = (int)(Math.floor(activeChar / 8));
				clrSets[cset][Globals.INDEX_CLR_BACK] = clrval;
				for(int c = 0; c < FONT_COLS; c++)
				{
					updateFontButton((cset * 8) + c);
				}
				updateFontButton(activeChar);
				gcChar.setColorBack(TIGlobals.TI_PALETTE_OPAQUE[clrval]);
				gcChar.redrawCanvas();
			}
			else if(command.equals(CMD_SWAPCHARS))
			{
				swapCharacters();
			}
			else if(command.equals(Globals.CMD_SHOWPOS))
			{
				mapdMain.toggleShowPosIndic();
			}
			else if(command.equals(Globals.CMD_BASEPOS))
			{
				mapdMain.toggleBasePosition();
			}            
			else if(command.equals(CMD_CHARTOOL))
			{
				jpnlCharacterEditor.setVisible(!jpnlCharacterEditor.isVisible());
			}
			else if(command.equals(CMD_MODESET))
			{
				advancedModeOn = !advancedModeOn;
				buildCharacterDock();
			}
			else if(command.equals(CMD_ABOUT))
			{
				informationAction(this, "About Magellan", "Magellan version " + VERSION_NUMBER + "\n\r©2010-2011 Howard Kistler/Dream Codex Retrogames\n\rwww.dreamcodex.com\n\r&\n\rretroclouds\n\rMagellan is free software");
			}
			mapdMain.redrawCanvas();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	/* WindowListener methods */
	public void windowClosing(WindowEvent we)
	{
		exitApp(0);
	}
	public void windowOpened(WindowEvent we)      { ; }
	public void windowClosed(WindowEvent we)      { ; }
	public void windowActivated(WindowEvent we)   { ; }
	public void windowDeactivated(WindowEvent we) { ; }
	public void windowIconified(WindowEvent we)   { ; }
	public void windowDeiconified(WindowEvent we) { ; }

	/* MouseListener methods */
	public void mousePressed(MouseEvent me)
	{
		if(mapdMain.isLookModeOn())
		{
			if(mapdMain.getLookChar() != MapCanvas.NOCHAR)
			{
				ActionEvent aeChar = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, Globals.CMD_EDITCHR + mapdMain.getLookChar());
				this.actionPerformed(aeChar);
				ActionEvent aeLook = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, Globals.CMD_LOOK);
				this.actionPerformed(aeLook);
			}
		}
		if(!mapdMain.getHotCell().equals(MapCanvas.PT_OFFGRID))
		{
			mapdMain.requestFocus();
		}
		updateComponents();
	}
	public void mouseReleased(MouseEvent me) {}
	public void mouseClicked(MouseEvent me) { updateComponents(); }
	public void mouseEntered(MouseEvent me) {}
	public void mouseExited(MouseEvent me) {}

	/* MouseMotionListener methods */
	public void mouseMoved(MouseEvent me)
	{
		if(mapdMain.isLookModeOn())
		{
			if(mapdMain.getLookChar() != MapCanvas.NOCHAR)
			{
				ActionEvent aeChar = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, Globals.CMD_EDITCHR + mapdMain.getLookChar());
				this.actionPerformed(aeChar);
				mapdMain.requestFocus();
			}
		}
		mapdMain.updateComponents();
	}
	public void mouseDragged(MouseEvent me)
	{
		if(mapdMain.isLookModeOn())
		{
			if(mapdMain.getLookChar() != MapCanvas.NOCHAR)
			{
				ActionEvent aeChar = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, Globals.CMD_EDITCHR + mapdMain.getLookChar());
				this.actionPerformed(aeChar);
				ActionEvent aeLook = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, Globals.CMD_LOOK);
				this.actionPerformed(aeLook);
			}
		}
		if(!mapdMain.getHotCell().equals(MapCanvas.PT_OFFGRID))
		{
			mapdMain.requestFocus();
		}
		updateComponents();
	}

	/* IconProvider methods */
	public Icon getDefaultIcon()
	{
		return getIconForObject(new Integer(activeChar));
	}
	public Icon getIconForObject(Object obj)
	{
		if(obj != null)
		{
			try
			{
				return jbtnChar[((Integer)obj).intValue()].getIcon();
			}
			catch(Exception e)
			{
				Image imgTemp = this.createImage(gcChar.getGridData().length, gcChar.getGridData()[0].length);
				if(imgTemp == null) { return (ImageIcon)null; }
				return new ImageIcon(imgTemp);
			}
		}
		Image imgTemp = this.createImage(gcChar.getGridData().length, gcChar.getGridData()[0].length);
		if(imgTemp == null) { return (ImageIcon)null; }
		return new ImageIcon(imgTemp);
	}

// Component Builder Methods ---------------------------------------------------------------/

	protected JPanel getPanel(LayoutManager layout)
	{
		JPanel jpnlRtn = new JPanel(layout);
		jpnlRtn.setOpaque(true);
		jpnlRtn.setBackground(Globals.CLR_COMPONENTBACK);
		return jpnlRtn;
	}

	protected Image getImage(String name)
	{
		return Toolkit.getDefaultToolkit().getImage(name);
	}

	protected ImageIcon getIcon(String name)
	{
		URL imageURL = getClass().getResource("images/icon_" + name + "_mono.png");
		return new ImageIcon(Toolkit.getDefaultToolkit().getImage(imageURL));
	}

	protected JButton getToolButton(String buttonkey, String tooltip, Color bgcolor)
	{
		JButton jbtnTool = new JButton(getIcon(buttonkey));
		jbtnTool.setActionCommand(buttonkey);
		jbtnTool.addActionListener(this);
		jbtnTool.setToolTipText(tooltip);
		jbtnTool.setMargin(new Insets(0, 0, 0, 0));
		jbtnTool.setBackground(bgcolor);
		jbtnTool.setPreferredSize(Globals.DM_TOOL);
		return jbtnTool;
	}

	protected JButton getToolButton(String buttonkey, String tooltip)
	{
		return getToolButton(buttonkey, tooltip, Globals.CLR_BUTTON_NORMAL);
	}

	protected JButton getDockButton(String buttonlabel, String actcmd, Color bgcolor)
	{
		JButton jbtnDock = new JButton(buttonlabel);
		jbtnDock.setActionCommand(actcmd);
		jbtnDock.addActionListener(this);
		jbtnDock.setOpaque(true);
		jbtnDock.setBackground(bgcolor);
		jbtnDock.setMargin(new Insets(0, 0, 0, 0));
		jbtnDock.setPreferredSize(Globals.DM_TOOL);
		return jbtnDock;
	}

	protected DualClickButton getPaletteButton(String forecmd, String backcmd, Color bgcolor)
	{
		DualClickButton dbtnPal = new DualClickButton("", forecmd, backcmd, (ActionListener)this);
		dbtnPal.addActionListener(this);
		dbtnPal.setOpaque(true);
		dbtnPal.setBackground(bgcolor);
		dbtnPal.setMargin(new Insets(0, 0, 0, 0));
		dbtnPal.setPreferredSize(Globals.DM_TOOL);
		return dbtnPal;
	}

	protected JLabel getLabel(String text, int align, Color clrback)
	{
		JLabel jlblRtn = new JLabel(text, align);
		jlblRtn.setOpaque(true);
		jlblRtn.setBackground(clrback);
		return jlblRtn;
	}

	protected JLabel getLabel(String text, int align)
	{
		return getLabel(text, align, Globals.CLR_COMPONENTBACK);
	}

	protected void buildCharacterDock()
	{
		int dockFontRows = (advancedModeOn ? ADV_FONT_ROWS : BAS_FONT_ROWS);
		if(jpnlFont != null)
		{
			jpnlFont.removeAll();
			jpnlFont.setLayout(new GridLayout(dockFontRows, FONT_COLS + 1));
		}
		else
		{
			jpnlFont = getPanel(new GridLayout(dockFontRows, FONT_COLS + 1));
		}
		int col = 8;
		int ccount = 1;
		int lcount = 1;
		int ucount = 1;
		for(int c = TIGlobals.MINCHAR; c <= TIGlobals.MAXCHAR; c++)
		{
			if(c < TIGlobals.FIRSTCHAR)
			{
				if(advancedModeOn)
				{
					if(col >= 8)
					{
						jpnlFont.add(getLabel("L" + lcount + " ", JLabel.RIGHT, CLR_CHARS_LOWER));
						lcount++;
						col = 0;
					}
					jpnlFont.add(jbtnChar[c]);
					col++;
				}
			}
			else if(c > TIGlobals.LASTCHAR)
			{
				if(advancedModeOn)
				{
					if(col >= 8)
					{
						jpnlFont.add(getLabel("U" + ucount + " ", JLabel.RIGHT, CLR_CHARS_UPPER));
						ucount++;
						col = 0;
					}
					jpnlFont.add(jbtnChar[c]);
					col++;
				}
			}
			else
			{
				if(col >= 8)
				{
					if(c > TIGlobals.FINALXBCHAR)
					{
						jpnlFont.add(getLabel(ccount + " ", JLabel.RIGHT, CLR_CHARS_BASE2));
					}
					else
					{
						jpnlFont.add(getLabel(ccount + " ", JLabel.RIGHT, CLR_CHARS_BASE1));
					}
					ccount++;
					col = 0;
				}
				jpnlFont.add(jbtnChar[c]);
				col++;
			}
		}
		jpnlFont.revalidate();
	}

// Data Transformation Methods -------------------------------------------------------------/

	protected int[][] shiftGrid(int[][] grid, int xshift, int yshift)
	{
		int y = (yshift < 0 ? grid.length - 1 : 0);
		while(y >= 0 && y <= (grid.length - 1))
		{
			int getY = y + yshift;
			int x = (xshift < 0 ? grid[y].length - 1 : 0);
			while(x >= 0 && x <= (grid[y].length - 1))
			{
				int getX = x + xshift;
				if(getY >= 0 && getY < grid.length && getX >= 0 && getX < grid[y].length)
				{
					grid[y][x] = grid[getY][getX];
				}
				else
				{
					grid[y][x] = 0;
				}
				x = x + (xshift < 0 ? -1 : 1);
			}
			y = y + (yshift < 0 ? -1 : 1);
		}
		return grid;
	}

	protected void swapCharacters()
	{
		CharacterSwapDialog swapper = new CharacterSwapDialog(this, (IconProvider)this, (advancedModeOn ? TIGlobals.MINCHAR : TIGlobals.FIRSTCHAR), (advancedModeOn ? TIGlobals.MAXCHAR : TIGlobals.LASTCHAR));
		if(swapper.isOkay())
		{
			// only process if both characters are not the same
			if(swapper.getBaseChar() != swapper.getSwapChar())
			{
				if(!swapper.getCharAction().equals(CharacterSwapDialog.CHAR_NOTHING))
				{
					for(int m = (swapper.doAllMaps() ? 0 : mapdMain.getCurrentMapId()); m < (swapper.doAllMaps() ? mapdMain.getMapCount() : mapdMain.getCurrentMapId() + 1); m++)
					{
						int[][] arrayToProc = mapdMain.getMapData(m);
						for(int y = 0; y < arrayToProc.length; y++)
						{
							for(int x = 0; x < arrayToProc[y].length; x++)
							{
								if(arrayToProc[y][x] == swapper.getBaseChar())
								{
									arrayToProc[y][x] = swapper.getSwapChar();
								}
								else if(swapper.getCharAction().equals(CharacterSwapDialog.CHAR_SWAP) && (arrayToProc[y][x] == swapper.getSwapChar()))
								{
									arrayToProc[y][x] = swapper.getBaseChar();
								}
							}
						}
					}
				}
				if(!swapper.getImageAction().equals(CharacterSwapDialog.IMAGE_NOTHING))
				{
					int[][] charGrid = hmCharGrids.get(getCharKey(swapper.getBaseChar()));
					hmCharGrids.put(getCharKey(swapper.getBaseChar()), hmCharGrids.get(getCharKey(swapper.getSwapChar())));
					if(swapper.getImageAction().equals(CharacterSwapDialog.IMAGE_SWAP))
					{
						hmCharGrids.put(getCharKey(swapper.getSwapChar()), charGrid);
						updateFontButton(swapper.getSwapChar());
					}
					updateFontButton(swapper.getBaseChar());

				}
			}
		}
	}

// File Handling Methods -------------------------------------------------------------------/

	protected void openDataFile()
	{
		int charNum = TIGlobals.MINCHAR;
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, FILEEXTS, "Map Data Files");
		if(whatFile != null)
		{
			newProject();
			mapDataFile = whatFile;
			this.setTitle(APPTITLE + " : " + mapDataFile.getName());
			charNum = readDataFile(mapDataFile);
		}
		for(int cn = 0; cn < jbtnChar.length; cn++)
		{
			jbtnChar[cn].setIcon((ImageIcon)null);
			jbtnChar[cn].setText((cn >= TIGlobals.CHARMAPSTART) && (cn < (TIGlobals.CHARMAPSTART + TIGlobals.CHARMAP.length)) ? "" + TIGlobals.CHARMAP[cn - TIGlobals.CHARMAPSTART] : "?");
			charNum++;
		}
		mapdMain.goToMap(0);
		updateFontButtons();
		updateFontButton(activeChar);
		updateComponents();
	}

	protected void saveDataFile()
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, FILEEXTS, "Map Data Files");
		if(whatFile != null)
		{
			if(!whatFile.getAbsolutePath().toLowerCase().endsWith("." + FILEEXT))
			{
				whatFile = new File(whatFile.getAbsolutePath() + "." + FILEEXT);
			}
			mapDataFile = whatFile;
			this.setTitle(APPTITLE + " : " + mapDataFile.getName());
			writeDataFile(mapDataFile);
		}
	}

	protected void appendDataFile()
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, FILEEXTS, "Map Data Files");
		if(whatFile != null)
		{
			readAppendDataFile(whatFile);
		}
		updateComponents();
	}

	protected void loadDefaultCharacters()
	{
		updateFontButtons();
		updateFontButton(activeChar);
		updateComponents();
	}

	protected void importCharImage(boolean isColor)
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, IMGEXTS, "Image Files", true);
		if(whatFile != null)
		{
			Image charImg = getImage(whatFile.getAbsolutePath());
			BufferedImage buffImg = new BufferedImage(8 * 8, 8 * 32, BufferedImage.TYPE_3BYTE_BGR);
			while(buffImg == null) { try { Thread.sleep(50); } catch(InterruptedException ie) {} }
			if(isColor)
			{
				Graphics2D g2d = ((Graphics2D)(buffImg.getGraphics()));
				g2d.setColor(TIGlobals.TI_COLOR_TRANSOPAQUE);
				g2d.fillRect(0, 0, 8 * 8, 8 * 32);
				g2d.setComposite(AlphaComposite.SrcOver);
				g2d.drawImage(charImg, 0, 0, this);
				g2d.setComposite(AlphaComposite.Src);
			}
			else
			{
				buffImg.getGraphics().drawImage(charImg, 0, 0, this);
			}
			if(isColor)
			{
				readCharImageColor(buffImg);
			}
			else
			{
				readCharImageMono(buffImg);
			}
		}
		updateComponents();
	}

	protected void importVramDump()
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.OPEN_DIALOG, VDPEXTS, "VRAM Dump Files", true);
		if(whatFile != null)
		{
			VramImportDialog importer = new VramImportDialog(this);
			if(importer.isOkay())
			{
				int charOffset  = importer.getCharDataOffset();
				int mapOffset   = importer.getMapDataOffset();
				int colorOffset = importer.getColorDataOffset();
				readVramDumpFile(whatFile, charOffset, mapOffset, colorOffset);
			}
		}
		updateComponents();
	}

	protected void exportDataFile(boolean withExecCode)
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, XBEXTS, "XB Data Files");
		if(whatFile != null)
		{
			boolean isExtensionAdded = false;
			for(int ex = 0; ex < XBEXTS.length; ex++)
			{
				if(whatFile.getAbsolutePath().toLowerCase().endsWith("." + XBEXTS[ex]))
				{
					isExtensionAdded = true;
				}
			}
			if(!isExtensionAdded)
			{
				whatFile = new File(whatFile.getAbsolutePath() + "." + XBEXT);
			}
			MagellanExportDialog exporter = new MagellanExportDialog(MagellanExportDialog.TYPE_BASIC, this, bExportComments, defStartChar, defEndChar, TIGlobals.FIRSTCHAR, (withExecCode ? TIGlobals.FINALXBCHAR : TIGlobals.LASTCHAR));
			if(exporter.isOkay())
			{
				int sChar = exporter.getStartChar();
				int eChar = exporter.getEndChar();
				int aLine = exporter.getCodeLineStart();
				int cLine = exporter.getCharLineStart();
				int mLine = exporter.getMapLineStart();
				int iLine = exporter.getLineInterval();
				bExportComments = exporter.includeComments();
				boolean bCurrentMapOnly = exporter.currentMapOnly();
				defStartChar = Math.min(sChar, eChar);
				defEndChar = Math.max(sChar, eChar);
				writeXBDataFile(whatFile, defStartChar, defEndChar, aLine, cLine, mLine, iLine, withExecCode, bExportComments, bCurrentMapOnly);
			}
			exporter.dispose();
		}
	}

	protected void exportAssemblerFile()
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, ASMEXTS, "Assembler Source Files");
		if(whatFile != null)
		{
			boolean isExtensionAdded = false;
			for(int ex = 0; ex < ASMEXTS.length; ex++)
			{
				if(whatFile.getAbsolutePath().toLowerCase().endsWith("." + ASMEXTS[ex]))
				{
					isExtensionAdded = true;
				}
			}
			if(!isExtensionAdded)
			{
				whatFile = new File(whatFile.getAbsolutePath() + "." + ASMEXT);
			}
			MagellanExportDialog exporter = new MagellanExportDialog(MagellanExportDialog.TYPE_ASM, this, bExportComments, defStartChar, defEndChar, TIGlobals.MINCHAR, TIGlobals.MAXCHAR);
			if(exporter.isOkay())
			{
				int sChar = exporter.getStartChar();
				int eChar = exporter.getEndChar();
				bExportComments = exporter.includeComments();
				boolean bCurrentMapOnly = exporter.currentMapOnly();
				defStartChar = Math.min(sChar, eChar);
				defEndChar = Math.max(sChar, eChar);
				writeASMDataFile(whatFile, defStartChar, defEndChar, bExportComments, bCurrentMapOnly);
			}
			exporter.dispose();
		}
	}

	protected void exportBinaryFile()
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, BINEXTS, "Binary Data Files");
		if(whatFile != null)
		{
			boolean isExtensionAdded = false;
			for(int ex = 0; ex < BINEXTS.length; ex++)
			{
				if(whatFile.getAbsolutePath().toLowerCase().endsWith("." + BINEXTS[ex]))
				{
					isExtensionAdded = true;
				}
			}
			if(!isExtensionAdded)
			{
				whatFile = new File(whatFile.getAbsolutePath() + "." + BINEXT);
			}
			MagellanExportDialog exporter = new MagellanExportDialog(MagellanExportDialog.TYPE_BINARY, this, bExportComments, defStartChar, defEndChar, TIGlobals.MINCHAR, TIGlobals.MAXCHAR);
			if(exporter.isOkay())
			{
				int sChar = exporter.getStartChar();
				int eChar = exporter.getEndChar();
				boolean bIncludeColorsets = exporter.includeColorsets();
				boolean bIncludeChardata = exporter.includeChardata();
				boolean bIncludeSpritedata = exporter.includeSpritedata();
				byte chunkByte = (byte)(0 | (bIncludeColorsets ? BIN_CHUNK_COLORS : 0) | (bIncludeChardata ? BIN_CHUNK_CHARS : 0) | (bIncludeSpritedata ? BIN_CHUNK_SPRITES : 0));
				boolean bCurrentMapOnly = exporter.currentMapOnly();
				defStartChar = Math.min(sChar, eChar);
				defEndChar = Math.max(sChar, eChar);
				writeBinaryFile(whatFile, chunkByte, defStartChar, defEndChar, bCurrentMapOnly);
			}
			exporter.dispose();
		}
	}

	protected void exportCharImage(boolean isColor)
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, IMGEXTS, "Image Files");
		if(whatFile != null)
		{
			boolean isExtensionAdded = false;
			for(int ex = 0; ex < IMGEXTS.length; ex++)
			{
				if(whatFile.getAbsolutePath().toLowerCase().endsWith("." + IMGEXTS[ex]))
				{
					isExtensionAdded = true;
				}
			}
			if(!isExtensionAdded)
			{
				whatFile = new File(whatFile.getAbsolutePath() + "." + IMGEXT);
			}
			writeCharImage(whatFile, 8, isColor);
		}
	}

	protected void exportMapImage()
	{
		File whatFile = getFileFromChooser((mapDataFile != null ? mapDataFile.getAbsolutePath() : "."), JFileChooser.SAVE_DIALOG, IMGEXTS, "Image Files");
		if(whatFile != null)
		{
			boolean isExtensionAdded = false;
			for(int ex = 0; ex < IMGEXTS.length; ex++)
			{
				if(whatFile.getAbsolutePath().toLowerCase().endsWith("." + IMGEXTS[ex]))
				{
					isExtensionAdded = true;
				}
			}
			if(!isExtensionAdded)
			{
				whatFile = new File(whatFile.getAbsolutePath() + "." + IMGEXT);
			}
			writeMapImage(whatFile);
		}
	}

	private File getFileFromChooser(String startDir, int dialogType, String[] exts, String desc, boolean hasImagePreview)
	{
		JFileChooser jfileDialog = new JFileChooser(startDir);
		jfileDialog.setDialogType(dialogType);
		jfileDialog.setFileFilter(new MutableFilter(exts, desc));
		if(hasImagePreview)
		{
			jfileDialog.setAccessory(new ImagePreview(jfileDialog));
		}
		int optionSelected = JFileChooser.CANCEL_OPTION;
		if(dialogType == JFileChooser.OPEN_DIALOG)
		{
			optionSelected = jfileDialog.showOpenDialog(this);
		}
		else if(dialogType == JFileChooser.SAVE_DIALOG)
		{
			optionSelected = jfileDialog.showSaveDialog(this);
		}
		else // default to an OPEN_DIALOG
		{
			optionSelected = jfileDialog.showOpenDialog(this);
		}
		if(optionSelected == JFileChooser.APPROVE_OPTION)
		{
			return jfileDialog.getSelectedFile();
		}
		return (File)null;
	}

	private File getFileFromChooser(String startDir, int dialogType, String[] exts, String desc)
	{
		return getFileFromChooser(startDir, dialogType, exts, desc, false);
	}

	protected int readDataFile(File mapDataFile)
	{
		hmCharGrids.clear();
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(mapDataFile));
			String lineIn = "";
			int mapY = 0;
			int mapX = 0;
			int mapCount = 1;
			int mapColor = 15;
			int mapWidth = 32;
			int mapHeight = 24;
			int charStart = TIGlobals.FIRSTCHAR;
			int charEnd = TIGlobals.LASTCHAR;
			int charRead = charStart;
			int cset = (int)(Math.floor(charStart / 8));
			int currMap = 0;
			do
			{
				lineIn = br.readLine();
				if(lineIn == null)
				{
					break;
				}
				else
				{
					if(lineIn.startsWith(Globals.KEY_COLORSET))
					{
						lineIn = lineIn.substring(Globals.KEY_COLORSET.length());
						clrSets[cset][Globals.INDEX_CLR_FORE] = Integer.parseInt(lineIn.substring(0, lineIn.indexOf("|")));
						clrSets[cset][Globals.INDEX_CLR_BACK] = Integer.parseInt(lineIn.substring(lineIn.indexOf("|") + 1));
						cset++;
					}
					else if(lineIn.startsWith(Globals.KEY_CHARDATA))
					{
						lineIn = lineIn.substring(Globals.KEY_CHARDATA.length());

						if(lineIn.length() > 16)
						{
							lineIn = lineIn.substring(0, 16);
						}
						else if(lineIn.length() < 16)
						{
							lineIn = lineIn + "0000000000000000";
							lineIn = lineIn.substring(0, 16);
						}
						hmCharGrids.put(getCharKey(charRead), Globals.getByteIntArray(lineIn, 8));
						charRead++;
					}
					else if(lineIn.startsWith(Globals.KEY_CHARRANG))
					{
						lineIn = lineIn.substring(Globals.KEY_CHARRANG.length());
						charStart = Integer.parseInt(lineIn.substring(0, lineIn.indexOf("|")));
						charEnd = Integer.parseInt(lineIn.substring(lineIn.indexOf("|") + 1));
						charRead = charStart;
						cset = (int)(Math.floor(charStart / 8));
					}
					else if(lineIn.startsWith(Globals.KEY_SCRBACK))
					{
						lineIn = lineIn.substring(Globals.KEY_SCRBACK.length());
						mapColor = Integer.parseInt(lineIn);
						mapdMain.setColorScreen(mapColor);
					}
					else if(lineIn.startsWith(Globals.KEY_MAPCOUNT))
					{
						lineIn = lineIn.substring(Globals.KEY_MAPCOUNT.length());
						mapCount = Integer.parseInt(lineIn);
					}
					else if(lineIn.equals(Globals.KEY_MAPSTART))
					{
						if(mapY > 0)
						{
							mapdMain.addBlankMap(mapWidth, mapHeight);
							currMap++;
							mapdMain.setCurrentMapId(currMap);
							mapY = 0;
						}
					}
					else if(lineIn.equals(Globals.KEY_MAPEND))
					{
						mapdMain.setColorScreen(mapColor);
						mapdMain.storeCurrentMap();
					}
					else if(lineIn.startsWith(Globals.KEY_MAPSIZE))
					{
						lineIn = lineIn.substring(Globals.KEY_MAPSIZE.length());
						mapWidth = Integer.parseInt(lineIn.substring(0, lineIn.indexOf("|")));
						mapHeight = Integer.parseInt(lineIn.substring(lineIn.indexOf("|") + 1));
						mapdMain.setGridWidth(mapWidth);
						mapdMain.setGridHeight(mapHeight);
					}
					else if(lineIn.startsWith(Globals.KEY_MAPBACK))
					{
						lineIn = lineIn.substring(Globals.KEY_MAPBACK.length());
						mapColor = Integer.parseInt(lineIn);
						mapdMain.setColorScreen(mapColor);
					}
					else if(lineIn.startsWith(Globals.KEY_MAPDATA))
					{
						if(mapY >= mapHeight)
						{
							mapdMain.setColorScreen(mapColor);
							mapdMain.storeCurrentMap();
							mapdMain.addBlankMap(mapWidth, mapHeight);
							currMap++;
							mapdMain.setCurrentMapId(currMap);
							mapY = 0;
						}
						lineIn = lineIn.substring(Globals.KEY_MAPDATA.length());
						StringTokenizer stParse = new StringTokenizer(lineIn, "|", false);
						while(stParse.hasMoreTokens())
						{
							String sVal = stParse.nextToken();
							mapdMain.setGridAt(mapX, mapY, Integer.parseInt(sVal));
							mapX++;
						}
						mapX = 0;
						mapY++;
					}
				}
			} while (lineIn != null);
			br.close();
			return charStart;
		}
		catch(Exception e) { e.printStackTrace(System.out); }
		return TIGlobals.MINCHAR;
	}

	protected void readAppendDataFile(File mapDataFile)
	{
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(mapDataFile));
			String lineIn = "";
			int mapY = 0;
			int mapX = 0;
			int mapCount = 1;
			int mapColor = 15;
			int mapWidth = 32;
			int mapHeight = 24;
			int currMap = mapdMain.getMapCount();
			do
			{
				lineIn = br.readLine();
				if(lineIn == null)
				{
					break;
				}
				else
				{
					if(lineIn.startsWith(Globals.KEY_COLORSET))
					{
					}
					else if(lineIn.startsWith(Globals.KEY_CHARDATA))
					{
					}
					else if(lineIn.startsWith(Globals.KEY_SCRBACK))
					{
						lineIn = lineIn.substring(Globals.KEY_SCRBACK.length());
						mapColor = Integer.parseInt(lineIn);
					}
					else if(lineIn.startsWith(Globals.KEY_MAPCOUNT))
					{
						lineIn = lineIn.substring(Globals.KEY_MAPCOUNT.length());
						mapCount = Integer.parseInt(lineIn);
					}
					else if(lineIn.startsWith(Globals.KEY_MAPSIZE))
					{
						lineIn = lineIn.substring(Globals.KEY_MAPSIZE.length());
						mapWidth = Integer.parseInt(lineIn.substring(0, lineIn.indexOf("|")));
						mapHeight = Integer.parseInt(lineIn.substring(lineIn.indexOf("|") + 1));
					}
					else if(lineIn.equals(Globals.KEY_MAPSTART))
					{
						if(mapY > 0)
						{
							mapdMain.addBlankMap(mapWidth, mapHeight);
							currMap++;
							mapdMain.setCurrentMapId(currMap);
							mapY = 0;
						}
					}
					else if(lineIn.equals(Globals.KEY_MAPEND))
					{
						mapdMain.setColorScreen(mapColor);
						mapdMain.storeCurrentMap();
					}
					else if(lineIn.startsWith(Globals.KEY_MAPBACK))
					{
						lineIn = lineIn.substring(Globals.KEY_MAPBACK.length());
						mapColor = Integer.parseInt(lineIn);
						mapdMain.setColorScreen(mapColor);
					}
					else if(lineIn.startsWith(Globals.KEY_MAPDATA))
					{
						if(currMap == mapdMain.getMapCount())
						{
							mapdMain.storeCurrentMap();
							mapdMain.addBlankMap(mapWidth, mapHeight);
							mapdMain.setCurrentMapId(currMap);
							mapY = 0;
							mapdMain.setColorScreen(mapColor);
						}
						else if(mapY >= mapHeight)
						{
							mapdMain.storeCurrentMap();
							mapdMain.addBlankMap();
							currMap++;
							mapdMain.setCurrentMapId(currMap);
							mapY = 0;
							mapdMain.setColorScreen(mapColor);
						}
						lineIn = lineIn.substring(Globals.KEY_MAPDATA.length());
						StringTokenizer stParse = new StringTokenizer(lineIn, "|", false);
						while(stParse.hasMoreTokens())
						{
							String sVal = stParse.nextToken();
							mapdMain.setGridAt(mapX, mapY, Integer.parseInt(sVal));
							mapX++;
						}
						mapX = 0;
						mapY++;
					}
				}
			} while (lineIn != null);
			br.close();
			mapdMain.updateComponents();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	protected void readCharacterData(File mapDataFile)
	{
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(mapDataFile));
			String lineIn = "";
			int charStart = TIGlobals.FIRSTCHAR;
			int charEnd = TIGlobals.LASTCHAR;
			int charRead = charStart;
			do
			{
				lineIn = br.readLine();
				if(lineIn == null)
				{
					break;
				}
				else
				{
					if(lineIn.startsWith(Globals.KEY_CHARDATA))
					{
						lineIn = lineIn.substring(Globals.KEY_CHARDATA.length());
						hmCharGrids.put(getCharKey(charRead), Globals.getByteIntArray(lineIn, 8));
						charRead++;
					}
					else if(lineIn.startsWith(Globals.KEY_CHARRANG))
					{
						lineIn = lineIn.substring(Globals.KEY_CHARRANG.length());
						charStart = Integer.parseInt(lineIn.substring(0, lineIn.indexOf("|")));
						charEnd = Integer.parseInt(lineIn.substring(lineIn.indexOf("|") + 1));
						charRead = charStart;
					}
				}
			} while (lineIn != null);
			br.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	protected void readVramDumpFile(File vramDumpFile, int charTableOffset, int mapTableOffset, int colorTableOffset)
	{
		try
		{
			FileInputStream fib = new FileInputStream(vramDumpFile);
			int readPos = 0;
			int readInt;
			int charStart = TIGlobals.MINCHAR;
			int charEnd = TIGlobals.MAXCHAR;
			StringBuffer sbChar = new StringBuffer();
			int charByte = 0;
			int charRead = charStart;
			int charTableLength = 256;
			int colorTableLength = 32;
			int mapCols = 32;
			int mapRows = 24;
			int mapTableLength = (mapCols * mapRows);
			while((readInt = fib.read()) != -1)
			{
				if((readPos >= charTableOffset) && (readPos < (charTableOffset + (charTableLength * 8))))
				{
					if(charRead <= charEnd)
					{
						sbChar.append((readInt < 16 ? "0" : "") + Integer.toHexString(readInt));
						charByte++;
						if(charByte >= 8)
						{
							hmCharGrids.put(getCharKey(charRead), Globals.getByteIntArray(sbChar.toString(), 8));
							charRead++;
							charByte = 0;
							sbChar.delete(0, sbChar.length());
						}
					}
				}
				else if(readPos >= mapTableOffset && readPos < (mapTableOffset + mapTableLength))
				{
					int mapCell = readPos - mapTableOffset;
					int mapRow  = (int)(Math.floor(mapCell / mapCols));
					int mapCol  = mapCell % mapCols;
					mapdMain.setGridAt(mapCol, mapRow, readInt);
				}
				else if(readPos >= colorTableOffset && readPos < (colorTableOffset + colorTableLength))
				{
					int setNum = readPos - colorTableOffset;
					int colorFore = (readInt & 0x0000FFFF)>>4;
					int colorBack = (readInt & 0xFFFF0000);
					clrSets[setNum][Globals.INDEX_CLR_FORE] = colorFore;
					clrSets[setNum][Globals.INDEX_CLR_BACK] = colorBack;
				}
				readPos++;
			}
			fib.close();
			updateFontButtons();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	protected void writeDataFile(File mapDataFile)
	{
		try
		{
			// store working map first
			mapdMain.storeCurrentMap();
			// get file output buffer
			BufferedWriter bw = new BufferedWriter(new FileWriter(mapDataFile));
			// output overall character range (this is for backwards compatibility with earlier Magellan releases, will be phased out)
			bw.write("* CHARACTER RANGE"); bw.newLine();
			bw.write(Globals.KEY_CHARRANG + TIGlobals.MINCHAR + "|" +  + TIGlobals.MAXCHAR); bw.newLine();
			// save colorsets
			bw.write("* COLORSET"); bw.newLine();
			for(int i = 0; i < clrSets.length; i++)
			{
				bw.write(Globals.KEY_COLORSET + clrSets[i][Globals.INDEX_CLR_FORE] + "|" + clrSets[i][Globals.INDEX_CLR_BACK]);
				bw.newLine();
			}
			// save chardefs
			bw.write("* CHAR DEFS"); bw.newLine();
			for(int i = TIGlobals.MINCHAR; i <= TIGlobals.MAXCHAR; i++)
			{
				bw.write(Globals.KEY_CHARDATA);
				if(hmCharGrids.get(getCharKey(i)) != null)
				{
					String hexstr = Globals.getByteString(hmCharGrids.get(getCharKey(i)));
					bw.write(hexstr, 0, hexstr.length());
				}
				else
				{
					bw.write(Globals.BLANKCHAR, 0, Globals.BLANKCHAR.length());
				}
				bw.newLine();
			}
			// save map parameters
			bw.write("* MAPS"); bw.newLine();
			bw.write(Globals.KEY_MAPCOUNT + mapdMain.getMapCount());
			bw.newLine();
			// save map(s)
			for(int m = 0; m < mapdMain.getMapCount(); m++)
			{
				bw.write("* MAP #" + (m + 1)); bw.newLine();
				bw.write(Globals.KEY_MAPSTART);
				bw.newLine();
				int[][] mapToSave = mapdMain.getMapData(m);
				bw.write(Globals.KEY_MAPSIZE + mapToSave[0].length + "|" + mapToSave.length);
				bw.newLine();
				bw.write(Globals.KEY_MAPBACK + mapdMain.getScreenColor(m));
				bw.newLine();
				for(int y = 0; y < mapToSave.length; y++)
				{
					bw.write(Globals.KEY_MAPDATA);
					for(int x = 0; x < mapToSave[y].length; x++)
					{
						bw.write((x > 0 ? "|" : "") + mapToSave[y][x]);
					}
					bw.newLine();
				}
				bw.write(Globals.KEY_MAPEND);
				bw.newLine();
			}
			bw.flush();
			bw.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
		updateComponents();
	}

	protected void writeXBDataFile(File mapDataFile, int startChar, int endChar, int codeLine, int charLine, int mapLine, int interLine, boolean addExecutableCode, boolean includeComments, boolean currMapOnly)
	{
		int currLine = codeLine;
		int itemCount = 0;
		int colorDataStart = 0;
		int colorSetStart = (int)(Math.floor(startChar / 8));
		int colorSetEnd = (int)(Math.floor(endChar / 8));
		int colorSetNum = (int)(Math.floor((startChar - TIGlobals.FIRSTCHAR) / 8)) + 1;
		int colorCount = (colorSetEnd - colorSetStart) + 1;
		int charDataStart = 0;
		int charCount = 0;
		int[] mapDataStart = new int[mapdMain.getMapCount()];
		int mapCols = 0;
		int mapRows = 0;
		StringBuffer sbOutLine = new StringBuffer();
		try
		{
			mapdMain.storeCurrentMap();
			BufferedWriter bw = new BufferedWriter(new FileWriter(mapDataFile));
			colorDataStart = currLine;
			if(includeComments)
			{
				bw.write("REM COLORSET DECLARATIONS");
				bw.newLine();
				bw.newLine();
			}
			int blockCount = 0;
			bw.write(currLine + " DATA ");
			for(int i = colorSetStart; i < (colorSetStart + colorCount); i++)
			{
				if(blockCount > 7)
				{
					bw.newLine();
					currLine = currLine + interLine;
					bw.write(currLine + " DATA ");
					blockCount = 0;
				}
				if(blockCount > 0) { bw.write(","); }
				bw.write(colorSetNum + "," + (clrSets[i][Globals.INDEX_CLR_FORE] + 1) + "," + (clrSets[i][Globals.INDEX_CLR_BACK] + 1));
				colorSetNum++;
				blockCount++;
			}
			bw.newLine();
			currLine = currLine + interLine;
			if(includeComments)
			{
				bw.newLine();
				bw.write("REM CHARACTER DATA");
				bw.newLine();
				bw.newLine();
			}
			currLine = charLine;
			charDataStart = currLine;
			for(int i = startChar; i <= endChar; i++)
			{
				String hexstr;
				if(hmCharGrids.get(getCharKey(i)) != null)
				{
					hexstr = Globals.getByteString(hmCharGrids.get(getCharKey(i)));
				}
				else
				{
					hexstr = Globals.BLANKCHAR;
				}
				if(!hexstr.equals(Globals.BLANKCHAR))
				{
					if(itemCount == 0) { sbOutLine.append(currLine + " DATA "); }
					else { sbOutLine.append(","); }
					sbOutLine.append(i + "," + '"' + hexstr.toUpperCase() + '"');
					itemCount++;
					if(itemCount >= 4)
					{
						bw.write(sbOutLine.toString());
						bw.newLine();
						sbOutLine.delete(0, sbOutLine.length());
						currLine = currLine + interLine;
						itemCount = 0;
					}
					charCount++;
				}
			}
			if(sbOutLine.length() > 0) { bw.write(sbOutLine.toString()); bw.newLine(); }
			currLine = currLine + interLine;
			itemCount = 0;
			sbOutLine.delete(0, sbOutLine.length());
			currLine = mapLine;
			if(includeComments)
			{
				bw.newLine();
				bw.write("REM MAP DATA");
				bw.newLine();
			}
			int mapTicker = 1;
			for(int m = 0; m < mapdMain.getMapCount(); m++)
			{
				if(!currMapOnly || m == mapdMain.getCurrentMapId())
				{
					int[][] mapToSave = mapdMain.getMapData(m);
					mapCols = mapToSave[0].length;
					mapRows = mapToSave.length;
					mapDataStart[m] = currLine;
					if(includeComments)
					{
						bw.newLine();
						bw.write("REM MAP #" + mapTicker);
						bw.newLine();
					}
					int mapColor = mapdMain.getScreenColorTI(m);
					if(includeComments)
					{
						bw.write("REM MAP #" + mapTicker + " WIDTH, HEIGHT, SCREEN COLOR");
						bw.newLine();
					}
					bw.write(currLine + " DATA " + mapCols + "," + mapRows + "," + mapColor);
					bw.newLine();
					if(includeComments)
					{
						bw.write("REM MAP #" + mapTicker + " DATA");
						bw.newLine();
					}
					currLine = currLine + interLine;
					int mapTileVal = TIGlobals.SPACECHAR;
					for(int y = 0; y < mapRows; y++)
					{
						for(int x = 0; x < mapCols; x++)
						{
							if(itemCount == 0) { sbOutLine.append(currLine + " DATA "); }
							else { sbOutLine.append(","); }
							mapTileVal = ((mapToSave[y][x] < startChar || mapToSave[y][x] > endChar) ? TIGlobals.SPACECHAR : mapToSave[y][x]);
							sbOutLine.append("" + mapTileVal);
							itemCount++;
							if(itemCount >= 16)
							{
								bw.write(sbOutLine.toString());
								bw.newLine();
								sbOutLine.delete(0, sbOutLine.length());
								currLine = currLine + interLine;
								itemCount = 0;
							}
						}
					}
					mapTicker++;
				}
			}
			if(addExecutableCode)
			{
				if(includeComments)
				{
					bw.newLine();
					bw.write("REM LOAD COLORSET");
					bw.newLine();
					bw.newLine();
				}
				bw.write(currLine + " RESTORE " + colorDataStart + "::FOR C=1 TO " + colorCount + "::READ CS,CF,CB::CALL COLOR(CS,CF,CB)::NEXT C");
				currLine = currLine + interLine;
				bw.newLine();
				if(includeComments)
				{
					bw.newLine();
					bw.write("REM LOAD CHARACTERS");
					bw.newLine();
					bw.newLine();
				}
				bw.write(currLine + " RESTORE " + charDataStart + "::FOR C=1 TO " + charCount + "::READ CN,CC$::CALL CHAR(CN,CC$)::NEXT C");
				currLine = currLine + interLine;
				bw.newLine();
				if(includeComments)
				{
					bw.newLine();
					bw.write("REM DRAW MAP(S)");
					bw.newLine();
					bw.newLine();
				}
				for(int mp = 0; mp < mapdMain.getMapCount(); mp++)
				{
					if(!currMapOnly || mp == mapdMain.getCurrentMapId())
					{
						bw.write(currLine + " CALL CLEAR");
						currLine = currLine + interLine;
						bw.newLine();
						bw.write(currLine + " RESTORE " + mapDataStart[mp]);
						currLine = currLine + interLine;
						bw.newLine();
						bw.write(currLine + " READ W,H,SC::CALL SCREEN(SC)::CALL CLEAR");
						currLine = currLine + interLine;
						bw.newLine();
						bw.write(currLine + " FOR Y=1 TO H");
						currLine = currLine + interLine;
						bw.newLine();
						bw.write(currLine + " FOR X=1 TO W");
						currLine = currLine + interLine;
						bw.newLine();
						bw.write(currLine + " READ CP::CALL VCHAR(Y,X,CP)");
						currLine = currLine + interLine;
						bw.newLine();
						bw.write(currLine + " NEXT X");
						currLine = currLine + interLine;
						bw.newLine();
						bw.write(currLine + " NEXT Y");
						currLine = currLine + interLine;
						bw.newLine();
						bw.write(currLine + " CALL KEY(0,K,S)::IF S=0 THEN " + currLine);
						currLine = currLine + interLine;
						bw.newLine();
					}
				}
				bw.write(currLine + " END");
				currLine = currLine + interLine;
				bw.newLine();
			}
			bw.flush();
			bw.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	protected void writeASMDataFile(File mapDataFile, int startChar, int endChar, boolean includeComments, boolean currMapOnly)
	{
		int itemCount = 0;
		StringBuffer sbLine = new StringBuffer();
		int colorDataStart = 0;
		int charDataStart = 0;
		int charCount = 0;
		int mapDataStart = 0;
		int mapCols = 0;
		int mapRows = 0;
		StringBuffer sbOutLine = new StringBuffer();
		try
		{
			mapdMain.storeCurrentMap();
			BufferedWriter bw = new BufferedWriter(new FileWriter(mapDataFile));
			if(includeComments)
			{
				printPaddedLine(bw, "****************************************", false);
				printPaddedLine(bw, "* Colorset Definitions", false);
				printPaddedLine(bw, "****************************************", false);
			}
			printPaddedLine(bw, "CLRNUM DATA " + (clrSets.length - 1), includeComments);
			for(int i = 0; i < clrSets.length; i++)
			{
				if(itemCount == 0)
				{
					if(i == 0)
					{
						sbLine.append("CLRSET BYTE ");
					}
					else
					{
						sbLine.append("       BYTE ");
					}
				}
				if(itemCount > 0) { sbLine.append(","); }
				sbLine.append(">");
				sbLine.append(Integer.toHexString(clrSets[i][Globals.INDEX_CLR_FORE]).toUpperCase());
				sbLine.append(Integer.toHexString(clrSets[i][Globals.INDEX_CLR_BACK]).toUpperCase());
				itemCount++;
				if(itemCount > 3)
				{
					printPaddedLine(bw, sbLine.toString(), includeComments);
					sbLine.delete(0, sbLine.length());
					itemCount = 0;
				}
			}
			if(sbLine.length() > 0)
			{
				printPaddedLine(bw, sbLine.toString(), includeComments);
				sbLine.delete(0, sbLine.length());
			}
			if(includeComments)
			{
				printPaddedLine(bw, "****************************************", false);
				printPaddedLine(bw, "* Character Patterns", false);
				printPaddedLine(bw, "****************************************", false);
			}
			sbLine.delete(0, sbLine.length());
			itemCount = 0;
			for(int i = startChar; i <= endChar; i++)
			{
				String hexstr = Globals.BLANKCHAR;
				if(hmCharGrids.get(getCharKey(i)) != null)
				{
					hexstr = Globals.getByteString(hmCharGrids.get(getCharKey(i))).toUpperCase();
				}
				if(!hexstr.equals(Globals.BLANKCHAR) && hexstr.length() >= 16)
				{
					printPaddedLine(bw, "PCH" + itemCount + (itemCount < 10 ? "  " : (itemCount < 100 ? " " : "")) + " DATA " + i, includeComments);
					sbLine.append("PAT" + itemCount + (itemCount < 10 ? "  " : (itemCount < 100 ? " " : "")) + " DATA ");
					sbLine.append(">" + hexstr.substring(0, 4) + ",");
					sbLine.append(">" + hexstr.substring(4, 8) + ",");
					sbLine.append(">" + hexstr.substring(8, 12) + ",");
					sbLine.append(">" + hexstr.substring(12, 16));
					printPaddedLine(bw, sbLine.toString(), includeComments);
					sbLine.delete(0, sbLine.length());
					itemCount++;
				}
			}
			if(includeComments)
			{
				printPaddedLine(bw, "****************************************", false);
				printPaddedLine(bw, "* Map Data", false);
				printPaddedLine(bw, "****************************************", false);
			}
			printPaddedLine(bw, "MCOUNT DATA " + (currMapOnly ? 1 : mapdMain.getMapCount()), includeComments);
			sbLine.delete(0, sbLine.length());
			boolean isFirstByte = true;
			String hexChunk = new String();
			for(int m = 0; m < mapdMain.getMapCount(); m++)
			{
				if(!currMapOnly || m == mapdMain.getCurrentMapId())
				{
					int[][] mapToSave = mapdMain.getMapData(m);
					if(includeComments)
					{
						printPaddedLine(bw, "* == Map #" + m + " == ", false);
					}
					printPaddedLine(bw, "MC" + m + (m < 10 ? "   " : (m < 100 ? "  " : (m < 1000 ? " " : ""))) + " DATA " + mapdMain.getScreenColor(m), includeComments);
					sbLine.append("MS" + m + (m < 10 ? "   " : (m < 100 ? "  " : (m < 1000 ? " " : ""))) + " DATA >");
					hexChunk = Integer.toHexString(mapToSave[0].length).toUpperCase();
					sbLine.append((hexChunk.length() < 1 ? "00" : (hexChunk.length() < 2 ? "0" : "")) + hexChunk);
					hexChunk = Integer.toHexString(mapToSave.length).toUpperCase();
					sbLine.append((hexChunk.length() < 1 ? "00" : (hexChunk.length() < 2 ? "0" : "")) + hexChunk);
					printPaddedLine(bw, sbLine.toString(), includeComments);
					sbLine.delete(0, sbLine.length());
					isFirstByte = true;
					for(int y = 0; y < mapToSave.length; y++)
					{
						if(includeComments)
						{
							printPaddedLine(bw, "* -- Map Row " + y + " -- ", false);
						}
						for(int cl = 0; cl < Math.ceil(mapToSave[y].length / 8); cl++)
						{
							if(y == 0 && cl == 0)
							{
								sbLine.append("MD" + m + (m < 10 ? "   " : (m < 100 ? "  " : (m < 1000 ? " " : ""))) + " DATA ");
							}
							else
							{
								sbLine.append("       DATA ");
							}
							for(int colpos = (cl * 8); colpos < Math.min((cl + 1) * 8, mapToSave[y].length); colpos++)
							{
								if(isFirstByte)
								{
									if(colpos > (cl * 8))
									{
										sbLine.append(",");
									}
									sbLine.append(">");
								}
								hexChunk = Integer.toHexString(mapToSave[y][colpos]).toUpperCase();
								if(mapToSave[y][colpos] == MapCanvas.NOCHAR) { hexChunk = "00"; }
								sbLine.append((hexChunk.length() < 1 ? "00" : (hexChunk.length() < 2 ? "0" : "")) + hexChunk);
								isFirstByte = !isFirstByte;
							}
							printPaddedLine(bw, sbLine.toString(), includeComments);
							sbLine.delete(0, sbLine.length());
						}
					}
				}
			}
			bw.flush();
			bw.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
	}

	protected void writeBinaryFile(File mapDataFile, byte chunkFlags, int startChar, int endChar, boolean currMapOnly)
	{
		try
		{
			// store working map first
			mapdMain.storeCurrentMap();
			// get file output buffer
			FileOutputStream fos = new FileOutputStream(mapDataFile);

			// write File Header
			fos.write(BIN_HEADER_MAG);
			fos.write(BIN_HEADER_VER);
			fos.write(chunkFlags);
			byte mapCount = (byte)(currMapOnly ? 1 : mapdMain.getMapCount());
			fos.write(mapCount);

			// write Colorset Chunk (if present)
			if((chunkFlags & BIN_CHUNK_COLORS) == BIN_CHUNK_COLORS)
			{
				for(int i = 0; i < clrSets.length; i++)
				{
					fos.write((byte)(clrSets[i][Globals.INDEX_CLR_FORE]<<4 | clrSets[i][Globals.INDEX_CLR_BACK]));
				}
			}

			// write Character Chunk (if present)
			if((chunkFlags & BIN_CHUNK_CHARS) == BIN_CHUNK_CHARS)
			{
				byte charCount = (byte)((endChar - startChar) + 1);
				fos.write(charCount);
				for(int bc = startChar; bc <= endChar; bc++)
				{
					fos.write(getCharBytes(bc));
				}
			}

			// write Maps
			for(int m = 0; m < mapdMain.getMapCount(); m++)
			{
				if(!currMapOnly || m == mapdMain.getCurrentMapId())
				{
					int[][] mapToSave = mapdMain.getMapData(m);
					int mapCols = mapToSave[0].length;
					int mapRows = mapToSave.length;
					int mapSize = mapCols * mapRows;
					int mapScreenColor = mapdMain.getScreenColor(m);

					// write Map Header
					//   reserved bytes for Magellan use
					fos.write(BIN_MAP_HEADER_RESERVED1);
					fos.write(BIN_MAP_HEADER_RESERVED2);
					fos.write(BIN_MAP_HEADER_RESERVED3);
					fos.write(BIN_MAP_HEADER_RESERVED4);
					//   map size as a series of bytes
					int mapSizeChunk = mapSize;
					for(int i = 0; i < 8; i++)
					{
						if(mapSizeChunk > 255)
						{
							fos.write(255);
							mapSizeChunk -= 255;
						}
						else if(mapSizeChunk > 0)
						{
							fos.write((byte)mapSizeChunk);
							mapSizeChunk = 0;
						}
						else
						{
							fos.write(0);
						}
					}
					//   map columns as a byte pair
					if(mapCols > 255)
					{
						fos.write(255);
						fos.write(mapCols - 255);
					}
					else
					{
						fos.write(mapCols);
						fos.write(0);
					}
					//   map rows as a byte pair
					if(mapRows > 255)
					{
						fos.write(255);
						fos.write(mapRows - 255);
					}
					else
					{
						fos.write(mapRows);
						fos.write(0);
					}
					//   map screen color
					fos.write(mapScreenColor);

					// write Map Data
					for(int y = 0; y < mapRows; y++)
					{
						for(int x = 0; x < mapCols; x++)
						{
							fos.write(mapToSave[y][x]);
						}
					}

					// write Sprite Chunk (if present)
					if((chunkFlags & BIN_CHUNK_SPRITES) == BIN_CHUNK_SPRITES)
					{
						// not yet implemented
					}
				}
			}

			fos.flush();
			fos.close();
		}
		catch(Exception e) { e.printStackTrace(System.out); }
		updateComponents();
	}

	protected void printPaddedLine(BufferedWriter bw, String str, boolean isCommand, int padlen)
	throws IOException
	{
		if(str.length() < padlen)
		{
			StringBuffer sbOut = new StringBuffer();
			sbOut.append(str);
			for(int i = str.length(); i < padlen; i++)
			{
				if((i == (padlen - 1)) && isCommand)
				{
					sbOut.append(";");
				}
				else
				{
					sbOut.append(" ");
				}
			}
			bw.write(sbOut.toString());
		}
		else
		{
			bw.write(str);
		}
		bw.newLine();
	}

	protected void printPaddedLine(BufferedWriter bw, String str, boolean isCommand)
	throws IOException
	{
		printPaddedLine(bw, str, isCommand, ASM_LINELEN);
	}

	protected void printPaddedLine(BufferedWriter bw, String str)
	throws IOException
	{
		printPaddedLine(bw, str, true);
	}

	protected void readCharImageMono(BufferedImage buffImg)
	{
		// get character glyphs
		int[][] charArray;
		int rowOffset = 0;
		int colOffset = 0;
		for(int charNum = TIGlobals.MINCHAR; charNum <= TIGlobals.MAXCHAR; charNum++)
		{
			int[][] newCharArray = new int[8][8];
			if(hmCharGrids.containsKey(getCharKey(charNum)))
			{
				hmCharGrids.remove(getCharKey(charNum));
			}
			for(int y = 0; y < 8; y++)
			{
				for(int x = 0; x < 8; x++)
				{
					newCharArray[y][x] = (buffImg.getRGB((colOffset * 8) + x, (rowOffset * 8) + y) != -1 ? 1 : 0);
				}
			}
			hmCharGrids.put(getCharKey(charNum), newCharArray);
			updateFontButton(charNum);
			colOffset++;
			if(colOffset >= 8)
			{
				colOffset = 0;
				rowOffset++;
			}
		}
	}

	protected void readCharImageColor(BufferedImage buffImg)
	{
		// get colorsets
		for(int cs = 0; cs < 32; cs++)
		{
			int colorFore = -1;
			int colorBack = -1;
			int testY = 0;
			int testX = 0;
			while(testY < 8 && (colorFore == -1 || colorBack == -1))
			{
				int pixelColor = buffImg.getRGB(testX, (cs * 8) + testY);
				int tiColor = getTIColorForPixel(pixelColor);
				if((tiColor > 0) && (colorFore == -1)) { colorFore = tiColor; }
				else if((tiColor > 0) && (tiColor != colorFore)) { colorBack = tiColor; }
				testX++;
				if(testX >= 64)
				{
					testX = 0;
					testY++;
				}
			}
			if(colorFore == -1) { colorFore = 1; }
			if(colorBack == -1) { colorBack = 0; }
			clrSets[cs][Globals.INDEX_CLR_FORE] = colorFore;
			clrSets[cs][Globals.INDEX_CLR_BACK] = colorBack;
		}
		// get character glyphs
		int rowOffset = 0;
		int colOffset = 0;
		int[][] charArray;
		int cSet = 0;
		for(int charNum = TIGlobals.MINCHAR; charNum <= TIGlobals.MAXCHAR; charNum++)
		{
			int[][] newCharArray = new int[8][8];
			if(hmCharGrids.containsKey(getCharKey(charNum)))
			{
				hmCharGrids.remove(getCharKey(charNum));
			}
			for(int y = 0; y < 8; y++)
			{
				for(int x = 0; x < 8; x++)
				{
					newCharArray[y][x] = (buffImg.getRGB((colOffset * 8) + x, (rowOffset * 8) + y) == TIGlobals.TI_PALETTE_OPAQUE[clrSets[cSet][Globals.INDEX_CLR_FORE]].getRGB() ? 1 : 0);
				}
			}
			hmCharGrids.put(getCharKey(charNum), newCharArray);
			updateFontButton(charNum);
			colOffset++;
			if(colOffset >= 8)
			{
				colOffset = 0;
				rowOffset++;
				cSet++;
			}
		}
	}

	protected void writeCharImage(File imageOut, int tileCols, boolean isColor)
	{
		int charCount = hmCharGrids.size();
		int tileRows = (int)Math.ceil(charCount / tileCols);
		BufferedImage bufferCharImage = new BufferedImage(tileCols * 8, tileRows * 8, BufferedImage.TYPE_INT_ARGB);
		if(bufferCharImage == null)
		{
			bufferCharImage = new BufferedImage(tileCols * 8, tileRows * 8, BufferedImage.TYPE_INT_ARGB);
		}
		try { Thread.sleep(500); } catch(InterruptedException ie) { System.out.println(ie.getMessage()); }
		if(bufferCharImage == null)
		{
			System.out.println("Unable to initialize BufferedImage");
		}
		else
		{
			int[][] charGrid;
			int drawRow = 0;
			int drawCol = 0;
			int setNum = 0;
			boolean shouldDraw = true;
			try
			{
				Graphics2D gf = (Graphics2D)(bufferCharImage.getGraphics());
				if(isColor)
				{
					gf.setComposite(AlphaComposite.Clear);
					gf.setColor(TIGlobals.TI_COLOR_TRANSPARENT);
				}
				else
				{
					gf.setComposite(AlphaComposite.SrcOver);
					gf.setColor(TIGlobals.TI_COLOR_WHITE);
				}
				gf.fillRect(0, 0, bufferCharImage.getWidth(), bufferCharImage.getHeight());
				gf.setColor(TIGlobals.TI_COLOR_BLACK);
				for(int ch = 0; ch < charCount; ch++)
				{
					gf.setComposite(AlphaComposite.SrcOver);
					if(hmCharGrids.containsKey(getCharKey(ch)))
					{
						charGrid = hmCharGrids.get(getCharKey(ch));
						for(int y = 0; y < charGrid.length; y++)
						{
							for(int x = 0; x < charGrid[y].length; x++)
							{
								shouldDraw = true;
								if(charGrid[y][x] == 1)
								{
									if(isColor)
									{
										if(clrSets[setNum][Globals.INDEX_CLR_FORE] == 0) { shouldDraw = false; }
										gf.setColor(TIGlobals.TI_PALETTE[clrSets[setNum][Globals.INDEX_CLR_FORE]]);
									}
									else
									{
										gf.setColor(TIGlobals.TI_COLOR_BLACK);
									}
								}
								else
								{
									if(isColor)
									{
										if(clrSets[setNum][Globals.INDEX_CLR_BACK] == 0) { shouldDraw = false; }
										gf.setColor(TIGlobals.TI_PALETTE[clrSets[setNum][Globals.INDEX_CLR_BACK]]);
									}
									else
									{
										gf.setColor(TIGlobals.TI_COLOR_WHITE);
									}
								}
								if(shouldDraw)
								{
									gf.fillRect((drawCol * 8) + x, (drawRow * 8) + y, 1, 1);
								}
							}
						}
					}
					drawCol++;
					if(drawCol >= tileCols)
					{
						drawCol = 0;
						drawRow++;
						setNum++;
					}
				}
				gf.dispose();
				ImageIO.write(bufferCharImage, "png", imageOut);
			}
			catch(Exception e)
			{
				e.printStackTrace(System.out);
			}
		}
	}

	protected void writeMapImage(File imageOut)
	{
		try
		{
			updateComponents();
			ImageIO.write(mapdMain.getBuffer(), "png", imageOut);
		}
		catch(Exception e)
		{
			e.printStackTrace(System.out);
		}
	}

	private void savePreferences()
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(new File("Magellan.prefs"));
			appProperties.setProperty("magnif", "" + mapdMain.getViewScale());
			appProperties.setProperty("textCursor", (mapdMain.showTypeCell() ? "true" : "false"));
			appProperties.setProperty("exportComments", (bExportComments ? "true" : "false"));
			appProperties.setProperty("expandCharacters", (advancedModeOn ? "true" : "false"));
			appProperties.setProperty("showGrid", (mapdMain.isShowGrid() ? "true" : "false"));
			appProperties.setProperty("showPosition", (mapdMain.showPosIndic() ? "true" : "false"));
			appProperties.setProperty("defStartChar", "" + defStartChar);
			appProperties.setProperty("defEndChar", "" + defEndChar);
			appProperties.store(fos, (String)null);
			fos.flush();
			fos.close();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace(System.out);
		}
	}

// Class Utility Methods -------------------------------------------------------------------/

	protected void newProject()
	{
		int cellNum = 0;
		int charNum = TIGlobals.MINCHAR;
		for(int r = 0; r < FONT_ROWS; r++)
		{
			clrSets[r][Globals.INDEX_CLR_BACK] = 0;
			clrSets[r][Globals.INDEX_CLR_FORE] = 1;
			for(int c = 0; c < FONT_COLS; c++)
			{
				cellNum = (r * FONT_COLS) + c;
				jbtnChar[cellNum].setBackground(TIGlobals.TI_PALETTE_OPAQUE[clrSets[r][Globals.INDEX_CLR_BACK]]);
				jbtnChar[cellNum].setForeground(TIGlobals.TI_COLOR_UNUSED);
				gcChar.clearGrid();
				hmCharGrids.put(getCharKey(charNum), gcChar.getGridData());
				updateFontButton(charNum);
				charNum++;
			}
		}
		mapdMain.delAllMaps();
		mapDataFile = (File)null;
		updateComponents();
	}

	protected int confirmationAction(Frame parent, String title, String message) 
	{
		return JOptionPane.showConfirmDialog(parent, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
	}

	protected void informationAction(Frame parent, String title, String message) 
	{
		JOptionPane.showMessageDialog(parent, message, title, JOptionPane.INFORMATION_MESSAGE);
	}

// Convenience Methods ---------------------------------------------------------------------/

	protected String getCharKey(int charnum)
	{
		return ("ch" + charnum);
	}

	protected void updateComponents()
	{
		updateFontButton(activeChar);
		jtxtChar.setText(Globals.getByteString(gcChar.getGridData()).toUpperCase());
		jbtnLook.setBackground((mapdMain.isLookModeOn() ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		jbtnClone.setBackground((mapdMain.isCloneModeOn() ? Globals.CLR_BUTTON_ACTIVE : Globals.CLR_BUTTON_NORMAL));
		mapdMain.updateComponents();
	}

	protected void updateFontButton(int charnum)
	{
		if(lastActiveChar != charnum && lastActiveChar != MapCanvas.NOCHAR)
		{
			jbtnChar[lastActiveChar].setBorder(Globals.bordButtonNormal);
		}
		lastActiveChar = charnum;
		jbtnChar[charnum].setBorder(Globals.bordButtonActive);
		jlblCharInt.setText((charnum < 100 ? " " : "") + charnum);
		jlblCharHex.setText("h" + Integer.toHexString(charnum).toUpperCase());
		int charmapIndex = charnum - TIGlobals.CHARMAPSTART;
		int cset = (int)(Math.floor(charnum / 8));
		jbtnChar[charnum].setBackground(TIGlobals.TI_PALETTE_OPAQUE[clrSets[cset][Globals.INDEX_CLR_BACK]]);
		if(hmCharGrids.get(getCharKey(charnum)) == null)
		{
			if(jbtnChar[charnum].getIcon() != null)
			{
				jbtnChar[charnum].setIcon(null);
				jbtnChar[charnum].setText((charmapIndex >= 0) && (charmapIndex < TIGlobals.CHARMAP.length) ? "" + TIGlobals.CHARMAP[charmapIndex] : "?");
			}
			return;
		}
		if(jbtnChar[charnum].getIcon() != null)
		{
		}
		else
		{
			jbtnChar[charnum].setText("");
			Image imgTemp = this.createImage(gcChar.getGridData().length, gcChar.getGridData()[0].length);
			if(imgTemp == null) { return; }
			jbtnChar[charnum].setIcon(new ImageIcon(imgTemp));
		}
		Graphics g = ((ImageIcon)(jbtnChar[charnum].getIcon())).getImage().getGraphics();
		int[][] charGrid = hmCharGrids.get(getCharKey(charnum));
		for(int y = 0; y < charGrid.length; y++)
		{
			for(int x = 0; x < charGrid[y].length; x++)
			{
				g.setColor(charGrid[y][x] == 1 ? TIGlobals.TI_PALETTE_OPAQUE[clrSets[cset][Globals.INDEX_CLR_FORE]] : TIGlobals.TI_PALETTE_OPAQUE[clrSets[cset][Globals.INDEX_CLR_BACK]]);
				g.fillRect(x, y, 1, 1);
			}
		}
		Image imgTrans = makeImageTransparent(((ImageIcon)(jbtnChar[charnum].getIcon())).getImage());
		hmCharImages.put(getCharKey(charnum), imgTrans);
		mapdMain.setCharImage(charnum, imgTrans);
		if(Globals.getByteString(hmCharGrids.get(getCharKey(charnum))).equals(Globals.BLANKCHAR))
		{
			jbtnChar[charnum].setIcon(null);
			jbtnChar[charnum].setText(((charnum - TIGlobals.CHARMAPSTART) >= 0 && (charnum - TIGlobals.CHARMAPSTART) < TIGlobals.CHARMAP.length) ? "" + TIGlobals.CHARMAP[charnum - TIGlobals.CHARMAPSTART] : "?");
		}
		jbtnChar[charnum].repaint();
		mapdMain.redrawCanvas();
	}

	protected void updateFontButtons()
	{
		for(int i = TIGlobals.MINCHAR; i <= TIGlobals.MAXCHAR; i++)
		{
			updateFontButton(i);
		}
	}

	protected Image makeImageTransparent(Image imgSrc)
	{
		ImageProducer ipTrans = new FilteredImageSource(imgSrc.getSource(), Globals.ifTrans);
		return Toolkit.getDefaultToolkit().createImage(ipTrans);
	}

	protected int getTIColorForPixel(int pixelRBG)
	{
		for(int c = 0; c < TIGlobals.TI_PALETTE_OPAQUE.length; c++)
		{
			if(pixelRBG == TIGlobals.TI_PALETTE_OPAQUE[c].getRGB())
			{
				return c;
			}
		}
		return 0;
	}

	protected byte[] getCharBytes(int charnum)
	{
		byte[] charbytes = new byte[8];
		int[][] chararray = hmCharGrids.get(getCharKey(charnum));
		if(chararray != null)
		{
			int bcount = 0;
			byte byteval = (byte)0;
			int bytepos = 0;
			boolean goHigh = true;
			for(int y = 0; y < 8; y++)
			{
				bytepos = 0;
				for(int x = 0; x < 8; x++)
				{
					if(chararray[y][x] > 0) { byteval = (byte)(byteval | (bytepos == 0 ? 8 : (bytepos == 1 ? 4 : (bytepos == 2 ? 2 : 1)))); }
					bytepos++;
					if(bytepos > 3)
					{
						charbytes[bcount] = (byte)(charbytes[bcount] | (goHigh ? byteval<<4 : byteval));
						if(!goHigh)
						{
							bcount++;
						}
						goHigh = !goHigh;
						byteval = (byte)0;
						bytepos = 0;
					}
				}
			}
		}
		else
		{
			for(int i = 0; i < charbytes.length; i++)
			{
				charbytes[i] = (byte)0;
			}
		}
		return charbytes;
	}

	public void exitApp(int status)
	{
		if(status == 0) { savePreferences(); }
		this.dispose();
		System.exit(status);
	}

// Main Method -----------------------------------------------------------------------------/

	public static void main(String[] args)
	{
		Magellan magellan = new Magellan();
	}

}
