package com.dreamcodex.ti;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;

/** Charbroiler
  * TI-99/4A graphical image to data bytes converter
  *
  * @author Howard Kistler
  */

public class Charbroiler extends Frame implements ActionListener, WindowListener
{
// Components ------------------------------------------------------------------------------/

	protected MediaTracker mediaTracker;

// Variables -------------------------------------------------------------------------------/

	protected String sImage;
	protected String sData;
	protected int imgW;
	protected int imgH;
	protected BufferedImage buffImg;

// Constructors ----------------------------------------------------------------------------/

	public Charbroiler(String imageFile, String textFile)
	{
		super("Charbroiler");

		sImage = new String(imageFile);
		sData = new String(textFile);

		this.addWindowListener(this);
		this.setVisible(true);
		this.setSize(200,200);
		mediaTracker = new MediaTracker(this);
		processImage(imageFile, textFile);
		this.dispose();
		System.exit(0);
	}

// Class Methods ---------------------------------------------------------------------------/

	public void processImage(String imageFile, String textFile)
	{
		loadImageFile(imageFile);

		int[][] charArray = new int[8][8];
		int rownum = 5000;
		int charBlock = 0;
		StringBuffer sbOut = new StringBuffer();
		sbOut.append(rownum + " DATA ");
		for(int ch = 0; ch < (imgW / 8); ch++)
		{
			for(int y = 0; y < 8; y++)
			{
				for(int x = 0; x < 8; x++)
				{
					charArray[y][x] = (buffImg.getRGB((ch * 8) + x, y) != -1 ? 1 : 0);
				}
			}
			if(charBlock > 0) { sbOut.append(","); }
			sbOut.append('"' + getByteString(charArray) + '"');
			charBlock++;
			if(charBlock > 5)
			{
				sbOut.append("\n\r");
				rownum += 10;
				sbOut.append(rownum + " DATA ");
				charBlock = 0;
			}
		}
		System.out.println(sbOut.toString().toUpperCase());
	}

	public void loadImageFile(String filename)
	{
		Image charImg = loadImage(filename);
		this.getGraphics().drawImage(charImg, 0, 0, this);
		imgW = charImg.getWidth(this);
		imgH = charImg.getHeight(this);
		buffImg = new BufferedImage(imgW, imgH, BufferedImage.TYPE_3BYTE_BGR);
		while(buffImg == null) { try { Thread.sleep(50); } catch(InterruptedException ie) {} }
		buffImg.getGraphics().drawImage(charImg, 0, 0, this);
	}

// Listeners -------------------------------------------------------------------------------/

	/* ActionListener methods */
	public void actionPerformed(ActionEvent ae)
	{
		String command = ae.getActionCommand();
		if(command.equals("exit"))
		{
			this.dispose();
			System.exit(0);
		}
		else if(command.equals("loadimage"))
		{
			String[] sExts = { "png", "gif", "jpg" };
			sImage = getFilenameFromChooser(".", FileDialog.LOAD, sExts, "Image Files");
			if(sImage != null && sImage.length() > 0)
			{
				loadImageFile(sImage);
			}
		}
		else if(command.equals("savedata"))
		{
			this.dispose();
			System.exit(0);
		}
	}

	/* WindowListener methods */
	public void windowClosing(WindowEvent we)
	{
		this.dispose();
		System.exit(0);
	}
	public void windowOpened(WindowEvent we)      { ; }
	public void windowClosed(WindowEvent we)      { ; }
	public void windowActivated(WindowEvent we)   { ; }
	public void windowDeactivated(WindowEvent we) { ; }
	public void windowIconified(WindowEvent we)   { ; }
	public void windowDeiconified(WindowEvent we) { ; }

// File Handling Methods -------------------------------------------------------------------/

	private String getFilenameFromChooser(String startDir, int dialogType, String[] exts, String desc)
	{
		FileDialog fDialog = new FileDialog(this, "Choose Image", dialogType);
		fDialog.setDirectory(startDir);
		fDialog.setModal(true);
//		fDialog.setFileFilter(new MutableFilter(exts, desc));
		fDialog.setVisible(true);
		return fDialog.getFile();
	}

// Convenience Methods ---------------------------------------------------------------------/

	public String getByteString(int[][] binarray)
	{
		StringBuffer sbBytes = new StringBuffer();
		int bytepos = 0;
		int byteval = 0;
		int charnum = 0;
		int charrow = (binarray.length / 8);
		int charcol = (binarray[0].length / 8);
		for(int cc = 0; cc < charcol; cc++)
		{
			for(int cr = 0; cr < charrow; cr++)
			{
				for(int y = 0; y < 8; y++)
				{
					int cy = y + (cr * 8);
					bytepos = 0;
					for(int x = 0; x < 8; x++)
					{
						int cx = x + (cc * 8);
						if(binarray[cy][cx] > 0) { byteval += (bytepos == 0 ? 8 : (bytepos == 1 ? 4 : (bytepos == 2 ? 2 : 1))); }
						bytepos++;
						if(bytepos > 3)
						{
							sbBytes.append(Integer.toHexString(byteval));
							byteval = 0;
							bytepos = 0;
						}
					}
					if(bytepos != 0)
					{
						sbBytes.append(Integer.toHexString(byteval));
						byteval = 0;
						bytepos = 0;
					}
				}
			}
		}
		return sbBytes.toString();
	}

	public int[][] getByteIntArray(String sbtyes, int rows)
	{
		int charcol = (sbtyes.length() / rows) * 4;
		int[][] barray = new int[rows][charcol];
		int charpos = 0;
		int rowpos = 0;
		int colpos = 0;
		for(int cc = 0; cc < sbtyes.length(); cc++)
		{
			String ch = "" + sbtyes.charAt(cc);
			int chi = Integer.parseInt(ch, 16);
			barray[rowpos][charpos+colpos] = (((chi & 8) == 8) ? 1 : 0); charpos++;
			barray[rowpos][charpos+colpos] = (((chi & 4) == 4) ? 1 : 0); charpos++;
			barray[rowpos][charpos+colpos] = (((chi & 2) == 2) ? 1 : 0); charpos++;
			barray[rowpos][charpos+colpos] = (((chi & 1) == 1) ? 1 : 0); charpos++;
			if(charpos >= 8)
			{
				charpos = 0;
				rowpos++;
				if(rowpos >= rows)
				{
					rowpos = 0;
					colpos += 8;
				}
			}
		}
		return barray;
	}

	protected Image loadImage(String imgname)
	{
		if(imgname == null || imgname.equals("") || mediaTracker == null) { return (Image)null; }
		Image imgTemp = Toolkit.getDefaultToolkit().getImage(imgname);
		mediaTracker.addImage(imgTemp, 1);
		try
		{
			mediaTracker.waitForID(1);
		}
		catch(InterruptedException ie)
		{
			System.err.println("Error loading image " + imgname + " - " + ie.getMessage());
		}
		return imgTemp;
	}

	protected Button getCommandButton(String txt, String actcmd)
	{
		Button btnCmd = new Button(txt);
		btnCmd.setActionCommand(actcmd);
		btnCmd.addActionListener(this);
		return btnCmd;
	}

// Main Method -----------------------------------------------------------------------------/

	public static void main(String[] args)
	{
		if(args.length < 1)
		{
			System.out.println("usage: Charbroiler [imagefile-in] [textfile-out]");
			System.exit(1);
		}
		else
		{
			String imgFile = args[0];
			String txtFile = imgFile + ".txt";
			if(args.length > 1)
			{
				txtFile = args[1];
			}
			Charbroiler charb = new Charbroiler(imgFile, txtFile);
		}
	}

}
