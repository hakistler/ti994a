# TI-99/4A

Graphic tools to support development on the TI-99/4A

__Magellan__ - Map Editor

__Tadataka__ - Bitmap Editor

__FonTI__ - Font Editor

__Charcot__ - Sprite Editor (in dev)

__Charbroiled__ - Commandline Image-to-Text Converter

![license](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

These works are licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](href="http://creativecommons.org/licenses/by-nc-sa/4.0/).

## Magellan

There is a fork of Magellan with lots of great new features added by the Atari Age community. You can find the repository here:

[https://github.com/Rasmus-M/magellan](https://github.com/Rasmus-M/magellan)